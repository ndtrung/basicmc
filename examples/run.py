import sys
import argparse
import json
import os

def create_parser():
  parser = argparse.ArgumentParser(description='Monte Carlo simulation suite')
  parser.add_argument('-input'   ,dest='input',default=None,help = 'Input JSON file (default: None)')
  return parser


def main(argv):

  print("")
  print("========================================================")
  print("  Python wrapper for Monte Carlo sampling package")
  print("    Trung Nguyen (Northwestern) Copyright 2018-2021")
  print("========================================================")
  print("")

  parser     = create_parser()
  args       = parser.parse_args()
  
  params = json.load(open(args.input))

  nrelaxations = 1000
  ncycles = 10000
  dumping_period = 1000

  # parameters
  from_restart = 0
  init_box = 0
  if "restart" in params:
    configurationfile = params["restart"]
    from_restart = 1
  else:
    nparticles = params["nparticles"]
    box = params["box"]
    init_box = 1

  if ("box" in params or "nparticles" in params) and from_restart == 1:
    print("WARNING: Restart configuration file will ignore the box and nparticles setting")

  if "nrelaxations" in params:
    nrelaxations = int(params["nrelaxations"])
  if "ncycles" in params:
    ncycles = int(params["ncycles"])
  if "dumping_period" in params:
    dumping_period = int(params["dumping_period"])

  engine = params["engine"]
  for engine_param in engine:
    if "sampling_mode" in engine_param:
      sampling_mode = engine_param["sampling_mode"]
    if "temperature" in engine_param:      
      temperature = engine_param["temperature"]
    if "pressure" in params:
      pressure = float(engine_param["pressure"])

  if sampling_mode == "nvt":
    binary = "mc_run"
  elif sampling_mode == "npt":
    binary = "mc_run"
  elif sampling_mode == "gc":
    binary = "mc_run"

  path_binary="../src/"

  # assemble the command-line arguments to execute

  cmd = path_binary 
  cmd += binary + " "
  if from_restart == 0:
    cmd += "nparticles " + str(nparticles) + " "
    cmd += "box " + box + " "
  else:
    cmd += "restart " + configurationfile + " "

  cmd += "ensemble " + sampling_mode + " "
  cmd += "temperature " + str(temperature) + " "
  cmd += "nrelaxations " + str(nrelaxations) + " "
  cmd += "ncycles " + str(ncycles) + " "

  # execute the command line

  print("Command-line arguments: %s" % cmd)
  os.system(cmd)

  print("")
  print("Finished.")

if __name__ == "__main__":
  main(sys.argv[1:])