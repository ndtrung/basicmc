/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __BUILDING_BLOCKS
#define __BUILDING_BLOCKS

#include <stdio.h>
#include <string.h>
#include <vector>
using namespace std;

namespace MC {

class Bead
{
public:
  Bead()
  {
    id = 0;
    mol = 0;
    sprintf(type, "1");
    ntype = 1;
    q = 0;
  }

  Bead(const Bead& b)
  {
    id = b.id;
    mol = b.mol;
    strcpy(type, b.type);
    ntype = b.ntype;
    x = b.x;
    y = b.y;
    z = b.z;
    q = b.q;
  }

  Bead(double _x, double _y, double _z)
  {
    id = 0;
    mol = 0;
    sprintf(type, "1");
    ntype = 1;
    x = _x;
    y = _y;
    z = _z;
    q = 0;
  }

  Bead& operator= (const Bead& b)
  {
    if (this == &b) return *this;
    id = b.id;
    mol = b.mol;
    strcpy(type, b.type);
    ntype = b.ntype;
    x = b.x;
    y = b.y;
    z = b.z;
    q = b.q;
    return *this;
  }

  ~Bead() {}

public:
  int id;
  int mol;
  int ntype;
  char type[8];
  double x;
  double y;
  double z;
  double q;
};


typedef vector<Bead> BeadList;

class Particle
{
public:
  Particle();
  Particle(const int nbeadsperparticle);
  Particle(const Particle& p);
  Particle& operator= (const Particle& p);

  BeadList beadList;

  int rot_flag;
  double quat[4];

  static int id;
};

class Chain : public Particle
{
  friend bool operator< (const Chain& p1, const Chain& p2);
public:
  Chain() {}
  Chain(const int nbeadsperchain);
  Chain(const Chain& p);
  Chain& operator= (const Chain& p);

  int num_grown;

  static int id;
};

} // namespace MC

#endif
