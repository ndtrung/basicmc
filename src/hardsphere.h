/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Hard sphere interaction

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017
*/

#ifndef _HARD_SPHERE_H
#define _HARD_SPHERE_H

#include <cmath>
#include "evaluator.h"
#include "lattice.h"

namespace MC {

#define BIG 1.0e+20

class HardSphere : public Evaluator
{
public:
  HardSphere() {}
  HardSphere(double _sigma) { cutoff = _sigma; cutsq = cutoff*cutoff; }
  virtual ~HardSphere() {}

  void set_params(double _sigma) {
    cutoff = _sigma;
    cutsq = cutoff*cutoff;
  }

  void compute(const double rsq, double& energy) {
    if (rsq < cutsq) energy = BIG;
    else energy = 0;
  }

protected:
  double cutsq;
};

} // namespace MC

#endif

