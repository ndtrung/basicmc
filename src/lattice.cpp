/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Trung Nguyen (Northwestern)
  Start: Aug 15, 2017
*/

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include "lattice.h"
#include "memory.h"

using namespace MC;
using namespace std;

enum {ID=0,MOL=1,TYPE=2,Q=3,X=4,Y=5,Z=6};

/*---------------------------------------------------------------------------*/

Lattice::Lattice() :  vertexList(NULL)
{
  n_dim = 3;
}

/*---------------------------------------------------------------------------*/

Lattice::Lattice(int _dim) : vertexList(NULL)
{
  n_dim = _dim;
}

/*---------------------------------------------------------------------------*/

Lattice::~Lattice()
{
  destroy(vertexList);
}

/*---------------------------------------------------------------------------
  lattice-indepedent function for getting vertices from a text file
-----------------------------------------------------------------------------*/

void Lattice::assign_vertices(double** x, int num_vertices)
{
  destroy(vertexList);
  create(vertexList, num_vertices);

  for (int i = 0; i < num_vertices; i++) {
    vertexList[i].occupied = 0;
    vertexList[i].x = x[i][0];
    vertexList[i].y = x[i][1];
    vertexList[i].z = x[i][2];
    vertexList[i].num_neigh = 0;
  }
}

/*---------------------------------------------------------------------------*/

void Lattice::assign_edges(Edge* edgeList, int num_edges)
{
  if (!vertexList) return;

  for (int i = 0; i < num_edges; i++) {
    int vi = edgeList[i].vi;
    int vj = edgeList[i].vj;
    vertexList[vi].neighbors[vertexList[vi].num_neigh++] = vj;
    vertexList[vj].neighbors[vertexList[vj].num_neigh++] = vi;
  }
}

/*---------------------------------------------------------------------------
  lattice-indepedent function for getting edges from a text file
-----------------------------------------------------------------------------*/

int Lattice::get_vertices(const char* filename)
{
  ifstream ifs(filename);
  if (!ifs.good()) {
    cout << "Cannot read file " << filename << std::endl;
    return 0;
  }

  int i, timestep;
  char* line = new char[1024];

  for (i = 0; i < 9; i++) {
   ifs.getline(line, 1024);
    if (i == 1) sscanf(line, "%d", &timestep);
    if (i == 3) sscanf(line, "%d", &num_vertices);
    if (i == 5) sscanf(line, "%lf %lf", &xlo, &xhi);
    if (i == 6) sscanf(line, "%lf %lf", &ylo, &yhi);
    if (i == 7) sscanf(line, "%lf %lf", &zlo, &zhi);
  }

  // allocate the vertex list
  if (vertexList) destroy(vertexList);
  create(vertexList, num_vertices);

  Lx = xhi - xlo;
  Ly = yhi - ylo;
  Lz = zhi - zlo;
  
  int id;
  int n = 0;
  for (i = 0; i < num_vertices; i++) {
    ifs.getline(line, 1024);

    sscanf(line, "%d %lf %lf %lf",
      &id, &vertexList[n].x, &vertexList[n].y, &vertexList[n].z);
    n++;
  }

  ifs.close();

  delete [] line;
  return 1;
}

/*---------------------------------------------------------------------------
  lattice-indepedent function for getting edges (vertex neighbors)
  from a text file
-----------------------------------------------------------------------------*/

int Lattice::get_edges(const char* filename)
{
  ifstream ifs(filename);
  if (!ifs.good()) {
    cout << "Cannot read file " << filename << std::endl;
    return 0;
  }

  int nedges,m,i1,i2;
  char* line = new char[1024];

  int narg, MAXARGS=16;
  char *copy, **arg;
  copy = new char [1024];
  arg = new char* [MAXARGS];

  ifs.getline(line, 1024);
  sscanf(line, "%d", &nedges);

  for (int i = 0; i < nedges; i++) {
    ifs.getline(line, 1024);

    sscanf(line, "%d %d %d", &m, &i1, &i2);
    if (i1 >= num_vertices || i2 >= num_vertices) {
      cout << "Invalid vertex indices\n";
      break;
    }
    vertexList[i1].neighbors[vertexList[i1].num_neigh++] = i2;
    vertexList[i2].neighbors[vertexList[i2].num_neigh++] = i1;
  }

  ifs.close();

  delete [] line;
  return 1;
}

/*---------------------------------------------------------------------------*/

void Lattice::output(const char* filename)
{
}

