/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Trung Nguyen (Northwestern)
  Start: Aug 15, 2017
*/

#include "lattice_cubic.h"
#include "memory.h"

using namespace MC;

/*---------------------------------------------------------------------------*/

LatticeCubic::LatticeCubic(int _nx, int _ny, int _nz) : Lattice(3), nx(_nx), ny(_ny), nz(_nz)
{
  xlo = 0;
  xhi = nx;
  ylo = 0;
  yhi = ny;
  zlo = 0;
  zhi = nz;
  Lx = nx;
  Ly = ny;
  Lz = nz;
  num_vertices = nx * ny * nz;
  create(vertexList, num_vertices);
  generate_edges();
}

/*---------------------------------------------------------------------------*/

void LatticeCubic::generate_edges()
{
  int i,ix,iy,iz,lo,hi,idx;
  int nxy = nx*ny;

  for (i = 0; i < num_vertices; i++) {
    iz = i / nxy;
    iy = (i % nxy) / nx;
    ix = (i % nxy) % nx;

    int m = 0;

    lo = ix-1;
    hi = ix+1;
    if (lo < 0) lo = nx-1;
    if (hi >= nx) hi = 0;
    idx = lo + iy * nx + iz * nxy;
    vertexList[i].neighbors[m++] = idx;
    idx = hi + iy * nx + iz * nxy;
    vertexList[i].neighbors[m++] = idx;

    lo = iy-1;
    hi = iy+1;
    if (lo < 0) lo = ny-1;
    if (hi >= ny) hi = 0;
    idx = ix + lo * nx + iz * nxy;
    vertexList[i].neighbors[m++] = idx;
    idx = ix + hi * nx + iz * nxy;
    vertexList[i].neighbors[m++] = idx;

    lo = iz-1;
    hi = iz+1;
    if (lo < 0) lo = nz-1;
    if (hi >= nz) hi = 0;
    idx = ix + iy * nx + lo * nxy;
    vertexList[i].neighbors[m++] = idx;
    idx = ix + iy * nx + hi * nxy;
    vertexList[i].neighbors[m++] = idx;

    vertexList[i].num_neigh = m;
  }
}

