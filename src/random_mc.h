/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Random class - implements a counter-based random number generator based on the SARU algorithm

  Trung Nguyen (Northwestern)
  Start: Aug 15, 2017
*/

#ifndef _RANDOM_MC_H
#define _RANDOM_MC_H

namespace MC {

class RandomMC
{
public:
  RandomMC() {}
  RandomMC(unsigned int);
  RandomMC(unsigned int, unsigned int);
  virtual ~RandomMC();

  virtual double random_tea8(unsigned int);
  virtual double random_mars();

protected:
  unsigned int seed1, seed2;

  // Marsaglia random number generator

  void init_mars();

  double *u;
  int i97,j97;
  double c,cd,cm;
};

} // namespace MC

#endif

