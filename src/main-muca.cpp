/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Non-Boltzmann sampling techniques:
   - Multicanonical ensemble
   - Wang-Landau
   - Statistical temperature
    
  Checks:
    Muguruma et al., J. Chem. Phys. 120:7557-7563, 2004
    Lennard-Jones 12-6:, sigma = 3.405 Angstroms, epsilon = 0.9961 kJ/mol
    Box length: L = 16.314 Angstroms = 4.8 sigma
    N = 256 particles
    Per-particle energy range: -7.0 kJ/mol - -4.0 kJ/mol -> -7 epsilon -> -4 epsilon

  Trung Nguyen (Northwestern)
  Nov 2017
*/

#include <stdlib.h>
#include <vector>
#include <iostream>
#include <cassert>

#include "buildingblocks.h"
#include "initializer.h"
#include "memory.h"
#include "simulation.h"
#include "random_mc.h"

#include "monte_carlo.h"
#include "mc_muca.h"
#include "stmc.h"
#include "wanglandau.h"

#include "lj126.h"

using namespace MC;
using namespace std;

enum {MUCA=0,ST=1,WL=2};

int main(int argc, char** argv)
{
  int me=0, nprocs=1;

  // engines
  int engine = ST;
  double dE = 1.0;
  int set_dE = 0;
  double Emin = -660; // per particle
  double Emax = -450;
  int set_rangeE = 0;
  double Tmin = 0.65;
  double Tmax = 1.82;
  double dmax = 0.5;
  int set_rangeT = 0;
  int ncycles = 10000;
  int nrelaxations = 100;
  double threshold = 0.8;

  // particle initialization and box dimensions
  int nparticles = 108;
  double ax = 1.2;
  double ay = 1.2;
  double az = 1.2;
  double lx = 12;
  double ly = 12;
  double lz = 12;

  // output setting  
  unsigned int seed = 3478914;
  int sampling_period = 100;
  int dump_period = 1000;
  char logfile[256];
  int use_log = 1; 
  char restartfile[256];
  int use_restart = 0;

  sprintf(logfile, "log.txt");

  std::cout << "Command line arguments: ";
  for (int i = 0; i < argc; i++)
    std::cout << argv[i] << " ";
  std::cout << "\n";   

  int iarg = 1;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"box") == 0) {
      lx = atof(argv[iarg+1]);
      ly = atof(argv[iarg+2]);
      lz = atof(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"nparticles") == 0) {
      nparticles = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"spacing") == 0) {
      ax = atof(argv[iarg+1]);
      ay = atof(argv[iarg+2]);
      az = atof(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"ncycles") == 0) {
      ncycles = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"nrelaxations") == 0) {
      nrelaxations = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"seed") == 0) {
      seed = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"sampling_period") == 0) {
      sampling_period = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"dump_period") == 0) {
      dump_period = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"log") == 0) {
      strcpy(logfile, argv[iarg+1]);
      use_log = 1;
      iarg += 2;
    } else if (strcmp(argv[iarg],"restart") == 0) {
      strcpy(restartfile, argv[iarg+1]);
      use_restart = 1;
      iarg += 2;
    } else if (strcmp(argv[iarg],"dmax") == 0) {
      dmax = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"threshold") == 0) {
      threshold = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"dE") == 0) {
      dE = atof(argv[iarg+1]);
      set_dE = 1;
      iarg += 2;
    } else if (strcmp(argv[iarg],"rangeE") == 0) {
      Emin = atof(argv[iarg+1]);
      Emax = atof(argv[iarg+2]);
      set_rangeE = 1;
      iarg += 3;
    } else if (strcmp(argv[iarg],"rangeT") == 0) {
      Tmin = atof(argv[iarg+1]);
      Tmax = atof(argv[iarg+2]);
      set_rangeT = 1;
      iarg += 3;
    } else if (strcmp(argv[iarg],"engine") == 0) {
      if (strcmp(argv[iarg+1], "muca") == 0) engine = MUCA;
      else if (strcmp(argv[iarg+1], "stmc") == 0) engine = ST;
      else if (strcmp(argv[iarg+1], "wanglandau") == 0) engine = WL;
      else std::cout << "Engine " << argv[iarg+1] << " is not supported.\n";
      iarg += 2;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  // particles
  Particle* particleList = NULL;
  // simulation box
  Box* box = NULL;
  // the simulator
  MonteCarlo* sim = NULL;
  // create the random number generator
  RandomMC* random = new RandomMC(seed+me);

  if (use_restart == 0) {

    Initializer<Particle> init;
    init.create_box(box, particleList, nparticles, lx, ly, lz, ax, ay, az);

    if (engine == MUCA) sim = new MCMUCA(box, particleList, random, nparticles);
    else if (engine == ST) sim = new STMC(box, particleList, random, nparticles);
    else if (engine == WL) sim = new MCWangLandau(box, particleList, random, nparticles);

  } else {

    if (engine == MUCA) sim = new MCMUCA(particleList, random, nparticles);
    else if (engine == ST) sim = new STMC(particleList, random, nparticles);
    else if (engine == WL) sim = new MCWangLandau(particleList, random, nparticles);
   
    if (sim) sim->read_restart(restartfile);

  }

  if (!sim) {
    std::cout << "Error: Cannot create the simulator\n";
    delete random;
    return 1;
  }

  // specify the pair style in use

  sim->set_evaluator(new LJ126(1.0, 1.0, 2.5));

  // specify output periods

  if (use_log) sim->set_log(logfile);
  sim->set_sampling(sampling_period);
  sim->set_dump(dump_period);
  sim->set_dmax(dmax);

  // specify engine-specific parameters

  if (engine == MUCA) {

    if (set_dE) ((MCMUCA*)sim)->set_energy_bin(dE);
    ((MCMUCA*)sim)->set_energy_range(Emin, Emax);
    ((MCMUCA*)sim)->set_flatness_threshold(threshold);

  } else if (engine == ST) {

    if (set_dE) ((STMC*)sim)->set_energy_bin(dE);
    ((STMC*)sim)->set_energy_range(Emin, Emax);
    ((STMC*)sim)->set_temperature_range(Tmin, Tmax);
    ((STMC*)sim)->set_flatness_threshold(threshold);


  } else if (engine == WL) {

    if (set_dE) ((MCWangLandau*)sim)->set_energy_bin(dE);
    ((MCWangLandau*)sim)->set_energy_range(Emin, Emax);
    ((MCWangLandau*)sim)->set_flatness_threshold(threshold);
  }

  // run the simulation

  sim->run(nrelaxations, ncycles);


  // clean up memory allocation
    
  delete [] particleList;
  delete random;
  delete box;
  delete sim;

  return 0;
}
