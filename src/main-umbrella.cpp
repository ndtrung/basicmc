/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  
  Umbrella sampling with Monte Carlo demonstration

  Trung Nguyen (Northwestern)
  Sep 2018
*/

#include <stdlib.h>
#include <vector>
#include <iostream>
#include <cassert>

#include "buildingblocks.h"
#include "initializer.h"
#include "simulation.h"
#include "random_mc.h"

#include "monte_carlo.h"
#include "mc_nvt.h"
#include "mc_npt.h"
#include "mc_gc.h"
#include "memory.h"

#include "lj126.h"
#include "hardsphere.h"
#include "square_well.h"
#include "opp.h"
#include "kern-frenkel.h"

using namespace MC;
using namespace std;

enum {MC_NVT=0,MC_NPT=1,MC_GC=2};

int main(int argc, char** argv)
{
  int me=0, nprocs=1;

  // ensemble
  int ensemble = MC_GC;
  double temperature = 1.0;
  double pressure = 2.0;
  double mu = -1.0;
  int ncycles = 100;
  int nrelaxations = 100;
  unsigned int seed = 3478914;
  double target_acceptance = -1.0;
  double dmax = -1.0;

  // particle initialization and box dimensions
  int nparticles = 125;
  double ax = 1.0;
  double ay = 1.0;
  double az = 1.0;
  double lx = 14;
  double ly = 14;
  double lz = 14;

  // force field
  char forcefield[64];
  strcpy(forcefield, "lj/cut");
  double epsilon = 1.0;
  double sigma = 1.0;
  double cutoff = 2.5;
  double kappa = 6.25;
  double phi = 0.62;
  double lambda = 1.2;
  double theta = 1.4;
  double A = 2.0;

  // output setting
  int sampling_period = 100;
  int dump_period = 1000;
  char logfile[256];
  int use_log = 0; 
  char restartfile[256];
  int use_restart = 0; 
  int anisotropic_flag = 0;

  int iarg = 1;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"ensemble") == 0) {
      if (strcmp(argv[iarg+1], "nvt") == 0) ensemble = MC_NVT;
      else if (strcmp(argv[iarg+1], "npt") == 0) ensemble = MC_NPT;
      else if (strcmp(argv[iarg+1], "gc") == 0) ensemble = MC_GC;
      else std::cout << "Ensemble " << argv[iarg+1] << " is not supported.\n";
      iarg += 2;
    } else if (strcmp(argv[iarg],"box") == 0) {
      lx = atof(argv[iarg+1]);
      ly = atof(argv[iarg+2]);
      lz = atof(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"nparticles") == 0) {
      nparticles = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"spacing") == 0) {
      ax = atof(argv[iarg+1]);
      ay = atof(argv[iarg+2]);
      az = atof(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"target_acceptance") == 0) {
      target_acceptance = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"dmax") == 0) {
      dmax = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"ncycles") == 0) {
      ncycles = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"nrelaxations") == 0) {
      nrelaxations = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"temperature") == 0) {
      temperature = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"pressure") == 0) {
      pressure = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"chemical_potential") == 0) {
      mu = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"seed") == 0) {
      seed = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"sampling_period") == 0) {
      sampling_period = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"dump_period") == 0) {
      dump_period = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"log") == 0) {
      strcpy(logfile, argv[iarg+1]);
      use_log = 1;
      iarg += 2;
    } else if (strcmp(argv[iarg],"restart") == 0) {
      strcpy(restartfile, argv[iarg+1]);
      use_restart = 1;
      iarg += 2;
    } else if (strcmp(argv[iarg],"pair_style") == 0) {
      strcpy(forcefield, argv[iarg+1]);
      if (strcmp(forcefield, "lj/cut") == 0) {
        epsilon = atof(argv[iarg+2]);
        sigma = atof(argv[iarg+3]);
        cutoff = atof(argv[iarg+4]);
        iarg += 5;
      } else if (strcmp(forcefield, "hardsphere") == 0) {
        sigma = atof(argv[iarg+2]);
        iarg += 3;
      } else if (strcmp(forcefield, "squarewell") == 0) {
        sigma = atof(argv[iarg+2]);
        lambda = atof(argv[iarg+3]);
        epsilon = atof(argv[iarg+4]);
        iarg += 5;
      } else if (strcmp(forcefield, "opp") == 0) {
        kappa = atof(argv[iarg+2]);
        phi = atof(argv[iarg+3]);
        cutoff = atof(argv[iarg+4]);
        iarg += 5;
      } else if (strcmp(forcefield, "kern/frenkel") == 0) {
        sigma = atof(argv[iarg+2]);
        theta = atof(argv[iarg+3]);
        A = atof(argv[iarg+4]);
        cutoff = atof(argv[iarg+5]);
        iarg += 6;
      } else {
        std::cout << "Invalid force field " << forcefield << "\n";
        return 1;
      }
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  // particles
  Particle* particleList = NULL;
  // simulation box
  Box* box = NULL;
  // the simulator
  MonteCarlo* sim = NULL;
  // create the random number generator
  RandomMC* random = new RandomMC(seed+me);

  if (use_restart == 0) {

    Initializer<Particle> init;
    init.create_box(box, particleList, nparticles, lx, ly, lz, ax, ay, az);

    if (ensemble == MC_NVT)
      sim = new MCNVT(box, particleList, random, nparticles, temperature);
    else if (ensemble == MC_NPT)
      sim = new MCNPT(box, particleList, random, nparticles, temperature, pressure);
    else if (ensemble == MC_GC)
      sim = new MCGC(box, particleList, random, nparticles, temperature, mu);

  } else {

    particleList = new Particle [nparticles];

    if (ensemble == MC_NVT)
      sim = new MCNVT(particleList, random, nparticles, temperature);
    else if (ensemble == MC_NPT)
      sim = new MCNPT(particleList, random, nparticles, temperature, pressure);
    else if (ensemble == MC_GC)
      sim = new MCGC(particleList, random, nparticles, temperature, mu);
    
    if (sim) sim->read_restart(restartfile);

  }

  if (!sim) {
    std::cout << "Error: Cannot create the simulator\n";
    delete random;
    return 1;
  }

  // specify the pair style in use

  if (strcmp(forcefield, "lj/cut") == 0)
    sim->set_evaluator(new LJ126(epsilon, sigma, cutoff));
  else if (strcmp(forcefield, "hardsphere") == 0)
    sim->set_evaluator(new HardSphere(cutoff));
  else if (strcmp(forcefield, "squarewell") == 0)
    sim->set_evaluator(new SquareWell(sigma, lambda, epsilon));
  else if (strcmp(forcefield, "opp") == 0)
    sim->set_evaluator(new Oscillating(kappa, phi, cutoff));
  else if (strcmp(forcefield, "kern/frenkel") == 0) {
    anisotropic_flag = 1;
    sim->set_evaluator(new KernFrenkel(sigma, theta, A, cutoff));
  }

  // specify optional settings

  if (target_acceptance > 0)
    sim->set_target_acceptance(target_acceptance);

  if (dmax > 0)
    sim->set_dmax(dmax);

  // specify output periods

  if (use_log) sim->set_log(logfile);
  sim->set_sampling(sampling_period);
  sim->set_dump(dump_period);
  sim->set_anisotropic(anisotropic_flag);

  int nsweeps = 100;
  int nsamples = 10000;
  double zeta0 = 15.0;  // center of the order parameter
  double kspring = 100; // spring constant

  // first equilibration the simulation

  sim->run(nrelaxations, ncycles);

  // compute the order parameter zeta

  // compute the current biasing energy

  // equilibrate within the window zeta0 (``pulling'' the system towards zeta0)

  for (int i = 0; i < nsweeps; i++) {  

    sim->run(nrelaxations, ncycles);

    // compute the order parameter zeta

    // compute the biasing potential

    double Enew = kspring*(zeta - zeta0)*(zeta - zeta0);

    // accept/reject the last configuration based on the change in the biasing potential

    if (E <= Enew) {
      // accept
    } else if (exp(-(Enew-E)) < drand48()) {
      // reject
    } else {
      // accept
    }
  }

  // sample within the window
  
  for (int i = 0; i < nsamples; i++) {
    sim->run(nrelaxations, ncycles);
  }

  // clean up memory allocation

  delete [] particleList;
  delete random;
  delete box;
  delete sim;

  return 0;
}
