/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Generic memory management functions
  support aligned memory allocation

  Trung Nguyen (Northwestern)
*/

#ifndef __MEMORY
#define __MEMORY

#include <stdlib.h>

namespace MC {

#define MEMALIGN 64

// 1D arrays

template <typename TYPE>
TYPE* create(TYPE*& array, int nsize)
{
  if (nsize == 0) return NULL;
  int64_t nbytes = nsize * sizeof(TYPE);
#if defined(MEMALIGN)
  void* ptr = NULL;
  int error = posix_memalign(&ptr, MEMALIGN, nbytes);
  if (error) array = NULL;
  else array = (TYPE*) ptr;
#else
  array = (TYPE*) malloc(nbytes);
#endif
  return array;
}

template <typename TYPE>
TYPE *grow(TYPE *&array, int n)
{
  if (array == NULL) return create(array,n);
  int64_t  nbytes = ((int64_t) sizeof(TYPE)) * n;
  array = (TYPE *) realloc(array,nbytes);
  return array;
}

template <typename TYPE>
void destroy(TYPE*& array)
{
  if (array == NULL) return;
  free(array);
  array = NULL;
}

// 2D arrays

template <typename TYPE>
TYPE** create(TYPE**& array, int row, int col)
{
  int64_t nbytes = (row*col) * ((int64_t)sizeof(TYPE));
  if (nbytes == 0) return NULL;

  TYPE* data;
#if defined(MEMALIGN)
  void* ptr = NULL;
  int error;
  error = posix_memalign(&ptr, MEMALIGN, nbytes);
  if (error) data = NULL;
  else data = (TYPE*) ptr;
#else
  data = (TYPE*) malloc(nbytes);
#endif

  int np = row * sizeof(TYPE*);
#if defined(MEMALIGN)
  error = posix_memalign(&ptr, MEMALIGN, np);
  if (error) array = NULL;
  else array = (TYPE**) ptr;
#else
  array = (TYPE**) malloc(np);
#endif

  int n = 0;
  for (int i = 0; i < row; i++) {
    array[i] = &data[n];
    n += col;
  }
  return array;
}

template <typename TYPE>
TYPE **grow(TYPE **&array, int n1, int n2)
{
  if (array == NULL) return create(array,n1,n2);

  int64_t nbytes = ((int64_t) sizeof(TYPE)) * n1*n2;
  TYPE *data = (TYPE *) realloc(array[0],nbytes);
  nbytes = ((int64_t) sizeof(TYPE *)) * n1;
  array = (TYPE **) realloc(array,nbytes);

  int64_t n = 0;
  for (int i = 0; i < n1; i++) {
    array[i] = &data[n];
    n += n2;
  }
  return array;
}

template <typename TYPE>
void destroy(TYPE**& array)
{
  if (array == NULL) return;
  free(array[0]);
  free(array);
  array = NULL;
}

// 3D arrays

template <typename TYPE>
TYPE ***create(TYPE ***&array, int n1, int n2, int n3)
{
  int64_t nbytes = ((int64_t) sizeof(TYPE)) * n1*n2*n3;
  if (nbytes == 0) return NULL;

  TYPE *data;
#if defined(MEMALIGN)
  void* ptr = NULL;
  int error;
  error = posix_memalign(&ptr, MEMALIGN, nbytes);
  if (error) data = NULL;
  else data = (TYPE*) ptr;
#else
  data = (TYPE*) malloc(nbytes);
#endif

  nbytes = ((int64_t) sizeof(TYPE *)) * n1*n2;
  TYPE **plane;
#if defined(MEMALIGN)
  error = posix_memalign(&ptr, MEMALIGN, nbytes);
  if (error) plane = NULL;
  else plane = (TYPE**) ptr;
#else
  plane = (TYPE**) malloc(nbytes);
#endif

  nbytes = ((int64_t) sizeof(TYPE **)) * n1;
#if defined(MEMALIGN)
  error = posix_memalign(&ptr, MEMALIGN, nbytes);
  if (error) array = NULL;
  else array = (TYPE***) ptr;
#else
  array = (TYPE***) malloc(nbytes);
#endif

  int i,j;
  int64_t m;
  int64_t n = 0;
  for (i = 0; i < n1; i++) {
    m = ((int64_t) i) * n2;
    array[i] = &plane[m];
    for (j = 0; j < n2; j++) {
      plane[m+j] = &data[n];
      n += n3;
    }
  }
  return array;
}

/* ----------------------------------------------------------------------
   grow or shrink 1st dim of a 3d array
   last 2 dims must stay the same
------------------------------------------------------------------------- */

template <typename TYPE>
TYPE ***grow(TYPE ***&array, int n1, int n2, int n3)
{
  if (array == NULL) return create(array,n1,n2,n3);

  int64_t nbytes = ((int64_t) sizeof(TYPE)) * n1*n2*n3;
  TYPE *data = (TYPE *) realloc(array[0][0],nbytes);
  nbytes = ((int64_t) sizeof(TYPE *)) * n1*n2;
  TYPE **plane = (TYPE **) realloc(array[0],nbytes);
  nbytes = ((int64_t) sizeof(TYPE **)) * n1;
  array = (TYPE ***) realloc(array,nbytes);

  int i,j;
  int64_t m;
  int64_t n = 0;
  for (i = 0; i < n1; i++) {
    m = ((int64_t) i) * n2;
    array[i] = &plane[m];
    for (j = 0; j < n2; j++) {
      plane[m+j] = &data[n];
      n += n3;
    }
  }
  return array;
}

template <typename TYPE>
void destroy(TYPE ***&array)
{
  if (array == NULL) return;
  free(array[0][0]);
  free(array[0]);
  free(array);
  array = NULL;
}

} // namespace MC

#endif
