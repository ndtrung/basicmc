/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Grand canonical Monte Carlo simulation

  Trung Nguyen (Northwestern)
  Start: Nov 8, 2017
*/

#ifndef _MC_GC_H
#define _MC_GC_H

#include "monte_carlo.h"

namespace MC {

class MCGC : public MonteCarlo
{
public:
  MCGC(Particle*& _particleList, class RandomMC* _random, int _nparticles,
    double _temperature, double _mu);
  MCGC(class Box* _box, Particle*& _particleList,
    class RandomMC* _random, int _nparticles, double _temperature, double _mu);
  ~MCGC();

  void init();
  void setup();
  void run(int nrelaxations, int ncycles);
  void sampling_gc(int ntrials, double insertion_prob, int numparticles_ave);
  void thermo(int n);

protected:
  double mu;                  // chemical potential
  double temperature;         // temperature
  double total_energy;        // total energy
  double virial;              // virial
  double volume;              // box volume
  double rho;                 // number density

  // trial moves
  double particle_displacement();
  double particle_insertion();
  double particle_deletion();

  void remove_particle(int particle_idx, int last_cell_idx, int idx_in_last_cell);

  // grow the particle list when a new particle is added (in incremental of DELTA elements)
  void grow_arrays(const int new_size);

  int particle_idx;           // chosen particle index
  double backup[4];           // saved particle coordinates

  double energy_correction;   // energy and pressure correction terms due to truncation
  double pressure_correction;

  int naccepted_displacement; // number of accepted/rejected moves
  int nrejected_displacement;
  int naccepted_insertion;
  int nrejected_insertion;
  int naccepted_deletion;
  int nrejected_deletion;

};

} // namespace MC

#endif

