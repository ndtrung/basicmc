/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Cell list implementation:
  - The particles are binned to cells whose sizes are determined from
    the interaction cutoff.
  - Particle energy is computed by looping through the particles in the same cell
    and the cell's 27 neighboring cells.
  - When a particle moves from a cell to another, it gets removed from the last cell
    and added to the end of the new cell.

  Trung Nguyen (Northwestern)
  Start: Apr 29, 2018
*/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>

#include "memory.h"
#include "monte_carlo.h"
#include "buildingblocks.h"
#include "lattice.h"
#include "cell.h"

using namespace MC;
using namespace std;

#define MAX_PARTICLES_IN_CELL 200
#define FULL_NEIGH 27

#define CellT Cell<PARTICLE>

/*---------------------------------------------------------------------------*/

template <typename PARTICLE>
CellT::Cell(class Box*& _box, PARTICLE*& _particleList, int _nparticles, double cellsize) :
  box(_box), particleList(_particleList), nparticles(_nparticles), cutneighmax(cellsize)
{
  cells = NULL;
  neighbor_cells = NULL;
  num_neighbor_cells = NULL;
  num_particles_in_cell = NULL;

  allocated_size = MAX_PARTICLES_IN_CELL;
}

/*---------------------------------------------------------------------------*/

template <typename PARTICLE>
CellT::~Cell()
{
  deallocate_arrays();
}

/*-----------------------------------------------------------------------------
  setup the cell list
  invoked at beginning of the run or when box dims change
-----------------------------------------------------------------------------*/

template <typename PARTICLE>
void CellT::setup()
{
  int i, j, k, c, cn, x, y, z, ncellxy;
	double cutneighmaxsq = cutneighmax * cutneighmax;
	double binsize_optimal = cutneighmax;
  double binsizeinv = 1.0/binsize_optimal;

	ncellx = static_cast<int> (box->Lx*binsizeinv);
	ncelly = static_cast<int> (box->Ly*binsizeinv);
	ncellz = static_cast<int> (box->Lz*binsizeinv);

	if (ncellx < 3) ncellx = 3;
	if (ncelly < 3) ncelly = 3;
	if (ncellz < 3) ncellz = 3;

	cellsizex = box->Lx / ncellx;
	cellsizey = box->Ly / ncelly;
	cellsizez = box->Lz / ncellz;

  ncellxy = ncellx * ncelly;
  ncells = ncellxy * ncellz;

  deallocate_arrays();

  create(cells, ncells, allocated_size);
  create(num_particles_in_cell, ncells);
  create(neighbor_cells, ncells, FULL_NEIGH);
  create(num_neighbor_cells, ncells);

  for (c = 0; c < ncells; c++) num_neighbor_cells[c] = 0;
 
  // loop over all the cells
  for (i = 0; i < ncellz; i++) {
    for (j = 0; j < ncelly; j++) {
      for (k = 0; k < ncellx; k++) {
  
        // identify current cell 
        c = i*ncellxy + j*ncellx + k;
    
        // always interact within cell 
        neighbor_cells[c][num_neighbor_cells[c]] = c;
        num_neighbor_cells[c]++;
    
        // collect near neighbors of cell c 
        for (x = -1; x <= 1; x++) {
          for (y = -1; y <= 1; y++) {
            for (z = -1; z <= 1; z++) {

              if (x == 0 && y == 0 && z == 0) continue;

              cn = ((i + z + ncellz) % ncellz) * ncellxy +
               ((j + y + ncelly) % ncelly) * ncellx + 
               (k + x + ncellx) % ncellx;
              neighbor_cells[c][num_neighbor_cells[c]] = cn;
              num_neighbor_cells[c]++;
            }
          }
        }
      }
    }
  } // end loop over cells
}

/*---------------------------------------------------------------------------*/

template <typename PARTICLE>
void CellT::deallocate_arrays()
{
	destroy(cells);
	destroy(neighbor_cells);
	destroy(num_neighbor_cells);
  destroy(num_particles_in_cell);
}

/*---------------------------------------------------------------------------
  update the cell with the particle idx's new coordinates
-----------------------------------------------------------------------------*/

template <typename PARTICLE>
void CellT::update(const int particle_idx, const int idx_in_last_cell, double* last_pos)
{
  const double xlo = box->xlo;
  const double ylo = box->ylo;
  const double zlo = box->zlo;
  const double xtmp = particleList[particle_idx].beadList[0].x;
  const double ytmp = particleList[particle_idx].beadList[0].y;
  const double ztmp = particleList[particle_idx].beadList[0].z;

  int last_cell = coord2cell(last_pos[0], last_pos[1], last_pos[2], xlo, ylo, zlo);
  int new_cell = coord2cell(xtmp, ytmp, ztmp, xlo, ylo, zlo);

  if (new_cell == last_cell) return;

  // remove the particle from the last cell
  //   by replacing it with the last particle in the cell

  cells[last_cell][idx_in_last_cell] = cells[last_cell][num_particles_in_cell[last_cell]-1];
  num_particles_in_cell[last_cell]--;

  // grow the array if needed

  if (num_particles_in_cell[new_cell] == MAX_PARTICLES_IN_CELL) {
    allocated_size += MAX_PARTICLES_IN_CELL;
    grow(cells, ncells, allocated_size);
  }

  // add the particle into the new cell

  cells[new_cell][num_particles_in_cell[new_cell]] = particle_idx;
  num_particles_in_cell[new_cell]++;
}

/*---------------------------------------------------------------------------*/

template <typename PARTICLE>
void CellT::bin_particles()
{
  const double xlo = box->xlo;
  const double ylo = box->ylo;
  const double zlo = box->zlo;
  const int ncellxy = ncellx * ncelly;

	for (int i = 0; i < ncells; i++) num_particles_in_cell[i] = 0;

  for (int i = 0; i < nparticles; i++) {
    const double xtmp = particleList[i].beadList[0].x;
    const double ytmp = particleList[i].beadList[0].y;
    const double ztmp = particleList[i].beadList[0].z;

    int c = coord2cell(xtmp, ytmp, ztmp, xlo, ylo, zlo);
    assert(c < ncells && c >= 0);

    // put particle i into cell c, increase the counter

    cells[c][num_particles_in_cell[c]] = i;
    num_particles_in_cell[c]++;

    if (num_particles_in_cell[c] == MAX_PARTICLES_IN_CELL) {
      allocated_size += MAX_PARTICLES_IN_CELL;
      grow(cells, ncells, allocated_size);
    }
  }
}

/*---------------------------------------------------------------------------
  return the cell idx that owns the particle with coordinates (x, y, z)
-----------------------------------------------------------------------------*/

template <typename PARTICLE>
int CellT::coord2cell(const double x, const double y, const double z,
 const double xlo, const double ylo, const double zlo)
{
  return (int)((z - zlo) / cellsizez) * ncellx * ncelly + 
     (int)((y - ylo) / cellsizey) * ncellx + 
     (int)((x - xlo) / cellsizex);
}

/*---------------------------------------------------------------------------
  add a particle idx (particle_idx) to the correct cell
  return the cell idx
-----------------------------------------------------------------------------*/

template <typename PARTICLE>
int CellT::add_particle(int particle_idx)
{
  const double xlo = box->xlo;
  const double ylo = box->ylo;
  const double zlo = box->zlo;
  const double xtmp = particleList[particle_idx].beadList[0].x;
  const double ytmp = particleList[particle_idx].beadList[0].y;
  const double ztmp = particleList[particle_idx].beadList[0].z;
  int c = coord2cell(xtmp, ytmp, ztmp, xlo, ylo, zlo);
  assert(c < ncells && c >= 0);

  if (num_particles_in_cell[c] == MAX_PARTICLES_IN_CELL) {
    allocated_size += MAX_PARTICLES_IN_CELL;
    grow(cells, ncells, allocated_size);
  }

  // put particle i into cell c, increase the counter

  cells[c][num_particles_in_cell[c]] = particle_idx; 
  num_particles_in_cell[c]++;

  // update the number of particles (used for binning and total energy calculation)

  nparticles++;

  return c;
}

/*---------------------------------------------------------------------------*/

template <typename PARTICLE>
void CellT::add_particle(int particle_idx, int cell_idx)
{
  if (num_particles_in_cell[cell_idx] == MAX_PARTICLES_IN_CELL) {
    allocated_size += MAX_PARTICLES_IN_CELL;
    grow(cells, ncells, allocated_size);
  }

  // put particle i into cell c, increase the counter

  cells[cell_idx][num_particles_in_cell[cell_idx]] = particle_idx; 
  num_particles_in_cell[cell_idx]++;

  // update the number of particles (used for binning and total energy calculation)

  nparticles++;
}

/*---------------------------------------------------------------------------*/

template <typename PARTICLE>
void CellT::remove_last_particle(int cell_idx)
{
  // reduce the particle counter in the cell

  num_particles_in_cell[cell_idx]--;

  // update the number of particles (used for binning and total energy calculation)

  nparticles--;
}

/*---------------------------------------------------------------------------*/

template <typename PARTICLE>
void CellT::remove_particle(int cell_idx, int idx_in_cell)
{
  // replace the particle at the idx_in_cell from the cell idx
  // reduce the counter

  int n = num_particles_in_cell[cell_idx];
  cells[cell_idx][idx_in_cell] = cells[cell_idx][n-1];
  num_particles_in_cell[cell_idx]--;

  // update the number of particles (used for binning and total energy calculation)

  nparticles--;
}

/*---------------------------------------------------------------------------
  replace particle_idx_dest with particle_idx_src
-----------------------------------------------------------------------------*/

template <typename PARTICLE>
void CellT::replace_particle(int particle_idx_src, int particle_idx_dest)
{
  const double xlo = box->xlo;
  const double ylo = box->ylo;
  const double zlo = box->zlo;
  const double xtmp = particleList[particle_idx_dest].beadList[0].x;
  const double ytmp = particleList[particle_idx_dest].beadList[0].y;
  const double ztmp = particleList[particle_idx_dest].beadList[0].z;

  // find the cell the particle_idx_dst belongs to

  int c = coord2cell(xtmp, ytmp, ztmp, xlo, ylo, zlo);

  // find the particle_idx_dst in the cell c, and replace it with particle_idx_src

  for (int i = 0; i < num_particles_in_cell[c]; i++) {
    if (cells[c][i] == particle_idx_dest) {
      // now the cell c stores particle_idx_src
      cells[c][i] = particle_idx_src;
      break;
    }
  }

}

/*---------------------------------------------------------------------------*/

template <typename PARTICLE>
void CellT::print_statistics()
{
  printf("Cell statistics:\n  Cell size = %g %g %g\n  Number of cells = %d\n  "
    "Number of neighboring cells = %d\n", cellsizex, cellsizey, cellsizez,
     ncells, num_neighbor_cells[0]);
}

template class Cell<Particle>;


