/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Metropolis canonical (NVT) Monte-Carlo simulation

  Trung Nguyen (Northwestern)
  Start: Nov 8, 2017
*/

#ifndef _MC_NVT_H
#define _MC_NVT_H

#include "monte_carlo.h"

namespace MC {

class MCNVT : public MonteCarlo
{
public:
  MCNVT(Particle*& _particleList, class RandomMC* _random, int _nparticles,
    double _temperature);
  MCNVT(class Box* _box, Particle*& _particleList, class RandomMC* _random,
    int _nparticles, double _temperature);
  ~MCNVT();

  void init();
  void setup();
  void run(int nrelaxations, int ncycles);
  void set_num_attempts(int _num_displacements) { num_displacements = _num_displacements; }
  void thermo(int n);

  virtual void sampling_nvt(double, int&, int&);

protected:
  double temperature;         // temperature
  double total_energy;        // total energy
  double virial;              // virial
  double volume;              // box volume
  double rho;                 // number density
  int particle_idx;           // particle index
  double energy_correction;   // energy correction term due to truncation
  int num_displacements;      // number of attempts per cycle
  int naccepted;
  int nrejected;
  double acceptance_ratio;
};

} // namespace MC

#endif

