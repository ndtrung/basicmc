/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Metropolis NVT Monte-Carlo simulation

  Trung Nguyen (Northwestern)
  Start: Aug 15, 2017
*/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>

#include "mc_nvt.h"
#include "buildingblocks.h"
#include "io.h"
#include "lattice.h"
#include "memory.h"
#include "random_mc.h"

using namespace MC;
using namespace std;

/*---------------------------------------------------------------------------*/

MCNVT::MCNVT(Particle*& _particleList, class RandomMC* _random, int _nparticles,
  double _temperature) : MonteCarlo(_particleList, _random, _nparticles),
  temperature(_temperature)
{
  dmax = 0.5;
  rmax = 0.5;
  num_displacements = nparticles;
}

/*---------------------------------------------------------------------------*/

MCNVT::MCNVT(class Box* _box, Particle*& _particleList, class RandomMC* _random,
   int _nparticles, double _temperature)
  : MonteCarlo(_box, _particleList, _random, _nparticles), temperature(_temperature)
{
  dmax = 0.5;
  rmax = 0.5;
  num_displacements = nparticles;
}

/*---------------------------------------------------------------------------*/

MCNVT::~MCNVT()
{
}

/*---------------------------------------------------------------------------*/

void MCNVT::init()
{
  MonteCarlo::init();

  printf("Running MC NVT simulation..\n");
  if (logfile) fprintf(logfile, "Running MC NVT simulation..\n");
}

/*---------------------------------------------------------------------------*/

void MCNVT::setup()
{
  MonteCarlo::setup();

  double cutoff = evaluator->get_cutoff();
  volume = box->Lx * box->Ly * box->Lz;
  rho = (double)nparticles/volume;
  evaluator->get_energy_correction(cutoff, rho, energy_correction);

  // cell list setup

  cell = new Cell<Particle>(box, particleList, nparticles, 2.0*cutoff);
  cell->setup();

  // compute total energy

  compute_total_energy(total_energy, virial);
  total_energy += nparticles * energy_correction;

  printf("Number of particles = %d\nNumber density = %g\nTemperature = %g\n",
    nparticles, (double)nNumTotalBeads/volume, temperature);
  if (logfile) {
    fprintf(logfile, "Number of particles = %d\nNumber density = %g\n"
      "Temperature = %g\n", nparticles, temperature, (double)nNumTotalBeads/volume);
    fflush(logfile);
  }

  // write out the initial configuration

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "initial", 0);
}

/*---------------------------------------------------------------------------*/

void MCNVT::run(int nrelaxations, int ncycles)
{
  // initialize the particles, if needed

  init();

  if (target_acceptance > 0) {
    printf("WARNING: Adjusting trial moves to achieve target acceptance ratio of %g, "
     "detailed balance is not satisfied.\n", target_acceptance);
  }

  // setting up the simulation run

  setup();

  // relaxation at high temperature

  printf("Relaxation for %d cycles\n", nrelaxations);
  printf("Cycle Energy AcceptanceRatio dmax\n");
  if (logfile) {
    fprintf(logfile, "Relaxation for %d cycles\n", nrelaxations);
    fprintf(logfile, "Cycle Energy AcceptanceRatio dmax\n");
    fflush(logfile);
  }

  std::vector<double> energies;
  int ntrials = nparticles;
  naccepted = 0;
  nrejected = 0;
  int niterations = 0;

  clock_t start, elapsed;
  start = clock();

  for (int n = 0; n < nrelaxations; n++) {

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++)
      sampling_nvt(100.0, naccepted, nrejected);

    if (n % sampling_period == 0 || n == nrelaxations)
      thermo(n);

    ncurrent_cycle++;
  }

  thermo(ncurrent_cycle);

  elapsed = clock()-start;
  printf("\nLoop time of %g seconds for %d cycles with %d particles.\n\n",
      (double)elapsed/CLOCKS_PER_SEC, nrelaxations, nparticles);
  if (logfile) {
    fprintf(logfile, "\nLoop time of %g seconds for %d cycles with %d particles.\n\n", 
      (double)elapsed/CLOCKS_PER_SEC, nrelaxations, nparticles);
  }

  // write out last configuration after relaxation

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
         box->ylo, box->yhi, box->zlo, box->zhi, "relaxed", nrelaxations);

  // production run

  printf("Production run for %d cycles\n", ncycles);
  printf("Cycle Energy AcceptanceRatio dmax\n");

  if (logfile) {
    fprintf(logfile, "\nProduction run for %d cycles\n", ncycles);
    fprintf(logfile, "Cycle Energy AcceptanceRatio dmax\n");
    fflush(logfile);
  }

  start = clock();
  naccepted = 0;
  nrejected = 0;
  niterations = 0;

  for (int n = 0; n < ncycles; n++) {

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++) {

      sampling_nvt(temperature, naccepted, nrejected);

      if (niterations % sampling_period == 0)
        energies.push_back(total_energy);

      niterations++;
    }

    // recompute total energy occasionally

    if (n % sampling_period == 0 || n % dump_period == 0) {
      thermo(ncurrent_cycle);

      if (target_acceptance > 0) {
        double diff = acceptance_ratio - target_acceptance;
        if (diff < -0.05) dmax *= 0.5;
        else if (diff > 0.05) dmax *= 2.0;
      }
    }

    if (n % dump_period == 0) {
      double mean, stdev;
      create_histogram(energies, "hist_energy.txt", mean, stdev);

      io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
         box->ylo, box->yhi, box->zlo, box->zhi, "config", n);
    }

    ncurrent_cycle++;
  }

  thermo(ncurrent_cycle);

  elapsed = clock()-start;
  printf("\nLoop time of %g seconds for %d cycles with %d particles\n",
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  if (logfile) {
    fprintf(logfile, "\nLoop time of %g seconds for%d cycles with %d particles\n", 
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  }

  // final configuration

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "final", ncurrent_cycle);

  // final energy histogram

  double mean, stdev;
  create_histogram(energies, "hist_energy_final.txt", mean, stdev);
  printf("Energy: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Energy: %f +/- %f\n", mean, stdev);
}

/*---------------------------------------------------------------------------*/

void MCNVT::thermo(int n)
{
  compute_total_energy(total_energy, virial);
  total_energy += nparticles * energy_correction;
  acceptance_ratio = (double)naccepted/((double)(naccepted+nrejected));

  printf("%8d %8.3f %0.3f %0.3f\n", n, total_energy, acceptance_ratio, dmax);
  if (logfile) {
    fprintf(logfile, "%8d %8.3f %0.3f %0.3f\n",
      n, total_energy, acceptance_ratio, dmax);
    fflush(logfile);
  }
}

/*---------------------------------------------------------------------------*/

void MCNVT::sampling_nvt(double _temperature, int& naccepted, int& nrejected)
{
  double beta = 1.0/_temperature;

  // randomly pick a particle idx

  while (1) {
    double r = random->random_mars();
    particle_idx = r * nparticles;
    if (particle_idx >= 0 && particle_idx < nparticles) break;
  }

  // compute the current energy of the chosen particle
  // store the idx of the particle in the last cell, used for updating the involved cells

  double current_particle_energy = compute_particle_energy(particle_idx);
  int idx_in_last_cell = idx_in_cell;

  // attempt a trial move to the chosen particle, backup last position
  // the trial move can make the particle move to another cell
  // but we don't need to remove the particle from the cell at this point

  double last_pos[4];
  int rot = 0;
  if (particleList[particle_idx].rot_flag) {
    if (random->random_mars() < 0.5) {
      trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
    } else {
      trial_rotation(&particleList[particle_idx], rmax, last_pos);
      rot = 1;
    }
  } else {
    trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
  }

  // compute the particle energy due to the trial move

  double trial_particle_energy = compute_particle_energy(particle_idx);
  double delta_E = trial_particle_energy - current_particle_energy;

  // accept or reject the trial move
  
  if (delta_E <= 0) { // accept the move

    cell->update(particle_idx, idx_in_last_cell, last_pos);
    total_energy += delta_E;
    naccepted++;

  } else {

    double p = exp(-beta*delta_E);
    if (random->random_mars() < p) { // accept

      cell->update(particle_idx, idx_in_last_cell, last_pos);
      total_energy += delta_E;
      naccepted++;

    } else { // reject
      if (!rot) {
        restore_displacement(&particleList[particle_idx], last_pos);
      } else {
        restore_rotation(&particleList[particle_idx], last_pos);
      }
      particle_energies[particle_idx] = current_particle_energy;
      nrejected++;
    }
  }

}

