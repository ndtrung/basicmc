/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Wang-Landau - implements the Wang-Landau Monte Carlo simulation

  Trung Nguyen (Northwestern)
  Start: Nov 29, 2017
*/

#ifndef _WANG_LANDAU_H
#define _WANG_LANDAU_H

#include "monte_carlo.h"

namespace MC {

class MCWangLandau : public MonteCarlo
{
public:
  MCWangLandau(Particle*& _particleList, class RandomMC* _random, int _nparticles);
  MCWangLandau(class Box* _box, Particle*& _particleList, class RandomMC* _random, int _nparticles);
  ~MCWangLandau();

  void init();
  void setup();
  void run(int nrelaxations, int ncycles);

  void set_energy_bin(const double _dE) { dE = _dE; }
  void set_energy_range(const double _Emin, const double _Emax);
  void set_flatness_threshold(double t) { threshold = t; }

  virtual void sampling_wl();

protected:
  double total_energy;        // total energy
  double virial;              // virial
  int energy_user;            // energy range set by user

  int particle_idx;           // particle index

  double energy_correction;   // energy correction term due to truncation

  double Emin, Emax, dE;      // energy range and energy bin size
  double *hE;                 // energy histogram
  int nbins;                  // number of bins
  double *log_g;              // log of density of states
  double f, log_f;            // modification factor
  double threshold;           // energy histogram flatness threshold

  int naccepted;
  int nrejected;

  void output_histograms(const char* filename);
};

} // namespace MC

#endif

