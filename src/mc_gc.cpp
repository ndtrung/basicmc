/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Grand canonical Monte-Carlo simulation 

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017

  Frenkel's book (Section 7.2, Figure 7.1):
  LJ fluid at rho = 0.4, T = 2.0:
  mu_ex ~ -0.75
  mu_id = k_B T ln(rho \Lamba^3) = -1.832, where k_B = 1, \Lambda = 1
  mu = mu_ex + mu_id ~ -2.582
*/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>

#include "mc_gc.h"
#include "buildingblocks.h"
#include "io.h"
#include "lattice.h"
#include "memory.h"
#include "random_mc.h"

using namespace MC;
using namespace std;

#define DELTA 1000

/*---------------------------------------------------------------------------*/

MCGC::MCGC(Particle*& _particleList, class RandomMC* _random, int _nparticles,
  double _temperature, double _mu)
 : MonteCarlo(_particleList, _random, _nparticles), temperature(_temperature), mu(_mu)
{
  dmax = 0.5;

  naccepted_displacement = 0;
  nrejected_displacement = 0;
  naccepted_insertion = 0;
  nrejected_insertion = 0;
  naccepted_deletion = 0;
  nrejected_deletion = 0;
}

/*---------------------------------------------------------------------------*/

MCGC::MCGC(class Box* _box, Particle*& _particleList, class RandomMC* _random,
  int _nparticles, double _temperature, double _mu)
  : MonteCarlo(_box, _particleList, _random, _nparticles), temperature(_temperature), mu(_mu)
{
  dmax = 0.5;

  naccepted_displacement = 0;
  nrejected_displacement = 0;
  naccepted_insertion = 0;
  nrejected_insertion = 0;
  naccepted_deletion = 0;
  nrejected_deletion = 0;
}

/*---------------------------------------------------------------------------*/

MCGC::~MCGC()
{
}

/*---------------------------------------------------------------------------*/

void MCGC::init()
{
  MonteCarlo::init();

  printf("Running MC grand canonical simulation..\n");
  if (logfile) fprintf(logfile, "Running MC grand canonical simulation..\n");
}

/*---------------------------------------------------------------------------*/

void MCGC::setup()
{
  MonteCarlo::setup();

  double cutoff = evaluator->get_cutoff();
  volume = box->Lx * box->Ly * box->Lz;
  rho = (double)nparticles/volume;
  evaluator->get_energy_correction(cutoff, rho, energy_correction);

  // cell list setup

  cell = new Cell<Particle>(box, particleList, nparticles, 2.0*cutoff);
  cell->setup();

  // compute total energy

  compute_total_energy(total_energy, virial);
  total_energy += nparticles * energy_correction;

  printf("Number of particles = %d\nNumber density = %g\nTemperature = %g\n"
    "Chemical potential = %g\n", nparticles, rho, temperature, mu);
  if (logfile) {
    fprintf(logfile, "Number of particles = %d\nNumber density = %g\n"
      "Temperature = %g\nChemical potential = %g\n", nparticles, temperature,
      rho, mu);
    fflush(logfile);
  }

  // write out the initial configuration

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "config", 0);
}

/*---------------------------------------------------------------------------*/

void MCGC::run(int nrelaxations, int ncycles)
{
  // initialize the particles, if needed

  init();

  if (target_acceptance > 0) {
    printf("WARNING: Adjusting trial moves to achieve target acceptance ratio of %g, "
     "detailed balance is not satisfied.\n", target_acceptance);
  }

  // setting up the simulation run

  setup();

  // relaxation

  printf("Relaxation for %d cycles\n", nrelaxations);
  printf("Cycle Energy NumParticles NumDensity ExcessChemPot "
    "Displ. Ins. Del.\n");

  if (logfile) {
    fprintf(logfile, "Relaxation for %d cycles\n", nrelaxations);
    fprintf(logfile, "Cycle Energy NumParticles NumDensity ExcessChemPot "
      "Displ. Ins. Del.\n");
    fflush(logfile);
  }

  double acceptance_disp, acceptance_insertion, acceptance_deletion;
  double current_energy;
  double mean, stdev;
  std::vector<double> numparticles, energies, densities;
  int ntrials = nparticles;
  double insertion_prob = 0.5;
  int niterations = 0;

  // relaxation to find the average number of particles
  // use equal probabilities for all types of trial moves

  clock_t start, elapsed;
  start = clock();

  for (int n = 0; n < nrelaxations; n++) {

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++) {

      // attempt exchanges

      double r = random->random_mars();
      if (r < insertion_prob)
        current_energy = particle_insertion();
      else
        current_energy = particle_deletion();

      // attempt displacement

      if (nparticles > 0)
        current_energy = particle_displacement();
    }

    if (n % dump_period == 0 || n % sampling_period == 0)
      thermo(ncurrent_cycle);

    if (n % sampling_period == 0)
      numparticles.push_back(nparticles);

    ncurrent_cycle++;
  }

  thermo(nrelaxations);

  elapsed = clock()-start;

  // write out last configuration after relaxation

  nNumTotalBeads = nparticles;
  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
         box->ylo, box->yhi, box->zlo, box->zhi, "relaxed", nrelaxations);

  // find the average number of particles

  create_histogram(numparticles, "NULL", mean, stdev);

  // ntrials = number of attempts per cycles 
  //         = the average number of particles + average number of exchanges

  int numparticles_ave = mean;
  int numexchanges_ave = mean;
  ntrials = numparticles_ave + numexchanges_ave;

  elapsed = clock()-start;
  printf("\nLoop time of %f seconds for %d cycles with %d particles on average\n",
      (double)elapsed/CLOCKS_PER_SEC, nrelaxations, numparticles_ave);
  if (logfile) {
    fprintf(logfile, "\nLoop time of %f seconds for%d cycles with %d particles on average\n", 
      (double)elapsed/CLOCKS_PER_SEC, nrelaxations, numparticles_ave);
  }

  // production run
  niterations = 0;

  printf("\nProduction run for %d cycles\n", ncycles);
  printf("Cycle Energy NumParticles NumDensity ExcessChemPot "
    "Displ. Ins. Del.\n");

  if (logfile) {
    fprintf(logfile, "\nProduction run for %d cycles\n", ncycles);
    fprintf(logfile, "Cycle Energy NumParticles NumDensity ExcessChemPot "
      "Displ. Ins. Del.\n");
    fflush(logfile);
  }

  start = clock();

  for (int n = 0; n < ncycles; n++) {

    if (n % dump_period == 0 || n % sampling_period == 0)
      thermo(ncurrent_cycle);

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++) {

      sampling_gc(ntrials, insertion_prob, numparticles_ave);
      
      if (niterations % sampling_period == 0) {
        numparticles.push_back(nparticles);
        densities.push_back(rho);
        energies.push_back(total_energy);
      }
      niterations++;
    }


    if (n % sampling_period == 0) {   
      create_histogram(energies, "hist_energy.txt", mean, stdev);
      create_histogram(densities, "hist_density.txt", mean, stdev);
    }

    if (n % dump_period == 0) {
      nNumTotalBeads = nparticles;
      io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
         box->ylo, box->yhi, box->zlo, box->zhi, "config", n);
    }

    ncurrent_cycle++;
  }

  thermo(ncurrent_cycle);
  
  elapsed = clock()-start;
  printf("\nLoop time of %g seconds for %d cycles with %d particles\n",
    (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  if (logfile) {
    fprintf(logfile, "\nLoop time of %g seconds for %d cycles with %d particles\n", 
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  }

  // final configuration

  nNumTotalBeads = nparticles;
  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "final", ncurrent_cycle);

  // final histograms

  create_histogram(energies, "hist_energy_final.txt", mean, stdev);
  printf("Energy: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Energy: %f +/- %f\n", mean, stdev);

  create_histogram(densities, "hist_density_final.txt", mean, stdev);
  printf("Density: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Density: %f +/- %f\n", mean, stdev);
}

/*---------------------------------------------------------------------------*/

void MCGC::thermo(int n)
{
  double cutoff = evaluator->get_cutoff();
  volume = box->Lx * box->Ly * box->Lz;
  rho = (double)nparticles/volume;
  evaluator->get_energy_correction(cutoff, rho, energy_correction);
  compute_total_energy(total_energy, virial);
  total_energy += nparticles * energy_correction;

  double acceptance_disp = (double)naccepted_displacement /
    (double)(naccepted_displacement+nrejected_displacement);
  double acceptance_insertion = 0;
  if (naccepted_insertion+nrejected_insertion > 0)
    acceptance_insertion = (double)naccepted_insertion /
      (double)(naccepted_insertion+nrejected_insertion);
  double acceptance_deletion = 0;
  if (naccepted_deletion+nrejected_deletion > 0)
    acceptance_deletion = (double)naccepted_deletion /
       (double)(naccepted_deletion+nrejected_deletion);

  double mu_exc = mu - temperature*log(rho);
  printf("%6d %8g %8d %8.3f %8.3f %8.3f %0.3f %0.3f\n", n, total_energy,
    nparticles, rho, mu_exc, acceptance_disp, acceptance_insertion, acceptance_deletion);
  if (logfile) {
    fprintf(logfile, "%6d %8g %8d %8.3f %8.3f %8.3f %0.3f %0.3f\n", n, total_energy,
      nparticles, rho, mu_exc, acceptance_disp, acceptance_insertion, acceptance_deletion);
    fflush(logfile);
  }
}

/*---------------------------------------------------------------------------*/

void MCGC::sampling_gc(int ntrials, double insertion_prob, int numparticles_ave)
{
  double current_energy;
  int r = (int)(random->random_mars() * ntrials);
  if (r < numparticles_ave) {
    if (nparticles > 0) current_energy = particle_displacement();
  } else {
    double p = random->random_mars();
    if (p < insertion_prob)
      current_energy = particle_insertion();
    else
      current_energy = particle_deletion();
  }
}

/*---------------------------------------------------------------------------*/

double MCGC::particle_displacement()
{
  if (nparticles <= 0) return 0;

  double current_energy, trial_energy;
  const double beta = 1.0/temperature;

  // randomly pick a particle idx

  while (1) {
    double r = random->random_mars();
    particle_idx = r * nparticles;
    if (particle_idx >= 0 && particle_idx < nparticles) break;
  }

  // compute the current energy of the chosen particle

  current_energy = compute_particle_energy(particle_idx);
  int idx_in_last_cell = idx_in_cell;

  // attempt a trial move to the chosen particle, backup last position

  double last_pos[4];
  int rot = 0;
  if (particleList[particle_idx].rot_flag) {
    if (random->random_mars() < 0.5) {
      trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
    } else {
      trial_rotation(&particleList[particle_idx], rmax, last_pos);
      rot = 1;
    }
  } else {
    trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
  }

  // compute the particle energy due to the trial move

  trial_energy = compute_particle_energy(particle_idx);

  double delta_E = trial_energy - current_energy;
  if (delta_E <= 0) { // accept the move

    cell->update(particle_idx, idx_in_last_cell, last_pos);
    current_energy = trial_energy;
    total_energy += delta_E;
    naccepted_displacement++;

  } else {

    double p = exp(-beta*delta_E);
    if (random->random_mars() < p) { // accept

      cell->update(particle_idx, idx_in_last_cell, last_pos);
      current_energy = trial_energy;
      total_energy += delta_E;
      naccepted_displacement++;

    } else { // reject
      if (!rot) {
        restore_displacement(&particleList[particle_idx], last_pos);
      } else {
        restore_rotation(&particleList[particle_idx], last_pos);
      }
      particle_energies[particle_idx] = current_energy;
      nrejected_displacement++;
    }
  }

  return current_energy;
}

/*---------------------------------------------------------------------------*/

double MCGC::particle_insertion()
{
  double r, trial_x, trial_y, trial_z;
  double beta = 1.0/temperature;
  volume = box->Lx * box->Ly * box->Lz;

  // generate a trial particle

  r = random->random_mars();
  trial_x = box->xlo + r * box->Lx;
  r = random->random_mars();
  trial_y = box->ylo + r * box->Ly;
  r = random->random_mars();
  trial_z = box->zlo + r * box->Lz;

  if (nparticles == nmax) {
    nmax += DELTA;
    grow_arrays(nmax);
  }

  int trial_nparticles = nparticles + 1;
  particleList[nparticles].beadList[0].x = trial_x;
  particleList[nparticles].beadList[0].y = trial_y;
  particleList[nparticles].beadList[0].z = trial_z;

  // put the new particle into cell

  int cell_idx = cell->add_particle(nparticles);

  // increment the number of particles by 1

  nparticles++;

  // compute the energy due to the trial insertion of the last particle

  double trial_energy = compute_particle_energy(nparticles-1);

  // energy difference due to the insertion of the chosen particle
  // delta_E = U(N+1) - U(N)

  double delta_E = trial_energy;
  double Lambda = 1.0;  // thermal de Broglie wavelength in LJ units
  // mu = mu_id + mu_exc, where mu_id = kT log(rho Lambda^3) = kT log(rho) + 3 kT log(Lambda)
  // so to plot the mu_exc vs rho curve, subtract kT log(rho) from mu
  double mu_rel = mu - 3*temperature*log(Lambda);

  // acceptance probability P_acc = min[1; exp(boltzfactor)] = exp[ min[0; boltzfactor] ]
  // Smit and Frenkel's book: Section 5.6.2, Eq. (5.6.8)

  double boltzfactor = exp(-beta*delta_E + beta*mu_rel) *
    volume/(double)(trial_nparticles);

  if (random->random_mars() < boltzfactor) { // accept

    total_energy += delta_E;
    naccepted_insertion++;

  } else {  // reject

    cell->remove_last_particle(cell_idx);
    nparticles--; // recover the number of particles
    nrejected_insertion++;
  }

  // update energy correction due to change in number density

  rho = (double)nparticles/volume;
  double cutoff = evaluator->get_cutoff();
  evaluator->get_energy_correction(cutoff, rho, energy_correction);

  return total_energy;
}

/*---------------------------------------------------------------------------*/

double MCGC::particle_deletion()
{
  if (nparticles <= 0) return 0;

  double beta = 1.0/temperature;
  volume = box->Lx * box->Ly * box->Lz;

  // randomly pick a particle idx

  while (1) {
    double r = random->random_mars();
    particle_idx = r * nparticles;
    if (particle_idx >= 0 && particle_idx < nparticles) break;
  }

  // compute the energy contribution due to the particle to the total energy

  double trial_energy = compute_particle_energy(particle_idx);
  int last_cell_idx = cell_idx;
  int idx_in_last_cell = idx_in_cell;

  // energy difference due to the deletion of the chosen particle
  // delta_E = U(N-1) - U(N)

  double delta_E = -trial_energy;
  double Lambda = 1.0; // thermal de Broglie wavelength in LJ units
  // mu = mu_id + mu_exc, where mu_id = kT log(rho Lambda^3) = kT log(rho) + 3 kT log(Lambda)
  // so to plot the mu_exc vs rho curve, subtract kT log(rho) from mu
  double mu_rel = mu - 3*temperature*log(Lambda);

  // acceptance probability P_acc = min[1; exp(boltzfactor)] = exp[ min[0; boltzfactor] ]
  // Smit and Frenkel's book: Section 5.6.2, Eq. (5.6.9)

  double boltzfactor = exp(-beta*delta_E - beta*mu_rel) *
    (double)(nparticles)/volume;

  if (random->random_mars() < boltzfactor) { // accept the deletion

    remove_particle(particle_idx, last_cell_idx, idx_in_last_cell);
    total_energy += delta_E;
    naccepted_deletion++;

  } else {  // reject

    nrejected_deletion++;
  }

  // update energy correction due to change in number density

  rho = (double)nparticles/volume; 
  double cutoff = evaluator->get_cutoff();
  evaluator->get_energy_correction(cutoff, rho, energy_correction);

  return total_energy;
}

/*---------------------------------------------------------------------------
  remove the particle from the particle list
  also update the cell
-----------------------------------------------------------------------------*/

void MCGC::remove_particle(int particle_idx, int last_cell_idx, int idx_in_last_cell)
{
  // remove the particle_idx from the last cell that owns it

  cell->remove_particle(last_cell_idx, idx_in_last_cell);

  // replace the last particle (nparticles-1) in the cell list with particle_idx

  cell->replace_particle(particle_idx, nparticles-1);

  // replace the chosen particle with the last particle in the particle list

  particleList[particle_idx] = particleList[nparticles-1];
  nparticles--;

  // update the cell list's number of particles (used for binning and total energy calculation)

  cell->set_num_particles(nparticles);
}

/*---------------------------------------------------------------------------
  reallocate particle list by new_size
-----------------------------------------------------------------------------*/

void MCGC::grow_arrays(const int new_size)
{
  Particle* tmp = new Particle [new_size];
  for (int i = 0; i < nparticles; i++) {
    tmp[i] = particleList[i]; 
  }
  delete [] particleList;
  particleList = tmp;

  grow(particle_energies, new_size);
  grow(particle_virials, new_size);
}


