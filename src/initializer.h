/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Initializer class - generic initialization fo box and particle coordinates

  Trung Nguyen (Northwestern)
  Start: Dec 10, 2017
*/

#ifndef _INITIALIZER_H
#define _INITIALIZER_H

namespace MC {

template <class ParticleType>
class Initializer
{
public:
  Initializer() {}
  virtual ~Initializer() {}

  virtual void create_box(class Box*& box, ParticleType*&, int,
    double, double, double, double, double, double);
 

protected:

};

} // namespace MC

#endif

