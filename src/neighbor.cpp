/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Neighbor list implementation

  Trung Nguyen (Northwestern)
  Start: Apr 20, 2018
*/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>

#include "memory.h"
#include "monte_carlo.h"
#include "buildingblocks.h"
#include "lattice.h"
#include "neighbor.h"

using namespace MC;
using namespace std;

#define MAX_NEIGHBORS 100

/*---------------------------------------------------------------------------*/

Neighbor::Neighbor(class Box* _box, class Particle*& _particleList, int _nparticles) :
  box(_box), particleList(_particleList), nparticles(_nparticles)
{
  bins = NULL;
  binhead = NULL;
  stencil = NULL;
  nstencil = 0;
  neighbors = NULL;
  num_neighbors = NULL;
  allocated_size = MAX_NEIGHBORS;
}

/*---------------------------------------------------------------------------*/

Neighbor::~Neighbor()
{
	destroy(bins);
	destroy(binhead);
	destroy(stencil);
  destroy(neighbors);
  destroy(num_neighbors);
}

/*---------------------------------------------------------------------------*/

void Neighbor::setup()
{
	int i, j, k, nbins, sx, sy, sz;
	double cutneighmaxsq = cutneighmax * cutneighmax;
	double binsize_optimal = cutneighmax;
  double binsizeinv = 1.0/binsize_optimal;

	nbinx = static_cast<int> (box->Lx*binsizeinv);
	nbiny = static_cast<int> (box->Ly*binsizeinv);
	nbinz = static_cast<int> (box->Lz*binsizeinv);

	if (nbinx == 0) nbinx = 1;
	if (nbiny == 0) nbiny = 1;
	if (nbinz == 0) nbinz = 1;

	binsizex = box->Lx / nbinx;
	binsizey = box->Ly / nbiny;
	binsizez = box->Lz / nbinz;

	sx = static_cast<int> (cutneighmax/binsizex);
	if (sx*binsizex < cutneighmax) sx++;
	sy = static_cast<int> (cutneighmax/binsizey);
	if (sy*binsizey < cutneighmax) sy++;
	sz = static_cast<int> (cutneighmax/binsizez);
	if (sz*binsizez < cutneighmax) sz++;

	int smax = (2*sx+1) * (2*sy+1) * (2*sz+1);

	nbins = nbinx * nbiny * nbinz;

  if (bins) destroy(bins);
  if (binhead) destroy(binhead);
  if (stencil) destroy(stencil);

	create(bins, nparticles);
	create(binhead, nbins);
	create(stencil, nbins, smax);
		
	int nx, ny, nz, ibin;
	
	// z, y then x
	for (nx = 0; nx < nbinx; nx++)		
		for (ny = 0; ny < nbiny; ny++)
			for (nz = 0; nz < nbinz; nz++) {
				nstencil = 0;
				ibin = nx * nbiny * nbinz + ny * nbinz + nz;
				
				for (i = 0; i <= sx; i++)
					for (j = -sy; j <= sy; j++)
						for (k = -sz; k <= sz; k++) {
							if (i > 0 || j > 0 || (j == 0 && k > 0)) {
								if (bin_distance(i, j, k) < cutneighmaxsq)
									stencil[ibin][nstencil++] = ((nx + i + nbinx) % nbinx) * nbiny * nbinz + 
										((ny + j + nbiny) % nbiny) * nbinz + (nz + k + nbinz) % nbinz;
							}
						}
			}

  // allocate the neighbor lists

  create(neighbors, nparticles, allocated_size);
  create(num_neighbors, nparticles);
}

/*---------------------------------------------------------------------------*/

void Neighbor::set_cutoff_neigh(double cutoff, double skin)
{
  cutneighmax = cutoff;
  skin2 = (cutoff + skin) * (cutoff + skin);
  printf("cutneighmax = %f; skin2 = %f\n", cutoff, skin2);
}

/*---------------------------------------------------------------------------*/

double Neighbor::bin_distance(int i, int j, int k)
{
	double delx, dely, delz;

	if (i > 0) delx = (i-1)*binsizex;
	else if (i == 0) delx = 0.0;
	else delx = (i+1)*binsizex;

	if (j > 0) dely = (j-1)*binsizey;
	else if (j == 0) dely = 0.0;
	else dely = (j+1)*binsizey;

	if (k > 0) delz = (k-1)*binsizez;
	else if (k == 0) delz = 0.0;
	else delz = (k+1)*binsizez;
 
	return (delx*delx + dely*dely + delz*delz);
}

/*---------------------------------------------------------------------------*/

void Neighbor::build()
{
  int i,j,k,ibin;
	const double Lx = box->Lx;
  const double Ly = box->Ly;
  const double Lz = box->Lz;
  const double xlo = box->xlo;
  const double ylo = box->ylo;
  const double zlo = box->zlo;

	for (i = 0; i < nparticles; i++) num_neighbors[i] = 0;

	bin_atoms();
	
	for (i = 0; i < nparticles; i++) {
		const double xtmp = particleList[i].beadList[0].x;
    const double ytmp = particleList[i].beadList[0].y;
    const double ztmp = particleList[i].beadList[0].z;
		
 		for (j = bins[i]; j >= 0; j = bins[j]) {
			const Bead* const bead = &particleList[j].beadList[0];

      double delx = bead->x - xtmp;
      double dely = bead->y - ytmp;
      double delz = bead->z - ztmp;

      // minimum image convention

      if (delx < -0.5*Lx) delx += Lx;
      else if (delx > 0.5*Lx) delx -= Lx;
      if (dely < -0.5*Ly) dely += Ly;
      else if (dely > 0.5*Ly) dely -= Ly;
      if (delz < -0.5*Lz) delz += Lz;
      else if (delz > 0.5*Lz) delz -= Lz;
      double rsq = delx * delx + dely * dely + delz * delz;
			
			if (rsq < skin2)	{
				neighbors[i][num_neighbors[i]++] = j;
				neighbors[j][num_neighbors[j]++] = i;
			}
		}
		
		ibin = coord2bin(xtmp, ytmp, ztmp, xlo, ylo, zlo);

		for (k = 0; k < nstencil; k++) {
			for (j = binhead[stencil[ibin][k]]; j >= 0; j = bins[j]) {
				
        const Bead* const bead = &particleList[j].beadList[0];
        double delx = bead->x - xtmp;
        double dely = bead->y - ytmp;
        double delz = bead->z - ztmp;

        // minimum image convention

        if (delx < -0.5*Lx) delx += Lx;
        else if (delx > 0.5*Lx) delx -= Lx;
        if (dely < -0.5*Ly) dely += Ly;
        else if (dely > 0.5*Ly) dely -= Ly;
        if (delz < -0.5*Lz) delz += Lz;
        else if (delz > 0.5*Lz) delz -= Lz;
        double rsq = delx * delx + dely * dely + delz * delz;
				
				if (rsq < skin2) {
  				neighbors[i][num_neighbors[i]++] = j;
  				neighbors[j][num_neighbors[j]++] = i;

          if (num_neighbors[i] == MAX_NEIGHBORS ||
              num_neighbors[j] == MAX_NEIGHBORS) {
            allocated_size += MAX_NEIGHBORS;
            grow(neighbors, nparticles, allocated_size);
          }
				}
			}
		}
		
	}

}

/*---------------------------------------------------------------------------*/

void Neighbor::bin_atoms()
{
  const double xlo = box->xlo;
  const double ylo = box->ylo;
  const double zlo = box->zlo;
	const int nbins = nbinx*nbiny*nbinz;

	for (int i = 0; i < nbins; i++) binhead[i] = -1;

  for (int i = 0; i < nparticles; i++) {
    const double xtmp = particleList[i].beadList[0].x;
    const double ytmp = particleList[i].beadList[0].y;
    const double ztmp = particleList[i].beadList[0].z;

    int ibin = coord2bin(xtmp, ytmp, ztmp, xlo, ylo, zlo);	
      
		bins[i] = binhead[ibin];
		binhead[ibin] = i;
  }
}

/*---------------------------------------------------------------------------*/

int Neighbor::coord2bin(const double x, const double y, const double z,
  const double xlo, const double ylo, const double zlo)
{
  return (int)((x - xlo) / binsizex) * nbiny * nbinz +
  	(int)((y - ylo) / binsizey) * nbinz +
  	(int)((z - zlo) / binsizez);
}

