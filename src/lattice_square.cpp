/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Trung Nguyen (Northwestern)
  Start: Aug 15, 2017
*/

#include "lattice_square.h"
#include "memory.h"

using namespace MC;
using namespace std;

/*---------------------------------------------------------------------------*/

LatticeSquare::LatticeSquare(int _size) : Lattice(2), nx(_size), ny(_size)
{
  xlo = 0;
  xhi = nx;
  ylo = 0;
  yhi = ny;
  zlo = 0;
  zhi = 1;
  Lx = nx;
  Ly = ny;
  num_vertices = nx * ny;
  create(vertexList, num_vertices);
}



