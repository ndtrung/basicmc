/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Cell list implementation

  Trung Nguyen (Northwestern)
  Start: Apr 29, 2018
*/

#ifndef _CELL_H
#define _CELL_H

#include "simulation.h"

namespace MC {

template <typename PARTICLE>
class Cell
{
public:
  Cell(class Box*& _box, PARTICLE*& _particleList, int _nparticles, double cellsize);
  ~Cell();

  // create the cell list and the neighboring cells of individual cells
  void setup();

  // update the involved cells when a particle moves
  void update(const int particle_idx, const int idx_in_last_cell, double* last_pos);

  // bin the particles into the cells
  void bin_particles();

  // add a (new) particle (whose index is particle_idx) to the correct cell
  int add_particle(int particle_idx);

  // add a particle whose index is particle_idx (back) to the cell idx
  void add_particle(int particle_idx, int cell_idx);

  // remove the last particle of the cell cell_idx

  void remove_last_particle(int cell_idx);

  // remove the particle whose index is particle_idx with idx in some cell

  void remove_particle(int cell_idx, int idx_in_cell);

  // replace particle_idx_dest with particle_idx_src (used with particle deletion)

  void replace_particle(int particle_idx_src, int particle_idx_dest);

  // compute the cell idx given a particle's coordinates and the box lower left corner
  int coord2cell(const double, const double, const double,
    const double, const double, const double);

  int get_ncells() { return ncells; }
  void set_cell_size(double value) { cutneighmax = value; }
  void set_num_particles(int n) { nparticles = n; }
  void print_statistics();

  int** cells;                // cells[i][j] returns the jth particle in cell i
  int** neighbor_cells;       // neighbor_cells[i][j] returns the jth neighboring cell of cell i
  int* num_neighbor_cells;    // num_neighbor_cells[i] return number of neighboring cell of cell i
  int* num_particles_in_cell; // return number of particles in cell i

protected:
  Box* box;
  PARTICLE*& particleList;
  int nparticles;

  double cellsizex;
  double cellsizey;
  double cellsizez;
  int ncellx;
  int ncelly;
  int ncellz;
  int ncells;

  double cutneighmax;
  int allocated_size;

  void deallocate_arrays();

};
}

#endif
