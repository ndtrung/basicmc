/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __INPUT_OUTPUT
#define __INPUT_OUTPUT

#include <vector>
#include "buildingblocks.h"

namespace MC {

class IOHandle
{
public:
  IOHandle() {}
  virtual ~IOHandle() {}

  virtual int read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
            double& Lx, double& Ly, double& Lz, std::vector<Bead>& beadList, int& timestep)
  {
    return 1;
  }

  virtual int read(const char* fileName, int& nNumTotalBeads,
            double& Lx, double& Ly, double& Lz, std::vector<Bead>& beadList, const int nframe)
  {
    return 1;
  }

  virtual void write(const std::vector<Particle>& particleList, int nNumTotalBeads,
            double xlo, double xhi, double ylo, double yhi, double zlo, double zhi,
            const char* fileName, int timestep)
  {
  }

  virtual void write(Particle* particleList, int nparticles, int nNumTotalBeads,
            double xlo, double xhi, double ylo, double yhi, double zlo, double zhi,
            const char* fileName, int timestep)
  {
  }

  void parse(char* line, char* copy, int& narg, char** arg) {

  	// make a copy to work on

	  strcpy(copy, line);

  	// strip any # comment by resetting string terminator
	  // do not strip # inside double quotes
	
	  int level = 0;
  	char *ptr = copy;
	  while (*ptr) {
  		if (*ptr == '#' && level == 0) {
		  	*ptr = '\0';
		  	break;
      }  
		
		  ptr++;
	  }

  	// entry = 1st arg
  	arg[0] = strtok(copy," \t\n\r\f");
  	if (arg[0] == NULL) return;

  	// point arg[] at each subsequent arg
  	// treat text between double quotes as one arg
  	// insert string terminators in copy to delimit args

    narg = 1;
    while (1) {
      arg[narg] = strtok(NULL," \t\n\r\f");
      if (arg[narg])
  		  narg++;
      else 
        break;
    }
  }

};

//! XYZ files
class XYZHandle : public IOHandle
{
public:
  XYZHandle() : IOHandle() {}
  ~XYZHandle() {}

  int read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
           double& Lx, double& Ly, double& Lz, std::vector<Particle>& particleList, int& timestep);

  void write(const std::vector<Particle>& particleList, int nNumTotalBeads,
             double xlo, double xhi, double ylo, double yhi, double zlo, double zhi,
             const char* fileName, int timestep);
};

//! HOOMD XML files
class XMLHandle : public IOHandle
{
public:
  XMLHandle() : IOHandle() {}
  ~XMLHandle() {}

  int read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
           double& Lx, double& Ly, double& Lz, std::vector<Particle>& particleList, int& timestep);

  void write(const std::vector<Particle>& particleList, int nNumTotalBeads,
             double xlo, double xhi, double ylo, double yhi, double zlo, double zhi,
             const char* fileName, int timestep);
};

//! LAMMPS DUMP files
class DUMPHandle : public IOHandle
{
public:
  DUMPHandle() : IOHandle()
  {
    nfields = 0; fields = NULL;
  }

  ~DUMPHandle()
  {
    if (fields) delete [] fields;
  }

  int read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
           double& Lx, double& Ly, double& Lz, std::vector<Particle>& particleList, int& timestep);

  void write(Particle* particleList, int nparticles, int nNumTotalBeads,
             double xlo, double xhi, double ylo, double yhi, double zlo, double zhi,
             const char* fileName, int timestep);
  int* fields;
  int nfields;
};

void extract(const char* fileName, int local, int nNumBeadsInChain, char* outfile);

}

#endif
