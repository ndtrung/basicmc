/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Trung Nguyen (Northwestern)
  Start: Aug 15, 2017
*/

#ifndef _LATTICE_CUBIC_H
#define _LATTICE_CUBIC_H

#include "lattice.h"

namespace MC {

class LatticeCubic : public Lattice
{
public:
  LatticeCubic(int, int, int);
  ~LatticeCubic() {}

protected:
  int nx, ny, nz;
  void generate_edges();

};

} // namespace MC

#endif

