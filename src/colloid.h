/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Colloidal interaction:
    V(r) = +infinity for r < sigma
         = -epsilon exp(kappa sigma(1-r/sigma)) / (r/sigma)  for sigma < r < cutoff
         = 0         for r >= cutoff

    epsilon > 0

  Trung Nguyen (Northwestern)
  Start: Jun 19, 2021
*/

#ifndef _COLLOID_H
#define _COLLOID_H

#include <cmath>
#include "evaluator.h"
#include "lattice.h"

namespace MC {

#define BIG 1.0e+20

class Colloid : public Evaluator
{
public:
  Colloid() {}
  Colloid(double _sigma, double _epsilon, double _kappa, double _cutoff) { 
    sigma = _sigma;
    epsilon = _epsilon;
    kappa = _kappa;
    cutoff = _cutoff;

    cutsq_hardsphere = sigma*sigma;
    cutsq = cutoff*cutoff;
  }
  virtual ~Colloid() {}

  void set_params(double _sigma, double _epsilon, double _kappa, double _cutoff) {
    sigma = _sigma;
    epsilon = _epsilon;
    kappa = _kappa;
    cutoff = _cutoff;

    cutsq_hardsphere = sigma*sigma;
    cutsq = cutoff*cutoff;
  }

  void compute(const double rsq, double& energy) {
    if (rsq < cutsq_hardsphere) {
      energy = BIG;
    } else if (rsq < cutsq) {
      double r = sqrt(rsq)/sigma;
      energy = -epsilon*exp(kappa*sigma*(1-r)) / r;
    } else energy = 0;
  }

protected:
  double epsilon, sigma, kappa;
  double cutsq_hardsphere;
  double cutsq;
};

} // namespace MC

#endif

