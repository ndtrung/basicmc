/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Base class for Monte-Carlo simulation methods

  Trung Nguyen (Northwestern)
  Start: Aug 15, 2017
*/

#include <stdlib.h>
#include <fstream>
#include <cmath>
#include <string.h>
#include <omp.h>

#include "memory.h"
#include "monte_carlo.h"
#include "buildingblocks.h"
#include "lattice.h"
#include "io.h"
#include "random_mc.h"

using namespace MC;
using namespace std;

#define VERSION "8Jul21"

enum {ID=0,MOL=1,TYPE=2,Q=3,X=4,Y=5,Z=6};

/*---------------------------------------------------------------------------*/

MonteCarlo::MonteCarlo(Particle*& _particleList, class RandomMC* _random,
  int _nparticles) : particleList(_particleList), random(_random), nparticles(_nparticles)
{
  evaluator = NULL;
  cell = NULL;
  io_handle = NULL;
  logfile = NULL;
  particle_energies = NULL;
  particle_virials = NULL;

  sampling_period = 100;
  dump_period = 100;
  ncurrent_cycle = 0;
  nmax = nparticles;

  target_acceptance = -1.0;
  box_changed = 0;
  anisotropic_flag = 0;
}

/*---------------------------------------------------------------------------*/

MonteCarlo::MonteCarlo(class Box* _box, Particle*& _particleList,
    class RandomMC* _random, int _nparticles)
 : box(_box), particleList(_particleList), random(_random), nparticles(_nparticles)
{
  evaluator = NULL;
  cell = NULL;
  io_handle = NULL;
  logfile = NULL;
  particle_energies = NULL;
  particle_virials = NULL;

  sampling_period = 100;
  dump_period = 100;
  ncurrent_cycle = 0;
  nmax = nparticles;

  target_acceptance = -1.0;
  box_changed = 0;
  anisotropic_flag = 0;
}

/*---------------------------------------------------------------------------*/

MonteCarlo::~MonteCarlo()
{
  if (evaluator) delete evaluator;
  if (cell) delete cell;
  if (box) delete box;
  if (io_handle) delete io_handle;
  destroy(particle_energies);
  destroy(particle_virials);

  if (logfile) {
    fclose(logfile);
    logfile = NULL;
  }
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::init()
{
  MonteCarlo::print_citations();

  // create the output handle

  io_handle = new DUMPHandle;
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::setup()
{
  // check if there is any valid evaluator (force field) defined

  if (!evaluator) {
    printf("ERROR: There is no interaction defined for the simulation!\n");
    if (logfile) {
      fprintf(logfile, "ERROR: There is no interaction defined for the simulation!\n");
      fclose(logfile);
    }
    exit(1);
  }

  // total number of single bead particles

  nNumTotalBeads = nparticles;

  // random number generator warm-up

  for (int i = 0; i < 100; i++) random->random_mars();

  // allocate particle energies

  allocated_size = nparticles;
  create(particle_energies, allocated_size);
  create(particle_virials, allocated_size);
}

/*---------------------------------------------------------------------------*/

double MonteCarlo::compute_particle_energy(const int idx)
{
  double eng = 0;
  compute_particle_energy_cell(idx, eng);
  return eng;
}

/*---------------------------------------------------------------------------
  compute particle energy with N^2
-----------------------------------------------------------------------------*/

void MonteCarlo::compute_particle_energy_nsq(const int particle_idx, double& eng)
{
  const double cutoff = evaluator->get_cutoff();
  const double cutsq = cutoff*cutoff;
  const double xtmp = particleList[particle_idx].beadList[0].x;
  const double ytmp = particleList[particle_idx].beadList[0].y;
  const double ztmp = particleList[particle_idx].beadList[0].z;
  const double Lx = box->Lx;
  const double Ly = box->Ly;
  const double Lz = box->Lz;

  eng = 0;

  for (int i = 0; i < nparticles; i++) {

    const Bead* const bead = &particleList[i].beadList[0];
    double delx = bead->x - xtmp;
    double dely = bead->y - ytmp;
    double delz = bead->z - ztmp;

    // minimum image convention

    if (delx < -0.5*Lx) delx += Lx;
    else if (delx > 0.5*Lx) delx -= Lx;
    if (dely < -0.5*Ly) dely += Ly;
    else if (dely > 0.5*Ly) dely -= Ly;
    if (delz < -0.5*Lz) delz += Lz;
    else if (delz > 0.5*Lz) delz -= Lz;

    double rsq = delx * delx + dely * dely + delz * delz;
    if (i != particle_idx && rsq < cutsq && rsq > 0) {
      double e;
      evaluator->compute(rsq, e);
      eng += e;
    }
  }

  particle_energies[particle_idx] = eng;
  particle_virials[particle_idx] = 0;
}

/*---------------------------------------------------------------------------
  compute particle energy using the cells
-----------------------------------------------------------------------------*/

void MonteCarlo::compute_particle_energy_cell(const int particle_idx, double& eng)
{
  const double cutoff = evaluator->get_cutoff();
  const double cutsq = cutoff*cutoff;
  const double xtmp = particleList[particle_idx].beadList[0].x;
  const double ytmp = particleList[particle_idx].beadList[0].y;
  const double ztmp = particleList[particle_idx].beadList[0].z;
  const double xlo = box->xlo;
  const double ylo = box->ylo;
  const double zlo = box->zlo;
  const double Lx = box->Lx;
  const double Ly = box->Ly;
  const double Lz = box->Lz;

  int** const cells = cell->cells;
  int** const neighbor_cells = cell->neighbor_cells;
  int* const num_neighbor_cells = cell->num_neighbor_cells;
  int* const num_particles_in_cell = cell->num_particles_in_cell;
  
  // note that the particle may come from another cell with its coordinates
  // need to check if particle_idx is the same as those from the cell c
  //   and its neighboring cells

  cell_idx = cell->coord2cell(xtmp, ytmp, ztmp, xlo, ylo, zlo);

  // loop through the particles within the cell c

  eng = 0;

  for (int i = 0; i < num_particles_in_cell[cell_idx]; i++) {    
    int j = cells[cell_idx][i];
    if (j == particle_idx) {
      idx_in_cell = i;     // store the idx of the particle in the cell
      continue;
    }

    const Bead* const bead = &particleList[j].beadList[0];
    double delx = bead->x - xtmp;
    double dely = bead->y - ytmp;
    double delz = bead->z - ztmp;

    // minimum image convention

    if (delx < -0.5*Lx) delx += Lx;
    else if (delx > 0.5*Lx) delx -= Lx;
    if (dely < -0.5*Ly) dely += Ly;
    else if (dely > 0.5*Ly) dely -= Ly;
    if (delz < -0.5*Lz) delz += Lz;
    else if (delz > 0.5*Lz) delz -= Lz;

    double rsq = delx * delx + dely * dely + delz * delz;
    if (rsq < cutsq && rsq > 0) {
      double e;
      evaluator->compute(rsq, e);
      eng += e;
    }

  }  // end within the cell
   
  // now condering in the neighboring cells

  for (int n = 1; n < num_neighbor_cells[cell_idx]; n++) {
    int cn = neighbor_cells[cell_idx][n];
    if (cn == cell_idx) continue;

    // loop through each bead in the cell[nbr]
    for (int i = 0; i < num_particles_in_cell[cn]; i++) {
      int j = cells[cn][i];
      if (j == particle_idx) {
        idx_in_cell = i;     // store the idx of the particle in the cell
        continue;
      }

      const Bead* const bead = &particleList[j].beadList[0];
      double delx = bead->x - xtmp;
      double dely = bead->y - ytmp;
      double delz = bead->z - ztmp;
 
      // minimum image convention

      if (delx < -0.5*Lx) delx += Lx;
      else if (delx > 0.5*Lx) delx -= Lx;
      if (dely < -0.5*Ly) dely += Ly;
      else if (dely > 0.5*Ly) dely -= Ly;
      if (delz < -0.5*Lz) delz += Lz;
      else if (delz > 0.5*Lz) delz -= Lz;

      double rsq = delx * delx + dely * dely + delz * delz;
      if (rsq < cutsq && rsq > 0) {
        double e;
        evaluator->compute(rsq, e);
        eng += e;
      }
    }
  }   // end loops over neighbor cells

  particle_energies[particle_idx] = eng;
  particle_virials[particle_idx] = 0;
}

/*---------------------------------------------------------------------------
  compute particle energy and virial
    return also cell_idx and idx_in_cell
-----------------------------------------------------------------------------*/

void MonteCarlo::compute_particle_energy_cell(const int particle_idx,
  double& eng, double& virial)
{
  const double cutoff = evaluator->get_cutoff();
  const double cutsq = cutoff*cutoff;
  const double xtmp = particleList[particle_idx].beadList[0].x;
  const double ytmp = particleList[particle_idx].beadList[0].y;
  const double ztmp = particleList[particle_idx].beadList[0].z;
  const double xlo = box->xlo;
  const double ylo = box->ylo;
  const double zlo = box->zlo;
  const double Lx = box->Lx;
  const double Ly = box->Ly;
  const double Lz = box->Lz;

  int** const cells = cell->cells;
  int** const neighbor_cells = cell->neighbor_cells;
  int* const num_neighbor_cells = cell->num_neighbor_cells;
  int* const num_particles_in_cell = cell->num_particles_in_cell;
  
  cell_idx = cell->coord2cell(xtmp, ytmp, ztmp, xlo, ylo, zlo);

  // loop through the particles within the cell c

  eng = 0;
  virial = 0;

  for (int i = 0; i < num_particles_in_cell[cell_idx]; i++) {    
    int j = cells[cell_idx][i];
    if (j == particle_idx) {
      idx_in_cell = i;     // store the idx of the particle in the cell
      continue;
    }

    const Bead* const bead = &particleList[j].beadList[0];
    double delx = bead->x - xtmp;
    double dely = bead->y - ytmp;
    double delz = bead->z - ztmp;

    // minimum image convention

    if (delx < -0.5*Lx) delx += Lx;
    else if (delx > 0.5*Lx) delx -= Lx;
    if (dely < -0.5*Ly) dely += Ly;
    else if (dely > 0.5*Ly) dely -= Ly;
    if (delz < -0.5*Lz) delz += Lz;
    else if (delz > 0.5*Lz) delz -= Lz;

    double rsq = delx * delx + dely * dely + delz * delz;
    if (rsq < cutsq && rsq > 0) {
      double e, v;
      evaluator->compute(rsq, e, v);
      eng += e;
      virial += v;
    }

  }  // end within the cell
   
  // now condering in the neighboring cells

  for (int n = 1; n < num_neighbor_cells[cell_idx]; n++) {
    int cn = neighbor_cells[cell_idx][n];
    if (cn == cell_idx) continue;

    // loop through each bead in the cell[nbr]
    for (int i = 0; i < num_particles_in_cell[cn]; i++) {
      int j = cells[cn][i];
      if (j == particle_idx) continue;

      const Bead* const bead = &particleList[j].beadList[0];
      double delx = bead->x - xtmp;
      double dely = bead->y - ytmp;
      double delz = bead->z - ztmp;
 
      // minimum image convention

      if (delx < -0.5*Lx) delx += Lx;
      else if (delx > 0.5*Lx) delx -= Lx;
      if (dely < -0.5*Ly) dely += Ly;
      else if (dely > 0.5*Ly) dely -= Ly;
      if (delz < -0.5*Lz) delz += Lz;
      else if (delz > 0.5*Lz) delz -= Lz;

      double rsq = delx * delx + dely * dely + delz * delz;
      if (rsq < cutsq) {
        double e, v;
        evaluator->compute(rsq, e, v);
        eng += e;
        virial += v;
      }
    }
  }   // end loops over neighbor cells

  particle_energies[particle_idx] = eng;
  particle_virials[particle_idx] = virial;
}

/*---------------------------------------------------------------------------
  compute total energy and virial
-----------------------------------------------------------------------------*/

void MonteCarlo::compute_total_energy(double &eng, double &virial)
{
  compute_total_energy_cell(eng, virial);
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::compute_total_energy_nsq(double &eng, double &virial)
{
  const double cutoff = evaluator->get_cutoff();
  const double cutsq = cutoff*cutoff;
  const double Lx = box->Lx;
  const double Ly = box->Ly;
  const double Lz = box->Lz;

  // N^2 brute force

  eng = 0;
  virial = 0.0;

  for (int i = 0; i < nparticles - 1; i++) {

    const double xtmp = particleList[i].beadList[0].x;
    const double ytmp = particleList[i].beadList[0].y;
    const double ztmp = particleList[i].beadList[0].z;

    for (int j = i + 1; j < nparticles; j++) {
      const Bead* const bead = &particleList[j].beadList[0];

      double delx = bead->x - xtmp;
      double dely = bead->y - ytmp;
      double delz = bead->z - ztmp;

      // minimum image convention

      if (delx < -0.5*Lx) delx += Lx;
      else if (delx > 0.5*Lx) delx -= Lx;
      if (dely < -0.5*Ly) dely += Ly;
      else if (dely > 0.5*Ly) dely -= Ly;
      if (delz < -0.5*Lz) delz += Lz;
      else if (delz > 0.5*Lz) delz -= Lz;

      double rsq = delx * delx + dely * dely + delz * delz;
      if (rsq < cutsq) {
        double e, v;
        evaluator->compute(rsq, e, v);
        eng += e;
        virial += v;
      }
    }
  }
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::compute_total_energy_cell(double &eng, double &virial)
{
  // bin the particles to the cells

  cell->bin_particles();
 
  eng = 0;
  virial = 0.0;
  
  // compute particle energy and virial

  for (int i = 0; i < nparticles; i++) {
    double e, v;
    compute_particle_energy_cell(i, e, v);
    eng += e;
    virial += v;
  }

  eng *= 0.5;
  virial *= 0.5;
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::trial_displacement(Particle* particle, Box* box, const double dmax,
  double* backup)
{
  // generate a random vector on a unit sphere

  double ran1, ran2;
  double rsq = 2.0;
  while (rsq >= 1.0) {
    ran1 = 1.0 - 2*random->random_mars();
    ran2 = 1.0 - 2*random->random_mars();
    rsq = ran1*ran1 + ran2*ran2;
  }
  double ranh = 2.0*sqrt(1.0 - rsq);
  double bx = ran1 * ranh;
  double by = ran2 * ranh;
  double bz = 1.0 - 2.0 * rsq;

  // save the particle coordinate into backup

  backup[0] = particle->beadList[0].x;
  backup[1] = particle->beadList[0].y;
  backup[2] = particle->beadList[0].z;

  // make a trial displacement

  double xtmp = particle->beadList[0].x + dmax*bx;
  double ytmp = particle->beadList[0].y + dmax*by;
  double ztmp = particle->beadList[0].z + dmax*bz;

  // apply periodic boundary conditions

  if (xtmp < box->xlo) xtmp += box->Lx;
  else if (xtmp > box->xhi) xtmp -= box->Lx;
  if (ytmp < box->ylo) ytmp += box->Ly;
  else if (ytmp > box->yhi) ytmp -= box->Ly;
  if (ztmp < box->zlo) ztmp += box->Lz;
  else if (ztmp > box->zhi) ztmp -= box->Lz;

  particle->beadList[0].x = xtmp;
  particle->beadList[0].y = ytmp;
  particle->beadList[0].z = ztmp;
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::restore_displacement(Particle* particle, const double* backup)
{
  particle->beadList[0].x = backup[0];
  particle->beadList[0].y = backup[1];
  particle->beadList[0].z = backup[2];
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::trial_rotation(Particle* particle, const double rmax,
   double* backup)
{
  // generate a random vector on a unit sphere (see Frenkel and Smit,
  // Vesely 1982, Marsaglia)

  double q[4];
  double s1, s2, s3;

  do {
    q[0] = random->random_mars()-1.0; // [-1.0;1.0]
    q[1] = random->random_mars()-1.0; // [-1.0;1.0]
  } while ((s1 = q[0] * q[0] + q[1] * q[1]) >= 1.0);

  do {
    q[2] = random->random_mars()-1.0; // [-1.0;1.0]
    q[3] = random->random_mars()-1.0; // [-1.0;1.0]
  } while ((s2 = q[2] * q[2] + q[3] * q[3]) >= 1.0 || s2 == 0.0);

  s3 = sqrt((1.0 - s1) / s2);
  q[2] *= s3;
  q[3] *= s3;

  // save the particle quaternion

  double *quat = particle->quat;

  backup[0] = quat[0];
  backup[1] = quat[1];
  backup[2] = quat[2];
  backup[3] = quat[3];

  // creat the new quaternion

  quat[0] += rmax * q[0];
  quat[1] += rmax * q[1];
  quat[2] += rmax * q[2];
  quat[3] += rmax * q[3];
  double mag = sqrt(quat[0]*quat[0] + quat[1]*quat[1] + quat[2]*quat[2] +
    quat[3]*quat[3]);

  // renormalize the new quaterionn
  quat[0] /= mag;
  quat[1] /= mag;
  quat[2] /= mag;
  quat[3] /= mag;
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::restore_rotation(Particle* particle, const double* backup)
{
  double *quat = particle->quat;
  quat[0] = backup[0];
  quat[1] = backup[1];
  quat[2] = backup[2];
  quat[3] = backup[3];
}

/*---------------------------------------------------------------------------*/

int MonteCarlo::read_restart(const char* filename)
{
  int i, success;
  char* line = new char[1024];
  double xlo,xhi,ylo,yhi,zlo,zhi;
  int nfields = 0;
  int* fields = NULL;
  
  ifstream ifs(filename);
  if (!ifs.good()) {
    printf("Cannot read file %s\n", filename);
    return 0;
  }

  if (particleList) delete [] particleList;

  int narg=0, MAXARGS=16;
  char *copy, **arg;
  copy = new char [1024];
  arg = new char* [MAXARGS];

  int nNumTotalBeads = 0;

  for (i = 0; i < 9; i++) {
   ifs.getline(line, 1024);
    if (i == 1) sscanf(line, "%d", &ncurrent_cycle);
    if (i == 3) sscanf(line, "%d", &nNumTotalBeads);
    if (i == 5) sscanf(line, "%lf %lf", &xlo, &xhi);
    if (i == 6) sscanf(line, "%lf %lf", &ylo, &yhi);
    if (i == 7) sscanf(line, "%lf %lf", &zlo, &zhi);
    if (i == 8) {
      io_handle->parse(line, copy, narg, arg);
      if (strcmp(arg[1], "ATOMS") != 0) {
        printf("Incorrect line!!\n");
        return 0;
      }
      if (narg >= 3) {
        nfields = narg - 2;
        fields = new int [nfields];
        for (int m = 0; m < nfields; m++) fields[m] = 0;
      } else {
        printf("Invalid number of fields: %s\n", line);
        return 0;
      }
      
      int iarg = 2;
      int m = 0;
      while (iarg < narg) {
        if (strcmp(arg[iarg],"id") == 0) fields[m] = ID;
        else if (strcmp(arg[iarg],"mol") == 0) fields[m] = MOL;
        else if (strcmp(arg[iarg],"type") == 0) fields[m] = TYPE;
        else if (strcmp(arg[iarg],"q") == 0) fields[m] = Q;
        else if (strcmp(arg[iarg],"x") == 0) fields[m] = X;
        else if (strcmp(arg[iarg],"y") == 0) fields[m] = Y;
        else if (strcmp(arg[iarg],"z") == 0) fields[m] = Z;
        iarg++;
        m++;
      }
    }
  }

  printf("Read from %s: current cycle: %d\n", filename, ncurrent_cycle);
  if (logfile) fprintf(logfile, "Read from %s: current cycle: %d\n", filename, ncurrent_cycle);

  double Lx = xhi - xlo;
  double Ly = yhi - ylo;
  double Lz = zhi - zlo;

  // create the simulation box
  box = new Box(Lx, Ly, Lz);

  nparticles = nNumTotalBeads;
  nmax = nparticles;
  particleList = new Particle [nparticles];

  int n = 0;
  for (i = 0; i < nNumTotalBeads; i++) {
    ifs.getline(line, 1024);

    io_handle->parse(line, copy, narg, arg);
    if (narg != nfields) {
      printf("ERROR: Inconsistent number of fields\n");
      return 0;
    }

    Bead* bead = &particleList[i].beadList[0];
    for (int m = 0; m < narg; m++) {
      if (fields[m] == ID) bead->id = atoi(arg[m]);
      else if (fields[m] == MOL) bead->mol = atoi(arg[m]);
      else if (fields[m] == TYPE) strcpy(bead->type, arg[m]);
      else if (fields[m] == X) bead->x = atof(arg[m]);
      else if (fields[m] == Y) bead->y = atof(arg[m]);
      else if (fields[m] == Z) bead->z = atof(arg[m]);
    }

    bead->id = n;
  }

  ifs.close();

  delete [] arg;
  delete [] copy;
  delete [] line;
  if (fields) delete [] fields;
  return 1;
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::set_log(const char* filename)
{
  logfile = fopen(filename, "w");
  if (!logfile) printf("ERROR: Cannot open log file %s to write\n", filename);
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::create_histogram(const std::vector<double> array,
  const char* filename, double& mean, double& stdev)
{
  int n = array.size();
  if (n == 0) return;

  double minvalue, maxvalue, s, s2;
  minvalue = maxvalue = array[0];
  s = s2 = 0;
  for (int i = 0; i < n; i++) {
    if (minvalue > array[i]) minvalue = array[i];
    if (maxvalue < array[i]) maxvalue = array[i];
    s += array[i];
    s2 += array[i]*array[i];
  }
  mean = s/(double)n;
  stdev = sqrt(s2/(double)n - mean*mean);

  int nbins = 100;
  double bin_size = (maxvalue - minvalue) / nbins;
  double* bins = new double [nbins];
  for (int i = 0; i < nbins; i++) bins[i] = 0;
  for (int i = 0; i < n; i++) {
    int ibin = (int)((array[i] - minvalue) / bin_size);
    if (ibin >= 0 && ibin < nbins)
      bins[ibin] += 1.0;
  }

  if (strcmp(filename, "NULL") != 0) {
 
    ofstream ofs;
    ofs.open(filename);
    ofs << "CYCLE: " << ncurrent_cycle << "\n";
    for (int i = 0; i < nbins; i++)
      ofs << minvalue + i*bin_size << " " << bins[i] << "\n";
    ofs.close();
  }

  delete [] bins;
}

/*---------------------------------------------------------------------------
  check flatness of a histogram using Kullback-Leibler's relative entropy
-----------------------------------------------------------------------------*/

int MonteCarlo::check_histogram_flatness(const double* histogram, const int nbins,
  const double threshold, double& fluctuation)
{
  int flat = 0;
/*
  double nonzeroBins   = 0;
  double sum = 0;
  for (int i = 0; i < nbins; i++) {
    if (histogram[i] > 0) {
      sum += histogram[i];
      nonzeroBins   += 1.0;
    }
  }

  if (nonzeroBins <= 2) {
    return 0;
  }

  double d_kullback = 0;
  for (int i = 0; i < nbins; i++) {
    if (histogram[i] > 0) {
      double P_i = histogram[i] / NumEvents;
      double Q_i = 1.0 / nonzeroBins;
      d_kullback += P_i * log(P_i / Q_i);
    }
  }
  if (d_kullback < threshold) flat = 1;
  else flat = 0;
*/
  
  // compute the mean value of the non-empty bins
  // also find the minimum value of the non-empty bins

  double nonzeroBins   = 0;
  double sum = 0;
  double minval = -1;
  for (int i = 0; i < nbins; i++) {
    if (histogram[i] > 0) {
      sum += histogram[i];
      nonzeroBins   += 1.0;

      if (minval < 0) minval = histogram[i];   
      if (minval > histogram[i]) minval = histogram[i];
    }
  }

  double meanH = sum / nonzeroBins;

  // if the minimum value divided by the mean is greater than threshold
  //   the histogram deems flat

  fluctuation = minval / meanH;
  if (fluctuation > threshold) flat = 1;
  else flat = 0;

  return flat;
}

/*---------------------------------------------------------------------------*/

void MonteCarlo::print_citations()
{
  printf("Monte Carlo sampling package %s\n", VERSION);
  if (logfile) {
    fprintf(logfile,"Monte Carlo sampling package %s\n", VERSION);
    fflush(logfile);
  }
}
