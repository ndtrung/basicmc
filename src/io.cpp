/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string.h>
#include <cmath>
#include "io.h"

using namespace std;

namespace MC {

enum {ID=0,MOL=1,TYPE=2,Q=3,X=4,Y=5,Z=6};

/*---------------------------------------------------------------------------*/

int XYZHandle::read(const char* fileName, char** types, int ntypes,
                    int& nNumTotalBeads, double& Lx, double& Ly, double& Lz,
                    std::vector<Particle>& particleList, int& timestep)
{
  int i;
  Bead bead;
  ifstream ifs(fileName);
  if (!ifs.good()) {
    std::cout << "Cannot read file " << fileName << std::endl;
    return 0;
  }

  particleList.clear();

  ifs >> nNumTotalBeads;
  ifs >> Lx >> Ly >> Lz;

  for (i = 0; i < nNumTotalBeads; i++) {
    ifs >> bead.type >> bead.x >> bead.y >> bead.z;

    bead.id = i;

    int included = 0;
    for (int m = 0; m < ntypes; m++) {
      if (strcmp(bead.type, types[m]) == 0) {
        included = 1;
        break;
      }
    }

    if (included == 1) {
      Particle p;
      p.beadList.push_back(bead);
      particleList.push_back(p);
    }
  }
  
  nNumTotalBeads = particleList.size();
  ifs.close();
  return 1;
}

/*---------------------------------------------------------------------------*/

void XYZHandle::write(const std::vector<Particle>& particleList, int nNumTotalBeads,
                      double xlo, double xhi, double ylo, double yhi,
                      double zlo, double zhi, const char* fileName, int timestep)
{
  int i, j, nNumParticles;
  char* file = new char [256];
  ofstream ofs;

  double Lx = xhi - xlo;
  double Ly = yhi - ylo;
  double Lz = zhi - zlo;

  sprintf(file, "%s-%08d.xyz", fileName, timestep);
  ofs.open(file);
  ofs << nNumTotalBeads << std::endl;
  ofs << Lx << "\t" << Ly << "\t" << Lz << std::endl;
  nNumParticles = particleList.size();
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++) {
      ofs << particleList[i].beadList[j].type << "\t"
          << particleList[i].beadList[j].x << "\t"
          << particleList[i].beadList[j].y << "\t"
          << particleList[i].beadList[j].z << std::endl;
    }
  }

  ofs.close();

  delete [] file;
}

/*---------------------------------------------------------------------------*/

int XMLHandle::read(const char* fileName, char** types, int ntypes,
                    int& nNumTotalBeads, double& Lx, double& Ly, double& Lz,
                    std::vector<Particle>& particleList, int& timestep)
{
  int i;
  char* line = new char [1024];
  Bead bead;
  vector<Bead> allBeads;
  
  ifstream ifs(fileName);
  if (!ifs.good()) {
    cout << "Cannot read file " << fileName << std::endl;
    return 0;
  }

  particleList.clear();

  int adding_beads = 0;
  int reading_pos = 0;
  int reading_type = 0;

  int added_beads = 0;
  int read_pos = 0;
  int read_type = 0;

  while (!ifs.eof()) {
    ifs.getline(line, 1024);

    string s(line);
    s.erase(remove(s.begin(), s.end(), '\"'), s.end());
    s.erase(remove(s.begin(), s.end(), '<'), s.end());
    s.erase(remove(s.begin(), s.end(), '>'), s.end());

    istringstream iss(s);
    size_t pos;
    string word;
    while (iss >> word) {
      pos = word.find("natoms");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+7);
        nNumTotalBeads = atoi(n.c_str());
      }
      pos = word.find("time_step");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+10);
        timestep = atoi(n.c_str());
      }

      pos = word.find("lx=");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+3);
        Lx = atof(n.c_str());
      }
      pos = word.find("ly=");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+3);
        Ly = atof(n.c_str());
      }
      pos = word.find("lz=");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+3);
        Lz = atof(n.c_str());
      }

      pos = word.find("position");
      if (pos!=std::string::npos && read_pos == 0)
        reading_pos = 1;
      pos = word.find("type");
      if (pos!=std::string::npos && read_type == 0) {
        reading_type = 1;
      }
    }    

    if (reading_pos == 1) {
      if (allBeads.size() == 0) {
        adding_beads = 1;
      }
      for (i = 0; i < nNumTotalBeads; i++) {
      ifs.getline(line, 1024);
        sscanf(line, "%lf %lf %lf", &bead.x, &bead.y, &bead.z);
       if (added_beads == 0) allBeads.push_back(bead);
        else {
          allBeads[i].x = bead.x;
          allBeads[i].y = bead.y;
          allBeads[i].z = bead.z;
        }
     }
      reading_pos = 0;
      read_pos = 1;
      added_beads = 1;
    }

    if (reading_type == 1) {
      if (allBeads.size() == 0) {
        adding_beads = 1;
      }
      for (i = 0; i < nNumTotalBeads; i++) {
      ifs.getline(line, 1024);
        sscanf(line, "%s", &bead.type);
        if (added_beads == 0) allBeads.push_back(bead);
        else {
          sprintf(allBeads[i].type, "%s", bead.type);
        }
     }
      reading_type = 0;
      read_type = 1;
      added_beads = 1;
    }

  }

  int n = 0;
  for (i = 0; i < nNumTotalBeads; i++) {
    int included = 0;
    for (int m = 0; m < ntypes; m++) {
      if (strcmp(allBeads[i].type, types[m]) == 0) {
        included = 1;
        break;
      }
    }

    if (included == 1) {
      allBeads[i].id = n;

      Particle p;
      p.beadList.push_back(allBeads[i]);
      particleList.push_back(p);
    }

    n++;
 }
  
  nNumTotalBeads = particleList.size();

  ifs.close();
  delete [] line;
  return 1;
}

/*---------------------------------------------------------------------------*/

void XMLHandle::write(const std::vector<Particle>& particleList, int nNumTotalBeads,
                      double xlo, double xhi, double ylo, double yhi,
                      double zlo, double zhi, const char* fileName, int timestep)
{
  int i, j, nNumParticles;
  char* file = new char [256];
  ofstream ofs;

  double Lx = xhi - xlo;
  double Ly = yhi - ylo;
  double Lz = zhi - zlo;

  sprintf(file, "%s-%08d.xml", fileName, timestep);
  ofs.open(file);

  ofs << "<\?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  ofs << "<hoomd_xml version=\"1.5\">\n";
  ofs << "<configuration time_step=\"" << timestep;
  ofs << "\" dimensions=\"3\" natoms=\"" << nNumTotalBeads << "\" >\n";
  ofs << "<box lx=\"" << Lx << "\" ly=\"" << Ly << "\" lz=\"" << Lz;
  ofs << "\" xy=\"0\" xz=\"0\" yz=\"0\"/>\n";
  ofs << "<position num=\"" << nNumTotalBeads << "\">\n";
  ofs.precision(15);

  nNumParticles = particleList.size();
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++) {
      ofs << particleList[i].beadList[j].x << " "
          << particleList[i].beadList[j].y << " "
          << particleList[i].beadList[j].z << "\n";
    }
  }

  ofs << "</position><type>\n";
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++)
      ofs << particleList[i].beadList[j].type << "\n";
  }
  ofs << "</type>\n";

  ofs << "</configuration>\n";
  ofs << "</hoomd_xml>\n";

  ofs.close();

  delete [] file;
}

/*---------------------------------------------------------------------------*/

int DUMPHandle::read(const char* fileName, char** types, int ntypes,
                     int& nNumTotalBeads, double& Lx, double& Ly, double& Lz,
                     std::vector<Particle>& particleList, int& timestep)
{
  int i;
  char* line = new char[1024];
  double xlo,xhi,ylo,yhi,zlo,zhi;
  Bead bead;
  
  ifstream ifs(fileName);
  if (!ifs.good()) {
    cout << "Cannot read file " << fileName << std::endl;
    return 0;
  }

//  particleList.clear();

  int narg=0, MAXARGS=16;
  char *copy, **arg;
  copy = new char [1024];
  arg = new char* [MAXARGS];

  for (i = 0; i < 9; i++) {
   ifs.getline(line, 1024);
    if (i == 1) sscanf(line, "%d", &timestep);
    if (i == 3) sscanf(line, "%d", &nNumTotalBeads);
    if (i == 5) sscanf(line, "%lf %lf", &xlo, &xhi);
    if (i == 6) sscanf(line, "%lf %lf", &ylo, &yhi);
    if (i == 7) sscanf(line, "%lf %lf", &zlo, &zhi);
    if (i == 8) {
      parse(line, copy, narg, arg);
      if (strcmp(arg[1], "ATOMS") != 0) printf("Incorrect line!!\n");
      if (narg >= 3) {
        nfields = narg - 2;
        fields = new int [nfields];
        for (int m = 0; m < nfields; m++) fields[m] = 0;
      } else {
        printf("Invalid number of fields: %s\n", line);
      }
      
      int iarg = 2;
      int m = 0;
      while (iarg < narg) {
        if (strcmp(arg[iarg],"id") == 0) fields[m] = ID;
        else if (strcmp(arg[iarg],"mol") == 0) fields[m] = MOL;
        else if (strcmp(arg[iarg],"type") == 0) fields[m] = TYPE;
        else if (strcmp(arg[iarg],"q") == 0) fields[m] = Q;
        else if (strcmp(arg[iarg],"x") == 0) fields[m] = X;
        else if (strcmp(arg[iarg],"y") == 0) fields[m] = Y;
        else if (strcmp(arg[iarg],"z") == 0) fields[m] = Z;
        iarg++;
        m++;
      }
    }
  }

  Lx = xhi - xlo;
  Ly = yhi - ylo;
  Lz = zhi - zlo;
/*  
  // check if the box center is at (0,0,0)
  // otherwise shift everything to [-Lx/2;Lx/2]x[-Ly/2;Ly/2]x[-Lz/2;Lz/2]
  
  double EPSILON = 0.0001;
  int box_center_at_origin = 1;
  double delx, dely, delz, cx, cy, cz;
  cx = 0.5 * (xlo + xhi);
  cy = 0.5 * (ylo + yhi);
  cz = 0.5 * (zlo + zhi);
  if (fabs(cx) > EPSILON || fabs(cy) > EPSILON || fabs(cz) > EPSILON) {
    box_center_at_origin = 0;
    delx = -cx;
    dely = -cy;
    delz = -cz;
  }
*/  
  int n = 0;
  for (i = 0; i < nNumTotalBeads; i++) {
    ifs.getline(line, 1024);

    parse(line, copy, narg, arg);
    if (narg != nfields) printf("ERROR: Inconsistent number of fields\n");

    for (int m = 0; m < narg; m++) {
      if (fields[m] == ID) bead.id = atoi(arg[m]);
      else if (fields[m] == MOL) bead.mol = atoi(arg[m]);
      else if (fields[m] == TYPE) strcpy(bead.type, arg[m]);
      else if (fields[m] == X) bead.x = atof(arg[m]);
      else if (fields[m] == Y) bead.y = atof(arg[m]);
      else if (fields[m] == Z) bead.z = atof(arg[m]);
    }
//    sscanf(line, "%d %d %s %lf %lf %lf", &bead.id, &mol, &bead.type, &bead.x, &bead.y, &bead.z);
/*    
    // atoms in LAMMPS dump files can be a little bit outside of the box
    
    if (bead.x < xlo) bead.x += Lx;
    else if (bead.x > xhi) bead.x -= Lx;
    if (bead.y < ylo) bead.y += Ly;
    else if (bead.y > yhi) bead.y -= Ly;
    if (bead.z < zlo) bead.z += Lz;
    else if (bead.z > zhi) bead.z -= Lz;
    
    // shift the bead positions so that the coordinates are in [-Lx/2;Lx/2]x[-Ly/2;Ly/2]x[-Lz/2;Lz/2]
    
    if (box_center_at_origin == 0) {
      bead.x += delx;
      bead.y += dely;
      bead.z += delz;
    }
*/    
    int included = 0;
    for (int m = 0; m < ntypes; m++) {
      if (strcmp(bead.type, types[m]) == 0) {
        included = 1;
        break;
      }
    }

    if (included == 1) {
      bead.id = n;

      Particle p;
      p.beadList.push_back(bead);
      particleList.push_back(p);
      n++;
    }
  }

  nNumTotalBeads = particleList.size();
  ifs.close();

  delete [] arg;
  delete [] copy;
  delete [] line;
  return 1;
}

/*---------------------------------------------------------------------------*/

void DUMPHandle::write(Particle* particleList, int nNumParticles, int nNumTotalBeads,
                       double xlo, double xhi, double ylo, double yhi, double zlo, double zhi,
                       const char* fileName, int timestep)
{
  int i, j, id;
  char* file = new char [256];
  ofstream ofs;

  sprintf(file, "%s-%08d.dump", fileName, timestep);
  ofs.open(file);
  ofs << "ITEM: TIMESTEP\n";
  ofs << timestep << "\n";
  ofs << "ITEM: NUMBER OF ATOMS\n";
  ofs << nNumTotalBeads << "\n";
  ofs << "ITEM: BOX BOUNDS pp pp pp\n";
  ofs << xlo << " " << xhi << "\n";
  ofs << ylo << " " << yhi << "\n";
  ofs << zlo << " " << zhi << "\n";

  if (particleList[0].rot_flag)
    ofs << "ITEM: ATOMS id mol type q x y z c_q[0] c_q[1] c_q[2] c_q[3]\n";
  else
    ofs << "ITEM: ATOMS id mol type q x y z\n";

  id = 1;
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++) {
      ofs << id << " ";
      ofs << particleList[i].beadList[j].mol << " ";
      ofs << particleList[i].beadList[j].ntype << " ";
      ofs << particleList[i].beadList[j].q << " ";
      ofs << particleList[i].beadList[j].x << " ";
      ofs << particleList[i].beadList[j].y << " ";
      ofs << particleList[i].beadList[j].z << " ";

      if (particleList[i].rot_flag) {
        Particle* p = &particleList[i];
        ofs << p->quat[0] << " ";
        ofs << p->quat[1] << " ";
        ofs << p->quat[2] << " ";
        ofs << p->quat[3] << " ";
      }
      ofs << "\n";
      id++;
    }
  }

  ofs.close();

  delete [] file;
}

/*---------------------------------------------------------------------------
  extract beads of a certain index locally to each chain
-----------------------------------------------------------------------------*/

void extract(const char* fileName, int local, int nNumBeadsInChain, char* outfile)
{
  ifstream ifs(fileName);
  vector<Bead> beadList;

  int i, nNumTotalBeads;
  double Lx, Ly, Lz;
  Bead bead;

  ifs >> nNumTotalBeads;
  ifs >> Lx >> Ly >> Lz;

  for (i = 0; i < nNumTotalBeads; i++) {
    ifs >> bead.type >> bead.x >> bead.y >> bead.z;
    if (i % nNumBeadsInChain == local)
      beadList.push_back(bead);
  }

  ifs.close();

  ofstream ofs(outfile);
 
  ofs << beadList.size() << "\n";
  ofs << Lx << "\t" << Ly << "\t" << Lz << "\n";
  for (i = 0; i < beadList.size(); i++) {
    ofs << beadList[i].type << "\t" << beadList[i].x << "\t"
        << beadList[i].y << "\t" <<  beadList[i].z << "\n";
  }

  ofs.close();
}

}

