/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Monte-Carlo Wang-Landau simulation 

  Trung Nguyen (Northwestern)
  Start: Nov 29 2017
*/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <cassert>

#include "wanglandau.h"
#include "buildingblocks.h"
#include "lattice.h"
#include "io.h"
#include "memory.h"
#include "random_mc.h"

using namespace MC;
using namespace std;

#define MAX_ITER 100
#define EPSILON 1e-2
//#define WL_DEBUG

/*---------------------------------------------------------------------------*/

MCWangLandau::MCWangLandau(Particle*& _particleList, class RandomMC* _random, int _nparticles)
 : MonteCarlo(_particleList, _random, _nparticles)
{
  dmax = 0.1;
  energy_user = 0;
  nbins = 100;
  dE = 0;
  f = 2.0;
  threshold = 0.8;

  log_g = hE = NULL;
}

/*---------------------------------------------------------------------------*/

MCWangLandau::MCWangLandau(class Box* _box, Particle*& _particleList, class RandomMC* _random,
  int _nparticles) : MonteCarlo(_box, _particleList, _random, _nparticles)
{
  dmax = 0.1;
  energy_user = 0;
  nbins = 100;
  f = 2.0;
  threshold = 0.8;

  log_g = hE = NULL;
}

/*---------------------------------------------------------------------------*/

MCWangLandau::~MCWangLandau()
{
  if (log_g) destroy(log_g);
  if (hE) destroy(hE);
}

/*---------------------------------------------------------------------------*/

void MCWangLandau::init()
{
  MonteCarlo::init();

  printf("Running Wang-Landau MC simulation..\n");
  if (logfile) fprintf(logfile, "Running Wang-Landau MC simulation..\n");
}

/*---------------------------------------------------------------------------*/

void MCWangLandau::setup()
{
  MonteCarlo::setup();

  double volume = box->Lx * box->Ly * box->Lz;
  double rho = (double)nparticles/volume;
  double cutoff = evaluator->get_cutoff();
  evaluator->get_energy_correction(cutoff, rho, energy_correction);

  // cell list setup

  cell = new Cell<Particle>(box, particleList, nparticles, 2.0*cutoff);
  cell->setup();

  // compute total energy and virial

  compute_total_energy(total_energy, virial);
  total_energy += energy_correction;

  // estimates for energy range

  if (!energy_user) {
    Emin = -2.0*nparticles;
    Emax = 2.0*nparticles;
  }

  if (dE == 0) {
    dE = (Emax - Emin) / nbins;
  }

  assert(dE > 0);
  nbins = (int)((Emax - Emin) / dE) + 1;

  create(hE, nbins);
  create(log_g, nbins);

  log_f = log(f);

  // initialize the log of the density of states estimate to be zero: log[g(E)] = 0

  for (int m = 0; m < nbins; m++) log_g[m] = 0;

  for (int m = 0; m < nbins; m++) hE[m] = 0;

  // write out the initial configuration

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "config", 0);

}

/*---------------------------------------------------------------------------*/

void MCWangLandau::set_energy_range(const double _Emin, const double _Emax)
{
  Emin = _Emin;
  Emax = _Emax;
  energy_user = 1;
}

/*---------------------------------------------------------------------------*/

void MCWangLandau::run(int nrelaxations, int ncycles)
{
  // initialize the particles, if needed

  init();

  // setting up the simulation run

  setup();

  double volume = box->Lx * box->Ly * box->Lz;
  std::vector<double> energies;

  printf("%d particles; Number density = %f; Total energy = %f\n",
    nparticles, (double)nparticles/volume, total_energy);
  printf("E = %f %f; nbins = %d, dE = %f\n", Emin, Emax, nbins, dE);

  if (logfile) {
    fprintf(logfile, "%d particles; Number density = %f; Total energy = %f\n",
      nparticles, (double)nparticles/volume, total_energy);
    fprintf(logfile, "E = %f %f; nbins = %d, dE = %f\n", Emin, Emax, nbins, dE);
    fflush(logfile);
  }

  printf("Wang-Landau iteration for %d cycles; nrelaxations = %d\n",
    ncycles, nrelaxations);
  printf("Cycle Energy f Flatness\n");

  if (logfile) {
    fprintf(logfile, "\nWang-Landau iteration for %d cycles; nrelaxations = %d\n",
      ncycles, nrelaxations);
    fprintf(logfile, "Cycle Energy f Flatness\n");
    fflush(logfile);
  }

  // main iteration

  clock_t start, elapsed;
  start = clock();

  int ntrials = nrelaxations;

  for (int n = 0; n <= ncycles; n++) {

    naccepted = 0;
    nrejected = 0;
    double acceptance_ratio = 0;

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++) {
      sampling_wl();
    }

    // recompute the total energy every cycle

    compute_total_energy(total_energy, virial);
    total_energy += energy_correction;
    energies.push_back(total_energy);

    acceptance_ratio = (double)naccepted/(double)(naccepted+nrejected);

    if (n % dump_period == 0) {

      ofstream ofs;
      ofs.open("energies.txt");
      int num = (int)energies.size();
      for (int m = 0; m < num; m++)
        ofs << m << " " << energies[m] << "\n";
      ofs.close();

      char filename[64];
      sprintf(filename, "S_E_%d.txt", n);
      ofs.open(filename);
      for (int m = 0; m < nbins; m++)
        ofs << Emin + m * dE << " " << hE[m] << " " << log_g[m] << "\n";
      ofs.close();

      io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
         box->ylo, box->yhi, box->zlo, box->zhi, "config", n);
    }

    if (n % sampling_period == 0 || n == ncycles) {

      // check if h(E) is flat

      double fluctuation = 0;
      int histogram_flat = check_histogram_flatness(hE, nbins, threshold, fluctuation);

      printf("%8d %8.3f %0.3f %d\n", n, total_energy, fluctuation, histogram_flat);
      if (logfile) {
        fprintf(logfile, "%8d %8.3f %0.3f %d\n", n, total_energy, fluctuation, histogram_flat);
        fflush(logfile);
      }

      // reduce f <- sqrt(f): log_f /= 2
      // reset energy histogram

      if (histogram_flat) {
        log_f /= 2.0;
        for (int m = 0; m < nbins; m++) hE[m] = 0;
      }
    }
    ncurrent_cycle++; 
  }

  elapsed = clock()-start;
  printf("\nLoop time of %f seconds for %d cycles with %d particles\n",
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  if (logfile) {
    fprintf(logfile, "\nLoop time of %f seconds for %d cycles with %d particles\n", 
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  }

  // final histogram
  double mean, stdev;
  create_histogram(energies, "hist_energy_final.txt", mean, stdev);
}

/*---------------------------------------------------------------------------
  sample with S(E)
  update total_energy with trial moves accepted or rejected
-----------------------------------------------------------------------------*/

void MCWangLandau::sampling_wl()
{
  // randomly pick a particle idx

  while (1) {
    double r = random->random_mars();
    particle_idx = r * nparticles;
    if (particle_idx >= 0 && particle_idx < nparticles) break;
  }

  // compute the current energy of the chosen particle

  double current_particle_energy = compute_particle_energy(particle_idx);
  int idx_in_last_cell = idx_in_cell;

  // attempt a trial move to the chosen particle

  int accepted = 0;
  double last_pos[4];
  int rot = 0;
  if (particleList[particle_idx].rot_flag) {
    if (random->random_mars() < 0.5) {
      trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
    } else {
      trial_rotation(&particleList[particle_idx], rmax, last_pos);
      rot = 1;
    }
  } else {
    trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
  }

  // compute the particle energy due to the trial move

  double trial_particle_energy = compute_particle_energy(particle_idx);
  double delta_E = trial_particle_energy - current_particle_energy;

  // change in total energy is still on the order of delta_E - a trial move by a particle

  double trial_energy = total_energy + delta_E;

  if (trial_energy < Emin || trial_energy > Emax) { // out of energy range, reject right away
    if (!rot) {
      restore_displacement(&particleList[particle_idx], last_pos);
    } else {
      restore_rotation(&particleList[particle_idx], last_pos);
    }
    particle_energies[particle_idx] = current_particle_energy;
    nrejected++;

  } else {

    // accept or reject the trial move based on P(E) ~ 1/g(E)
    // P(old) P(old -> new) alpha(old -> new) = P(new) P(new -> old) alpha(new -> old)
    //   assuming symmetry transition probability: alpha(old -> new) = alpha(new -> old)
    //   P_acc (old -> new) / P_acc (new -> old) = P(new) / P(old) = g(old) / g(new)
    // Metropolis choice: P_acc (old -> new) = min[1; g(old)/g(new)]
    //   compute log(P_acc) = min[0; log[g(old)] - log[g(new)] ] 

    int current_e_bin = (int)((total_energy - Emin) / dE);
    int trial_e_bin   = (int)((trial_energy - Emin) / dE);
    double current_log_g = log_g[current_e_bin];
    double trial_log_g   = log_g[trial_e_bin];
    double delta_log_g   = current_log_g - trial_log_g;

    double p = exp(delta_log_g);
    if (p >= 1.0 || random->random_mars() < p) { // accept
      cell->update(particle_idx, idx_in_last_cell, last_pos);
      total_energy = trial_energy;
      naccepted++;
      accepted = 1;

    } else { // reject the move

      if (!rot) {
        restore_displacement(&particleList[particle_idx], last_pos);
      } else {
        restore_rotation(&particleList[particle_idx], last_pos);
      }
      particle_energies[particle_idx] = current_particle_energy;
      nrejected++;
    }

    // note: S = log[g(E)]
    // update log_g at the current bin: S = S * f
    // update energy histogram
    if(total_energy < Emin ||  total_energy > Emax) {
      printf("total energy out of bounds: %f\n", total_energy);
      exit(1);
    }

    current_e_bin = (int)((total_energy - Emin) / dE);
    if (current_e_bin >= 0 && current_e_bin < nbins) {
      log_g[current_e_bin] += log_f;
      hE[current_e_bin] += 1.0;
    }

    #ifdef WL_DEBUG
    printf("accepted = %d: current_e_bin %d; hE: %f; "
      "current log g: %f; trial log g: %f\n",
      accepted, current_e_bin, hE[current_e_bin], log_g[current_e_bin], trial_log_g);
    #endif
  }
}

/*---------------------------------------------------------------------------
  output current h(E) and S(E)
-----------------------------------------------------------------------------*/

void MCWangLandau::output_histograms(const char* filename)
{
  ofstream ofs;
  ofs.open(filename);
  for (int i = 0; i < nbins; i++)
    ofs << Emin + i * dE << " " << hE[i] << " " << log_g[i] << "\n";
  ofs.close();
}

