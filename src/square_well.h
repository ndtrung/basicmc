/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Square-well/square-shoulder interaction:
    V(r) = +infinity for r < sigma
         = epsilon   for sigma < r < lambda*sigma
         = 0         for r >= lambda*sigma

    epsilon < 0: square-well
    epsilon = 0: hard-sphere
    epsilon > 0: square-shoulder

  Trung Nguyen (Northwestern)
  Start: Jan 13, 2018
*/

#ifndef _SQUARE_WELL_H
#define _SQUARE_WELL_H

#include <cmath>
#include "evaluator.h"
#include "lattice.h"

namespace MC {

#define BIG 1.0e+20

class SquareWell : public Evaluator
{
public:
  SquareWell() {}
  SquareWell(double _sigma, double _lambda, double _epsilon) { 
    sigma = _sigma;
    lambda = _lambda;
    epsilon = _epsilon;

    cutsq_hardsphere = sigma*sigma;
    cutoff = sigma+lambda*sigma;
    cutsq = cutoff*cutoff;
  }
  virtual ~SquareWell() {}

  void set_params(double _sigma, double _lambda, double _epsilon) {
    sigma = _sigma;
    lambda = _lambda;
    epsilon = _epsilon;

    cutsq_hardsphere = sigma*sigma;
    cutoff = sigma+lambda*sigma;
    cutsq = cutoff*cutoff;
  }

  void compute(const double rsq, double& energy) {
    if (rsq < cutsq_hardsphere) {
      energy = BIG;
    } else if (rsq < cutsq) {
      energy = epsilon;
    } else energy = 0;
  }

protected:
  double epsilon, sigma, lambda;
  double cutsq_hardsphere;
  double cutsq;
};

} // namespace MC

#endif

