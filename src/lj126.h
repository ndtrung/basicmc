/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Lennard Jones 12-6

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017
*/

#ifndef _LJ126_H
#define _LJ126_H

#include <cmath>
#include "evaluator.h"
#include "lattice.h"

namespace MC {

class LJ126 : public Evaluator
{
public:
  LJ126()
  {
    epsilon = 1.0; sigma = 1.0; cutoff = 2.5;
  }

  LJ126(double _epsilon, double _sigma, double _cutoff)
  { 
    epsilon = _epsilon; sigma = _sigma; cutoff = _cutoff;
  }

  virtual ~LJ126() {}

  void set_params(double _epsilon, double _sigma, double _cutoff)
  {
    epsilon = _epsilon; sigma = _sigma; cutoff = _cutoff;
  }

  void compute(const double rsq, double& energy) {
    double r6inv = rsq*rsq*rsq;
    r6inv = 1.0/r6inv;
    energy = 4.0*epsilon*r6inv*(r6inv-1.0);
  }

  void compute(const double rsq, double& energy, double& virial) {
    double r6inv = rsq*rsq*rsq;
    r6inv = 1.0/r6inv;
    energy = 4.0*epsilon*r6inv*(r6inv-1.0);
    virial = 48.0*epsilon*r6inv*(r6inv-0.5);
  }

  void get_energy_correction(const double cutoff, const double rho,
     double& ecor)
  {
    double rc3inv = 1.0/(cutoff * cutoff * cutoff);
    ecor = 8.0*M_PI*rho*(rc3inv*rc3inv*rc3inv/9.0-rc3inv/3.0);
  }

  void get_engv_corrections(const double cutoff, const double rho,
    double& ecor, double& pcor)
  {
    double rc3inv = 1.0/(cutoff * cutoff * cutoff);
    ecor = 8.0*M_PI*rho*(rc3inv*rc3inv*rc3inv/9.0-rc3inv/3.0);
    pcor = 16.0/3.0*M_PI*rho*rho*(2.0/3.0*rc3inv*rc3inv*rc3inv - rc3inv);
  }

protected:
  double epsilon;
  double sigma;
};

} // namespace MC

#endif

