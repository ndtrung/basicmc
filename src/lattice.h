/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Trung Nguyen (Northwestern)
  Start: Aug 15, 2017
*/

#ifndef _LATTICE_H
#define _LATTICE_H

#include <vector>

// maximum number of neighbors per vertex
#define MAX_NEIGH 8 

namespace MC {

struct Vertex
{
public:
  int occupied;
  double x, y, z;
  int num_neigh;
  int neighbors[MAX_NEIGH];
};

struct Edge
{
  int vi, vj;
};

class Box
{
public:
  Box() { n_dim = 3; }

  Box(const Box& b)
  {
    Lx = b.Lx; Ly = b.Ly; Lz = b.Lz;
    xlo = b.xlo; xhi = b.xhi;
    ylo = b.ylo; yhi = b.yhi;
    zlo = b.zlo; zhi = b.zhi;
  }

  Box(double _Lx, double _Ly, double _Lz)
  {
    Lx = _Lx; Ly = _Ly; Lz = _Lz;
    xlo = 0; xhi = _Lx;
    ylo = 0; yhi = _Ly;
    zlo = 0; zhi = _Lz;
  }

  Box(int _dim)
  {
    n_dim = _dim;
  }

  Box& operator = (const Box& b)
  {
    if (&b == this) return *this;
    Lx = b.Lx; Ly = b.Ly; Lz = b.Lz;
    xlo = b.xlo; xhi = b.xhi;
    ylo = b.ylo; yhi = b.yhi;
    zlo = b.zlo; zhi = b.zhi;
    return *this;
  }

  ~Box() {}

  double Lx, Ly, Lz;
  double xlo, xhi, ylo, yhi, zlo, zhi;

protected:
  int n_dim;
};

class Lattice : public Box
{
public:
  Lattice();
  Lattice(int _dim);
  ~Lattice();

  virtual void assign_vertices(double** x, int num_vertices);
  virtual void assign_edges(Edge* edgeList, int num_edges);

  int get_vertices(const char* filename);
  int get_edges(const char* filename);

  void output(const char* filename);

  double Lx, Ly, Lz;
  double xlo, xhi, ylo, yhi, zlo, zhi;
  Vertex* vertexList;
  int num_vertices;

protected:

};


} // namespace MC

#endif

