/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Metropolis isobaric-isothermal NPT Monte-Carlo simulation

  Trung Nguyen (Northwestern)
  Start: Nov 8, 2017
*/

#ifndef _MC_NPT_H
#define _MC_NPT_H

#include "monte_carlo.h"

namespace MC {

class MCNPT : public MonteCarlo
{
public:
  MCNPT(Particle*& _particleList, class RandomMC* _random, int _nparticles,
    double _temperature, double _pressure);
  MCNPT(class Box* _box, Particle*& _particleList,
    class RandomMC* _random, int _nparticles, double _temperature, double _pressure);
  ~MCNPT();

  void init();
  void setup();
  void run(int nrelaxations, int ncycles);
  void set_num_attempts(int num_displacements, int num_vol_exchanges);
  void thermo(int n);

  virtual void sampling_npt(double&, int, int);

protected:
  double logdVmax;            // maximum logdV trials
  double pressure;            // pressure
  double temperature;         // temperature
  double total_energy;        // total energy
  double virial;              // virial
  double volume;              // box volume
  double rho;                 // number density
  double vpress;              // pressure from virial
  int particle_idx;           // chosen particle index
  double **s;                 // saved particle scaled coordinates
  double energy_correction;   // energy and pressure correction terms due to truncation
  double pressure_correction;

  // trial moves
  double particle_displacement();
  double volume_scaling();

  // restore box dims and particle coordinates if the trial volume change is rejected
  void restore_all_particles();

  int naccepted_displacement; // number of accepted/rejected moves
  int nrejected_displacement;
  int naccepted_vol_scaling;
  int nrejected_vol_scaling;

  int num_displacements;      // number of attempts per cycle
  int num_vol_exchanges;

};

} // namespace MC

#endif

