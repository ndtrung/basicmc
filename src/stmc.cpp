/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Statistical temperature Monte-Carlo simulation 

  Reference: Kim, Straub, Keyes, PRL 2006, 97:050601.

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017
*/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <cassert>

#include "stmc.h"
#include "buildingblocks.h"
#include "lattice.h"
#include "io.h"
#include "memory.h"
#include "random_mc.h"

using namespace MC;
using namespace std;

#define MAX_ITER 100
#define EPSILON 1e-2
//#ifdef STMC_DEBUG

/*---------------------------------------------------------------------------*/

STMC::STMC(Particle*& _particleList, class RandomMC* _random, int _nparticles)
 : MonteCarlo(_particleList, _random, _nparticles)
{
  dmax = 0.25;
  energy_user = 0;
  nbins = 50;
  dE = 0;
  dT = 0;
  f_d = 0.0001;
  threshold = 0.1;

  TE = hE = LE = NULL;
}

/*---------------------------------------------------------------------------*/

STMC::STMC(class Box* _box, Particle*& _particleList, class RandomMC* _random,
  int _nparticles) : MonteCarlo(_box, _particleList, _random, _nparticles)
{
  dmax = 0.25;
  energy_user = 0;
  nbins = 100;
  dT = 0;
  f_d = 0.0001;
  threshold = 0.1;

  TE = hE = LE = NULL;
}

/*---------------------------------------------------------------------------*/

STMC::~STMC()
{
  if (LE) destroy(LE);
  if (hE) destroy(hE);
  if (TE) destroy(TE);
}

/*---------------------------------------------------------------------------*/

void STMC::init()
{
  MonteCarlo::init();

  printf("Running Statistical Temperature MC simulation..\n");
  if (logfile) fprintf(logfile, "Running Statistical Temperature MC simulation..\n");
}

/*---------------------------------------------------------------------------*/

void STMC::setup()
{
  MonteCarlo::setup();

  double volume = box->Lx * box->Ly * box->Lz;
  double rho = (double)nparticles/volume;
  double cutoff = evaluator->get_cutoff();
  evaluator->get_energy_correction(cutoff, rho, energy_correction);

  // cell list setup

  cell = new Cell<Particle>(box, particleList, nparticles, 2.0*cutoff);
  cell->setup();

  // compute total energy and virial

  compute_total_energy(total_energy, virial);
  total_energy += energy_correction;

  // estimates for energy range

  if (!energy_user) {
    Emin = -2.0*nparticles;
    Emax = 2.0*nparticles;
  }

  if (dE == 0) {
    dE = (Emax - Emin) / nbins;
  }

  assert(dE > 0);
  nbins = (int)((Emax - Emin) / dE) + 1;

  create(hE, nbins);
  create(TE, nbins);
  create(LE, nbins);

  // initialize the statistical temperature to be Tmax

  for (int i = 0; i < nbins; i++) TE[i] = Tmax;
  for (int i = 0; i < nbins; i++) hE[i] = 0;
  for (int i = 0; i < nbins; i++) LE[i] = 0;

  flattening = 1;

  // write out the initial configuration

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "config", 0);

}

/*---------------------------------------------------------------------------*/

void STMC::set_energy_range(const double _Emin, const double _Emax)
{
  Emin = _Emin;
  Emax = _Emax;
  energy_user = 1;
}

/*---------------------------------------------------------------------------*/

void STMC::set_temperature_range(const double _Tmin, const double _Tmax)
{
  Tmin = _Tmin;
  Tmax = _Tmax;
}

/*---------------------------------------------------------------------------*/

void STMC::run(int nrelaxations, int ncycles)
{
  // initialize the particles, if needed

  init();

  // setting up the simulation run

  setup();

  double volume = box->Lx * box->Ly * box->Lz;
  std::vector<double> energies;

  printf("Number density = %f; Total energy = %f\n",
    (double)nNumTotalBeads/volume, total_energy);
  printf("E = %f %f; nbins = %d, dE = %f\n", Emin, Emax, nbins, dE);
  printf("T = %f %f\n", Tmin, Tmax);

  if (logfile) {
    fprintf(logfile, "Number density = %f; Total energy = %f\n",
      (double)nNumTotalBeads/volume, total_energy);
    fprintf(logfile, "E = %f %f; nbins = %d, dE = %f\n", Emin, Emax, nbins, dE);
    fprintf(logfile, "T = %f %f\n", Tmin, Tmax);
    fflush(logfile);
  }

  printf("STMC iteration for %d cycles; nrelaxations = %d\n",
    ncycles, nrelaxations);
  printf("Cycle Energy TEmin acceptance ratio f_d Flatness\n");

  if (logfile) {
    fprintf(logfile, "\nSTMC iteration for %d cycles; nrelaxations = %d\n",
      ncycles, nrelaxations);
    fprintf(logfile, "Cycle Energy TEmin acceptance ratio f_d Flatness\n");
    fflush(logfile);
  }

  // main iteration

  clock_t start, elapsed;
  start = clock();

  int ntrials = nrelaxations;
  double TEmin=Tmax;

  for (int n = 0; n <= ncycles; n++) {

    naccepted = 0;
    nrejected = 0;
    double acceptance_ratio = 0;

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++) {
      sampling_stmc();
    }

    // recompute the total energy

    compute_total_energy(total_energy, virial);
    total_energy += energy_correction;
    energies.push_back(total_energy);

    if (flattening) TEmin = low_end_flattening();

    acceptance_ratio = (double)naccepted/(double)(naccepted+nrejected);

    if (n % sampling_period == 0 || n == ncycles) {

      // check for flatness in hE

      double fluctuation = 0;
      int histrogram_flat = check_histogram_flatness(hE, nbins, threshold, fluctuation);

      printf("%8d %8g %0.3f %0.3f %0.3f %0.3f\n",
        n, total_energy, TEmin, acceptance_ratio, f_d, fluctuation);
      if (logfile) {
        fprintf(logfile, "%8d %8g %0.3f %0.3f %0.3f %0.3f\n",
          n, total_energy, TEmin, acceptance_ratio, f_d, fluctuation);
        fflush(logfile);
      }

      // reduce f <- sqrt(f) = sqrt(1 + f_d), then recompute f_d
      // reset energy histogram

      if (histrogram_flat) {
        f_d = sqrt(1 + f_d) - 1;
        for (int m = 0; m < nbins; m++) hE[m] = 0;
      }
    }

    if (n % dump_period == 0) {

      compute_total_energy(total_energy, virial);
      total_energy += energy_correction;

      ofstream ofs;

      ofs.open("energies.txt");
      int num = (int)energies.size();
      for (int m = 0; m < num; m++)
        ofs << m << " " << energies[m] << "\n";
      ofs.close();

      char filename[64];
      sprintf(filename, "T_E_%d.txt", n);
      ofs.open(filename);
      for (int m = 0; m < nbins; m++) {
        double eng = Emin + m * dE;
        double S_E = interpolate(eng, Emin, dE, TE, nbins);
        ofs << eng << " " << TE[m] << " " << hE[m] << " " << S_E << "\n";
      }
      ofs.close();

      io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
         box->ylo, box->yhi, box->zlo, box->zhi, "config", n);     
    }

    ncurrent_cycle++; 
  }

  elapsed = clock()-start;
  printf("\nLoop time of %f seconds for %d cycles with %d particles\n",
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  if (logfile) {
    fprintf(logfile, "\nLoop time of %f seconds for %d cycles with %d particles\n", 
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  }

  // final histogram

  double mean, stdev;
  create_histogram(energies, "hist_energy_final.txt", mean, stdev);
}

/*---------------------------------------------------------------------------
  sample with S(E), update T(E)
  update total_energy with trial moves accepted or rejected
-----------------------------------------------------------------------------*/

void STMC::sampling_stmc()
{
  // update LE at grid points, then used for interpolation

  approximate_integral();

  // randomly pick a particle idx

  while (1) {
    double r = random->random_mars();
    particle_idx = r * nparticles;
    if (particle_idx >= 0 && particle_idx < nparticles) break;
  }

  // compute the current energy of the chosen particle

  double current_particle_energy = compute_particle_energy(particle_idx);
  int idx_in_last_cell = idx_in_cell;

  // attempt a trial move to the chosen particle

  int accepted = 0;
  double last_pos[4];
  int rot = 0;
  if (particleList[particle_idx].rot_flag) {
    if (random->random_mars() < 0.5) {
      trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
    } else {
      trial_rotation(&particleList[particle_idx], rmax, last_pos);
      rot = 1;
    }
  } else {
    trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
  }

  // compute the particle energy due to the trial move

  double trial_particle_energy = compute_particle_energy(particle_idx);
  double delta_E = trial_particle_energy - current_particle_energy;

  // change in total energy is still on the order of delta_E - a trial move by a particle

  double trial_energy = total_energy + delta_E;
  if (trial_energy < Emin || trial_energy > Emax) { // out of energy range, reject right away

    if (!rot) {
      restore_displacement(&particleList[particle_idx], last_pos);
    } else {
      restore_rotation(&particleList[particle_idx], last_pos);
    }
    particle_energies[particle_idx] = current_particle_energy;
    nrejected++;

  } else {

    // accept or reject the trial move based on P(E) ~ 1/exp(-S(E)) similar to Wang-Landau
    double current_S_E = interpolate(total_energy, Emin, dE, TE, nbins);
    double trial_S_E   = interpolate(trial_energy, Emin, dE, TE, nbins);
    double delta_S_E    = current_S_E - trial_S_E;

    double p = exp(delta_S_E);
    if (p >= 1.0 || random->random_mars() < p) { // accept the move
      cell->update(particle_idx, idx_in_last_cell, last_pos);
      total_energy = trial_energy;
      naccepted++;
      accepted = 1;

    } else { // reject the move
      if (!rot) {
        restore_displacement(&particleList[particle_idx], last_pos);
      } else {
        restore_rotation(&particleList[particle_idx], last_pos);
      }
      particle_energies[particle_idx] = current_particle_energy;
      nrejected++;
    }

    int current_e_bin = (int)((total_energy - Emin) / dE);
    if (current_e_bin < 0 || current_e_bin >= nbins) return;

    hE[current_e_bin] += 1.0;

    // update T(E) at current_e_bin-1 and current_e_bin+1

    double delta_f = 0.5 * (log(1.0 + f_d) / dE);
    double alpha, T;
    if (current_e_bin >= 1) {
      alpha = 1.0 / (1.0 + delta_f * TE[current_e_bin - 1]);
      T = alpha * TE[current_e_bin - 1];
      if (T < Tmin) T = Tmin;
      else if (T > Tmax) T = Tmax;
      TE[current_e_bin - 1] = T;
    }

    if (current_e_bin < nbins - 1) {
      alpha = 1.0 / (1.0 - delta_f * TE[current_e_bin + 1]);
      T = alpha * TE[current_e_bin + 1];
      if (T < Tmin) T = Tmin;
      else if (T > Tmax) T = Tmax;
      TE[current_e_bin + 1] = T;
    }

    #ifdef STMC_DEBUG
    printf("accepted = %d: current_e_bin %d; hE: %f; "
      "current SE %f; trial SE %f; TE: %f %f %f\n",
      accepted, current_e_bin, hE[current_e_bin], current_S_E, trial_S_E,
      TE[current_e_bin - 1], TE[current_e_bin], TE[current_e_bin + 1]);
    #endif
  }
}

/*---------------------------------------------------------------------------
  flattening T(E) at the lower energy region
-----------------------------------------------------------------------------*/

double STMC::low_end_flattening()
{
  int i_min = 0;
  double T = TE[0];
  for (int i = 0; i < nbins; i++) {
    if (T > TE[i]) {
      T = TE[i];
      i_min = i;
    }
  }
    
  if (T >= Tmin) {
    for (int i = 0; i <= i_min; i++)
      TE[i] = T;
    if (fabs(T - Tmin) <= EPSILON) flattening = 0;
  }
  return T;
}

/*---------------------------------------------------------------------------
 compute L_E for energy E between j and (j+1)
   L_j(E) = 1/lambda_{j-1} log[1 + lambda_{j-1} * (E - E_{j-1}) / T_{j-1})]
 where
   lambda_{j-1} = (T_{j} - T_{j-1}) / dE

 At grid points E_j:
   L_j(E_j) = 1/lambda_{j-1} log[1 + lambda_{j-1} * (E_j - E_{j-1}) / T_{j-1})]
            = 1/lambda_{j-1} log[1 + lambda_{j-1} * dE / T_{j-1})]
            = 1/lambda_{j-1} log[1 + (T_{j} - T_{j-1}) / T_{j-1})]
 need to be called every time, which makes the update nonlocal

 see Eqs. 4 and 5 in the Reference.
-----------------------------------------------------------------------------*/

void STMC::approximate_integral()
{
  LE[0] = 0;
  for (int i = 1; i < nbins; i++) {
    double lambda_p = (TE[i] - TE[i-1]) / dE; // lambda[i-1]
//    if (lambda_p) LE[i] = log(1 + (TE[i] - TE[i-1])/TE[i-1]) / lambda_p;
//    else LE[i] = 0;
    if (lambda_p) LE[i] = log(1 + lambda_p * dE / TE[i-1]) / lambda_p;
    else LE[i] = 0;
  }

}

/*---------------------------------------------------------------------------
  interpolate for S(E) between T(E_i) and T(E_{i+1})
    for energy E between E_i and E_{i+1} (see Eq. 5 in the Reference)
-----------------------------------------------------------------------------*/

double STMC::interpolate(const double E, const double Emin, const double dE,
  const double* TE, const int nbins)
{
  int i = (int)((E - Emin) / dE);

  double E_i = Emin + i * dE;
  double E_next = E_i + dE; // E_{i+1}
  if (i == nbins - 1) E_next = Emax;
  double E_m = 0.5 * (E_i + E_next); // \bar{E}

  int istar = i;
  if (E < E_m) istar = i - 1;

  double lambda_i; // lambda[i]
  if (i == nbins - 1) lambda_i = 0;
  else lambda_i = (TE[i+1] - TE[i]) / dE;

  double S_E = 0;
  if (lambda_i) S_E = log(1 + lambda_i * (E - E_i)/TE[i]) / lambda_i; // L_{i+1}(E)
  for (int j = 1; j <= istar; j++) // sum^{istar}_{l+1} L_j(E_j)
    S_E += LE[j];

  return S_E;
}

/*---------------------------------------------------------------------------
  output current h(E) and T(E)
-----------------------------------------------------------------------------*/

void STMC::output_histograms(const char* filename)
{
  ofstream ofs;
  ofs.open(filename);
  for (int i = 0; i < nbins; i++)
    ofs << Emin + i * dE << " " << hE[i] << " " << TE[i] << "\n";
  ofs.close();
}

