/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Gibbs ensemble Monte Carlo simulation

  Trung Nguyen (Northwestern)
  Start: Nov 23, 2017
*/

#ifndef _GEMC_H
#define _GEMC_H

#include "monte_carlo.h"

namespace MC {

class GEMC : public MonteCarlo
{
public:
  GEMC(Particle*& _particleList, class RandomMC* _random, int _nparticles,
    double _temperature);
  GEMC(class Box* _box, Particle*& _particleList,
    class RandomMC* _random, int _nparticles, double _temperature);
  ~GEMC();

  void init();
  void setup();
  void run(int nrelaxations, int ncycles);
  void thermo(int n);
  void sampling_gemc(int ntrials);
  void set_num_attempts(int num_displacements, int num_vol_exchanges,
    int num_particle_exchanges);

protected:
  double temperature;         // temperature
  double rho, rho2;           // total densities
  double Wbox, Wbox2;         // chemical potentials: mu = -ln Wbox/beta
  double dmax;                // maximum displacement trials
  double logdVmax;            // log of maximum volume changes
  double energy, energy2;     // energies of the particles in two boxes
  double virial, virial2;     // virials in two boxes
  double total_volume;        // total volume of the two box (held constant)

  // trial moves
  double particle_displacement(Particle* list, Cell<Particle>* cell,
    Box* box, const int& nparticles, double& energy);
  double volume_exchange();
  void particle_exchange();

  // compute energy of particle idx with the rest of the specified box
  double compute_particle_energy(const int idx,
    Particle* list, const int nparticles, Box* box);

  double compute_particle_energy(const int idx, Particle* list, 
   Cell<Particle>* cell, const int n, Box* box);

  // compute total energy of the specified box
  void compute_total_energy(Particle* list,
    const int nparticles, Box* box, double &eng, double &virial);

  void compute_total_energy(Particle* list,
    const int nparticles, Box* box, Cell<Particle>* cell, double &eng, double &virial);

  // rescale the particle coords in the specified box
  void rescale_box(Particle* list, const int n,
    Box* box, const double scale_L, double** scaledcoords);

  // transfer a particle from a box to the other
  int particle_transfer(Particle* list1, Cell<Particle>* cell1, int& nparticles1, Box* box1,
    Particle*& list2, Cell<Particle>* cell2, double**& s2, int& nparticles2, int& nmax2, Box* box2, double& Wb2);

  // restore the chosen particle's coordinates
  void restore(Particle& particle, const double* backup);

  // restore the whole box and particles
  void restore_box(Particle* list, const int n, Box* old_box,
    Box* box, double** scaledcoords);

  void remove_particle(Particle* list, Cell<Particle>* cell, int particle_idx,
    int last_cell_idx, int idx_in_last_cell, int& num_particles);

  // grow the particle list when a new particle is added (in incremental of DELTA elements)
  void grow_arrays(Particle*& list, double**& scaledcoords,
    const int n, const int new_size);

  double cutoff;              // assume some cutoff for pair-wise interaction
  int particle_idx;           // chosen particle index
  double backup[3];           // saved particle coordinates
  double **s;                 // saved particle scaled coordinates in box 1
  double **s2;                // saved particle scaled coordinates in box 2

  double energy_correction;   // energy and pressure correction terms (not used yet)
  double pressure_correction;

  int nparticles2;            // number of particles in the 2nd box
  int nmax2;                  // maximum number of particles in the 2nd box
  Particle* particleList2;    // particles in the 2nd box
  Box* box2;                  // the 2nd box
  Cell<Particle>* cell2;

  int num_displacements;      // number of attempts per cycle
  int num_vol_exchanges;
  int num_particle_exchanges;

  int naccepted_displacement; // number of accepted/rejected attempts
  int nrejected_displacement;
  int naccepted_vol_exchange;
  int nrejected_vol_exchange;
  int naccepted_exchange;
  int nrejected_exchange;

};

} // namespace MC

#endif

