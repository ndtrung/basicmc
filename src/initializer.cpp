/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Implement for system initialization

  Trung Nguyen (Northwestern)
  Start: Dec 10, 2017
*/

#include <cassert>

#include "initializer.h"
#include "buildingblocks.h"
#include "lattice.h"

using namespace MC;
using namespace std;

/*---------------------------------------------------------------------------
  intialize the particles on a simple cubic lattice in a cubic box
-----------------------------------------------------------------------------*/

template <class ParticleType>
void Initializer<ParticleType>::create_box(Box*& box, ParticleType*& particleList,
   int nparticles, double lx, double ly, double lz, double ax, double ay, double az)
{
  // create the simulation box

  box = new Box(lx, ly, lz);

  // allocate and initialize the single-bead particles
  
  int nx = lx / ax - 1;
  int ny = ly / ay - 1;
  int nz = nparticles / (nx * ny) + 1;

  assert(nx * ny * nz > nparticles);

  particleList = new ParticleType [nparticles];

  for (int i = 0; i < nparticles; i++) {
    int iz = i / (nx * ny);
    int iy = (i % (nx * ny)) / nx;
    int ix = (i % (nx * ny)) % nx;
    double x = ix * ax;
    double y = iy * ay;
    double z = iz * az;

    // apply periodic boundary conditions

    if (x < box->xlo) x += box->Lx;
    else if (x > box->xhi) x -= box->Lx;
    if (y < box->ylo) y += box->Ly;
    else if (y > box->xhi) y -= box->Ly;
    if (z < box->zlo) z += box->Lz;
    else if (z > box->zhi) z -= box->Lz;

    particleList[i].beadList[0].x = x;
    particleList[i].beadList[0].y = y;
    particleList[i].beadList[0].z = z; 
  }

}

template class Initializer<Particle>;

