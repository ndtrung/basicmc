/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Gibbs ensemble Monte-Carlo simulation 

  Initialize the simulation with two boxes with the same number of particles and box volume
  so that the initial density is in the unstable regime where phase seperation can spontaneously occur.

  Trung Nguyen (Northwestern)
  Start: Nov 23, 2017
*/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>

#include "gemc.h"
#include "buildingblocks.h"
#include "io.h"
#include "lattice.h"
#include "memory.h"
#include "random_mc.h"

using namespace MC;
using namespace std;

enum {ACCEPT=0,REJECT=1};

#define DELTA 1000

/*---------------------------------------------------------------------------*/

GEMC::GEMC(Particle*& _particleList, RandomMC* _random, int _nparticles, double _temperature)
 : MonteCarlo(_particleList, _random, _nparticles), temperature(_temperature)
{
  dmax = 0.5;
  rmax = 0.5;
  logdVmax = 0.1;

  num_displacements = nparticles;
  num_vol_exchanges = (int)(0.1*nparticles) + 1;;
  num_particle_exchanges = (int)(0.5*nparticles) + 1;

  naccepted_displacement = 0;
  nrejected_displacement = 0;
  naccepted_vol_exchange = 0;
  nrejected_vol_exchange = 0;
  naccepted_exchange = 0;
  nrejected_exchange = 0;

  s = NULL;
  s2 = NULL;
  particleList2 = NULL;
  box2 = NULL;

  box_changed = 1;
}

/*---------------------------------------------------------------------------*/

GEMC::GEMC(class Box* _box, Particle*& _particleList, RandomMC* _random,
  int _nparticles, double _temperature)
  : MonteCarlo(_box, _particleList, _random, _nparticles), temperature(_temperature)
{
  dmax = 0.5;
  rmax = 0.5;
  logdVmax = 0.1;

  num_displacements = nparticles;
  num_vol_exchanges = (int)(0.1*nparticles) + 1;;
  num_particle_exchanges = (int)(0.5*nparticles) + 1;

  naccepted_displacement = 0;
  nrejected_displacement = 0;
  naccepted_vol_exchange = 0;
  nrejected_vol_exchange = 0;
  naccepted_exchange = 0;
  nrejected_exchange = 0;

  s = NULL;
  s2 = NULL;
  particleList2 = NULL;
  box2 = NULL;
  cell2 = NULL;

  box_changed = 1;
}

/*---------------------------------------------------------------------------*/

GEMC::~GEMC()
{
  if (cell2) delete cell2;
  if (box2) delete box2;
  if (particleList2) delete [] particleList2;
  destroy(s);
  destroy(s2);
}

/*---------------------------------------------------------------------------*/

void GEMC::set_num_attempts(int _num_displacements, int _num_vol_exchanges,
  int _num_particle_exchanges)
{
  num_displacements = _num_displacements;
  num_vol_exchanges = _num_vol_exchanges;
  num_particle_exchanges = _num_particle_exchanges;
}

/*---------------------------------------------------------------------------*/

void GEMC::init()
{
  MonteCarlo::init();

  printf("Running Gibbs ensemble MC simulation..\n");
  if (logfile) fprintf(logfile, "Running Gibbs ensemble MC simulation..\n");
}

/*---------------------------------------------------------------------------*/

void GEMC::setup()
{
  MonteCarlo::setup();

  double cutoff = evaluator->get_cutoff();

  // create the 2nd particle list and box

  box2 = new Box(box->Lx, box->Ly, box->Lz);
  nparticles2 = nparticles;
  nmax2 = nparticles;
  particleList2 = new Particle [nparticles];
  for (int i = 0; i < nparticles; i++) {
    particleList2[i] = particleList[i];
  }

  // initial volumes
  double volume = box->Lx * box->Ly * box->Lz;
  double volume2 = box2->Lx * box2->Ly * box2->Lz;
  total_volume = volume + volume2;

  // cell list setup

  cell = new Cell<Particle>(box, particleList, nparticles, 2.0*cutoff);
  cell->setup();
  cell2 = new Cell<Particle>(box2, particleList2, nparticles2, 2.0*cutoff);
  cell2->setup();

  // put the particle into the cells

  cell->bin_particles();
  cell2->bin_particles();

  // compute total energy and virial for two boxes

  compute_total_energy(particleList, nparticles, box, cell, energy, virial);
  compute_total_energy(particleList2, nparticles2, box2, cell2, energy2, virial2);

  // allocate backup scaled coordinates

  create(s, nparticles, 3);
  create(s2, nparticles2, 3);

  printf("Number of particles = %d %d\nNumber density = %g %g\nTemperature = %g\n"
    "Energy = %g %g\n", nparticles, nparticles2, (double)nparticles/volume,
    (double)nparticles2/volume2, temperature, energy, energy2);
  if (logfile) {
    fprintf(logfile, "Number of particles = %d %d\nNumber density = %g %g\nTemperature = %g\n"
      "Energy = %g %g\n", nparticles, nparticles2, (double)nparticles/volume,
      (double)nparticles2/volume2, temperature, energy, energy2);
    fflush(logfile);
  }

  // write out the initial configuration

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "config", 0);
}

/*---------------------------------------------------------------------------*/

void GEMC::run(int nrelaxations, int ncycles)
{
  // initialize the particles, if needed

  init();

  // setting up the simulation run

  setup();

  // looping through the cycles

  double acceptance_disp, acceptance_vol, acceptance_parexc;
  double current_energy;
  double mean, stdev;

  std::vector<double> energies, densities;
  std::vector<double> energies2, densities2;
  std::vector<double> Wbox_list, Wbox2_list;

  printf("Relaxation for %d cycles\n", nrelaxations);
  printf("Cycle N_1 N_2 Energy Energy2\n");

  if (logfile) {
    fprintf(logfile, "Relaxation for %d cycles\n", nrelaxations);
    fprintf(logfile, "Cycle N_1 N_2 Energy Energy2\n");
    fflush(logfile);
  }

  int ntrials = num_displacements + num_vol_exchanges + num_particle_exchanges;
  int niterations = 0;
 
  // relaxation

  clock_t start, elapsed;
  start = clock();

  for (int n = 0; n < nrelaxations; n++) {

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++)
      sampling_gemc(ntrials);

    if (n % sampling_period == 0)
      thermo(n);

    ncurrent_cycle++;
  }

  thermo(ncurrent_cycle);

  elapsed = clock()-start;
  printf("\nLoop time of %f seconds for %d cycles with %d particles in total\n",
    (double)elapsed/CLOCKS_PER_SEC, nrelaxations, nparticles+nparticles2);

  acceptance_disp = (double)naccepted_displacement /
     (double)(naccepted_displacement+nrejected_displacement);
  acceptance_vol = 0;
  if (naccepted_vol_exchange+nrejected_vol_exchange > 0)
    acceptance_vol = (double)naccepted_vol_exchange /
          (double)(naccepted_vol_exchange+nrejected_vol_exchange);
  acceptance_parexc = 0;
  if (naccepted_exchange+nrejected_exchange > 0)
    acceptance_parexc = (double)naccepted_exchange /
          (double)(naccepted_exchange+nrejected_exchange);
  printf("Acceptance ratio: displacement %f; volume exchange %f; particle exchange %f\n",
     acceptance_disp, acceptance_vol, acceptance_parexc);

  if (logfile) {
    fprintf(logfile, "\nLoop time of %f seconds for %d cycles with %d particles in total\n", 
      (double)elapsed/CLOCKS_PER_SEC, nrelaxations, nparticles+nparticles2);
    fprintf(logfile, "Acceptance ratio: displacement %f; volume exchange %f; particle exchange %f\n",
     acceptance_disp, acceptance_vol, acceptance_parexc);
  }

  // production run
  niterations = 0;

  printf("\nProduction for %d cycles\n", ncycles);
  printf("Cycle N_1 N_2 rho_1 rho_2 Energy Energy2\n");

  if (logfile) {
    fprintf(logfile, "\nProduction for %d cycles\n", ncycles);
    fprintf(logfile, "Cycle N_1 N_2 rho_1 rho_2 Energy Energy2\n");
    fflush(logfile);
  }

  for (int n = 0; n < ncycles; n++) {

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++) {
      sampling_gemc(ntrials);

      if (niterations % sampling_period == 0) {
        densities.push_back(rho);
        energies.push_back(energy);
        Wbox_list.push_back(Wbox);
        densities2.push_back(rho2);
        energies2.push_back(energy2);
        Wbox2_list.push_back(Wbox2);
      }

      niterations++;
    }

    if (n % sampling_period == 0 || n % dump_period == 0)
      thermo(n);

    if (n % sampling_period == 0) {
      densities.push_back(rho);
      energies.push_back(energy);
      Wbox_list.push_back(Wbox);
      densities2.push_back(rho2);
      energies2.push_back(energy2);
      Wbox2_list.push_back(Wbox2);
    }

    if (n % dump_period == 0) {

      create_histogram(energies, "hist_energy.txt", mean, stdev);
      create_histogram(densities, "hist_density.txt", mean, stdev);
      create_histogram(energies2, "hist_energy2.txt", mean, stdev);
      create_histogram(densities2, "hist_density2.txt", mean, stdev);
      create_histogram(Wbox_list, "hist_Wbox.txt", mean, stdev);
      create_histogram(Wbox2_list, "hist_Wbox2.txt", mean, stdev);

      io_handle->write(particleList, nparticles, nparticles, box->xlo, box->xhi,
         box->ylo, box->yhi, box->zlo, box->zhi, "config", n);
      io_handle->write(particleList2, nparticles2, nparticles2, box2->xlo, box2->xhi,
         box2->ylo, box2->yhi, box2->zlo, box2->zhi, "config2", n);
    }

    ncurrent_cycle++;
  }

  thermo(ncycles);

  elapsed = clock()-start;
  printf("\nLoop time of %f seconds for %d cycles with %d particles in total\n",
    (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles+nparticles2);

  acceptance_disp = (double)naccepted_displacement /
     (double)(naccepted_displacement+nrejected_displacement);
  acceptance_vol = 0;
  if (naccepted_vol_exchange+nrejected_vol_exchange > 0)
    acceptance_vol = (double)naccepted_vol_exchange /
          (double)(naccepted_vol_exchange+nrejected_vol_exchange);
  acceptance_parexc = 0;
  if (naccepted_exchange+nrejected_exchange > 0)
    acceptance_parexc = (double)naccepted_exchange /
          (double)(naccepted_exchange+nrejected_exchange);
  printf("Acceptance ratio: displacement %f; volume exchange %f; particle exchange %f\n",
     acceptance_disp, acceptance_vol, acceptance_parexc);

  if (logfile) {
    fprintf(logfile, "\nLoop time of %f seconds for %d cycles with %d particles in total\n", 
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles+nparticles2);
    fprintf(logfile, "Acceptance ratio: displacement %f; volume exchange %f; particle exchange %f\n",
     acceptance_disp, acceptance_vol, acceptance_parexc);
  }

  // final histograms

  double beta = 1.0/temperature;

  create_histogram(energies, "hist_energy_final.txt", mean, stdev);
  printf("Energy: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Energy: %f +/- %f\n", mean, stdev);

  create_histogram(densities, "hist_density_final.txt", mean, stdev);
  printf("Density: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Density: %f +/- %f\n", mean, stdev);

  create_histogram(Wbox_list, "hist_Wbox_final.txt", mean, stdev);
  printf("Chemical potential: %f +/- %f\n", -log(mean)/beta, log(stdev)/beta);
  if (logfile) fprintf(logfile, "Chemical potential: %f +/- %f\n", -log(mean)/beta, log(stdev)/beta);

  create_histogram(energies2, "hist_energy2_final.txt", mean, stdev);
  printf("Energy2: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Energy2: %f +/- %f\n", mean, stdev);

  create_histogram(densities2, "hist_density2_final.txt", mean, stdev);
  printf("Density2: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Density2: %f +/- %f\n", mean, stdev);

  create_histogram(Wbox2_list, "hist_Wbox2_final.txt", mean, stdev);
  printf("Chemical potential: %f +/- %f\n", -log(mean)/beta, log(stdev)/beta);
  if (logfile) fprintf(logfile, "Chemical potential: %f +/- %f\n", -log(mean)/beta, log(stdev)/beta);
}

/*---------------------------------------------------------------------------*/

void GEMC::sampling_gemc(int ntrials)
{
  double current_energy;
  int r = (int)(random->random_mars() * ntrials);
  if (r < num_displacements) {
    // choose the box to make the trial displacement
    if (random->random_mars() < 0.5)
      current_energy = particle_displacement(particleList, cell, box, nparticles, energy);
    else
      current_energy = particle_displacement(particleList2, cell2, box2, nparticles2, energy2);

  } else if (r < num_displacements + num_vol_exchanges) {

    current_energy = volume_exchange();

  } else {

    particle_exchange();
  }
}

/*---------------------------------------------------------------------------*/

void GEMC::thermo(int n)
{
  double volume = box->Lx * box->Ly * box->Lz;
  rho = (double)nparticles/volume;
  double volume2 = box2->Lx * box2->Ly * box2->Lz;
  rho2 = (double)nparticles2/volume2;
  compute_total_energy(particleList, nparticles, box, cell, energy, virial);
  compute_total_energy(particleList2, nparticles2, box2, cell2, energy2, virial2);

  printf("%8d %8d %8d %8g %8g %8g %8g\n", n, nparticles, nparticles2, rho, rho2, energy, energy2);
  if (logfile) {
    fprintf(logfile, "%8d %8d %8d %8g %8g %8g %8g\n",
      n, nparticles, nparticles2, rho, rho2, energy, energy2);
    fflush(logfile);
  }
}

/*---------------------------------------------------------------------------
  attempt to move a particle with a box (and cell)
  return the total energy of the box in energy
  NOTE: if the density of a box gets too small, with volume exchange moves
    the box size can be smaller than dmax, the trial displacement could put
    the picked particle out of the box (even after PBC applied).
-----------------------------------------------------------------------------*/

double GEMC::particle_displacement(Particle* list, Cell<Particle>* _cell, Box* _box,
   const int& num_particles, double& energy)
{
  if (num_particles <= 0) return 0;

  double current_energy, trial_energy;
  double beta = 1.0/temperature;

  // randomly pick a particle idx

  while (1) {
    double r = random->random_mars();
    particle_idx = r * num_particles;
    if (particle_idx >= 0 && particle_idx < num_particles) break;
  }

  // compute the current energy of the chosen particle

  current_energy = compute_particle_energy(particle_idx, list, _cell, num_particles, _box);
  int idx_in_last_cell = idx_in_cell;

  // attempt a trial move to the chosen particle, backup last position
  // the trial move can make the particle move to another cell
  // but we don't need to remove the particle from the cell at this point

  double last_pos[4];
  int rot = 0;
  if (list[particle_idx].rot_flag) {
    if (random->random_mars() < 0.5) {
      trial_displacement(&list[particle_idx], _box, dmax, last_pos);
    } else {
      trial_rotation(&list[particle_idx], rmax, last_pos);
      rot = 1;
    }
  } else {
    trial_displacement(&list[particle_idx], _box, dmax, last_pos);
  }

  // TODO: only updates the involved bins

  //_cell->bin_particles();

  // compute the particle energy due to the trial move

  trial_energy = compute_particle_energy(particle_idx, list, _cell, num_particles, _box);

  double delta_E = trial_energy - current_energy;
  if (delta_E <= 0) { // accept the move

    _cell->update(particle_idx, idx_in_last_cell, last_pos);
    current_energy = trial_energy;
    energy += delta_E;
    naccepted_displacement++;

  } else {

    double p = exp(-beta*delta_E);
    if (random->random_mars() < p) { // accept

      _cell->update(particle_idx, idx_in_last_cell, last_pos);
      current_energy = trial_energy;
      energy += delta_E;
      naccepted_displacement++;

    } else { // reject

      if (!rot) {
        restore_displacement(&list[particle_idx], last_pos);
      } else {
        restore_rotation(&list[particle_idx], last_pos);
      }

      // TODO: only updates the involved bins

      //_cell->bin_particles();

      nrejected_displacement++;
    }
  }

  return current_energy;
}

/*---------------------------------------------------------------------------*/

double GEMC::volume_exchange()
{
  double current_energy, trial_energy, current_virial, trial_virial;
  double current_energy2, trial_energy2, current_virial2, trial_virial2;
  double beta = 1.0/temperature;

  // pick a random number of [-logdVmax;logdVmax]

  double r = random->random_mars();
  double delta = -logdVmax + r * (2*logdVmax);

  double current_volume = box->Lx * box->Ly * box->Lz;
  double logV = log(current_volume);
  double logVtrial = log(current_volume) + delta;
  double Vtrial = exp(logVtrial);
  double deltaV = Vtrial - current_volume;
  double scale_vol = Vtrial/current_volume;
  double scale_L = pow(scale_vol, 1.0/3.0);

  double current_volume2 = box2->Lx * box2->Ly * box2->Lz;
  double V2trial = total_volume - Vtrial;
  double deltaV2 = V2trial - current_volume2;
  double scale_vol2 = V2trial/current_volume2;
  double scale_L2 = pow(scale_vol2, 1.0/3.0);

  // compute the current energy of box 1
  compute_total_energy(particleList, nparticles, box, cell, current_energy, current_virial);

  // rescale box dims and particle coordinates
  Box old_box(box->Lx, box->Ly, box->Lz);
  rescale_box(particleList, nparticles, box, scale_L, s);
  cell->setup();
  cell->bin_particles();

  // compute the trial energy of box 1 due to the trial volume scaling
  compute_total_energy(particleList, nparticles, box, cell, trial_energy, trial_virial);

  double delta_E = trial_energy - current_energy;

  // compute the current energy of box 2
  compute_total_energy(particleList2, nparticles2, box2, cell2, current_energy2, current_virial2);

  // rescale box dims and particle coordinates
  Box old_box2(box2->Lx, box2->Ly, box2->Lz);
  rescale_box(particleList2, nparticles2, box2, scale_L2, s2);
  cell2->setup();
  cell2->bin_particles();

  // compute the trial energy of box 2 due to the trial volume scaling
  compute_total_energy(particleList2, nparticles2, box2, cell2, trial_energy2, trial_virial2);

  double delta_E2 = trial_energy2 - current_energy2;

  double boltzfactor = nparticles*log(scale_vol) + nparticles2*log(scale_vol2) -
    beta*(delta_E + delta_E2);

  if (boltzfactor >= 0) { // accept the move

    energy = trial_energy;
    energy2 = trial_energy2;
    naccepted_vol_exchange++;

  } else {

    double p = exp(boltzfactor);
    if (random->random_mars() < p) { // accept

      energy = trial_energy;
      energy2 = trial_energy2;
      naccepted_vol_exchange++;

    } else { // reject

      restore_box(particleList, nparticles, &old_box, box, s);
      cell->setup();
      cell->bin_particles();

      restore_box(particleList2, nparticles2, &old_box2, box2, s2);
      cell2->setup();
      cell2->bin_particles();

      nrejected_vol_exchange++;
    }
  }

  return 0.0;
}

/*---------------------------------------------------------------------------
  save scaled coordinates
  rescale particle coordinates into the new box
-----------------------------------------------------------------------------*/

void GEMC::rescale_box(Particle* list, const int n, Box* box, const double scale_L,
  double** scaledcoords)
{
  double old_Lx = box->Lx;
  double old_Ly = box->Ly;
  double old_Lz = box->Lz;

  double xlo = box->xlo;
  double ylo = box->ylo;
  double zlo = box->zlo;

  // make trial box dimensions

  box->Lx *= scale_L;
  box->Ly *= scale_L;
  box->Lz *= scale_L;

  box->xhi = box->xlo + box->Lx;
  box->yhi = box->ylo + box->Ly;
  box->zhi = box->zlo + box->Lz;

  // rescale the particle coordinates, fixed point at (xlo, ylo, zlo)

  for (int i = 0; i < n; i++) {

    double xtmp = list[i].beadList[0].x;
    double ytmp = list[i].beadList[0].y;
    double ztmp = list[i].beadList[0].z;

    scaledcoords[i][0] = (xtmp - xlo) / old_Lx;
    scaledcoords[i][1] = (ytmp - ylo) / old_Ly;
    scaledcoords[i][2] = (ztmp - zlo) / old_Lz;

    list[i].beadList[0].x = xlo + scaledcoords[i][0] * box->Lx;
    list[i].beadList[0].y = ylo + scaledcoords[i][1] * box->Ly;
    list[i].beadList[0].z = zlo + scaledcoords[i][2] * box->Lz;
  }

}

/*---------------------------------------------------------------------------*/

void GEMC::particle_exchange()
{
  int decision;
  double r = random->random_mars();
  if (r < 0.5) { // deletion in box 1, insertion in box 2

    decision = particle_transfer(particleList, cell, nparticles, box,
                      particleList2, cell2, s2, nparticles2, nmax2, box2, Wbox2);

  } else { // insertion in box 1, deletion in box 2

    decision = particle_transfer(particleList2, cell2, nparticles2, box2,
                      particleList, cell, s, nparticles, nmax, box, Wbox);
  }
}

/*---------------------------------------------------------------------------
  attempt to transfer a particle from box1 to box2
    delete a particle from list 1 and insert a particle to list 2
  resize particle list 2 and scaled coordinates array of needed
-----------------------------------------------------------------------------*/

int GEMC::particle_transfer(Particle* list1, Cell<Particle>* cell1, int& n1, Box* box1,
  Particle*& list2,  Cell<Particle>* cell2, double**& s2, int& n2, int& nmax2, Box* box2, double& Wb2)
{
  if (n1 <= 0) return REJECT;

  int decision;
  double beta = 1.0/temperature;

  // randomly pick a particle idx in box1 to delete

  double r;
  int particle_idx;
  while (1) {
    r = random->random_mars();
    particle_idx = r * n1;
    if (particle_idx >= 0 && particle_idx < n1) break;
  }

  double volume1 = box1->Lx * box1->Ly * box1->Lz;

  // compute the energy contribution due to the particle to the energy in box 1

  double trial_energy1 = compute_particle_energy(particle_idx, list1, cell1, n1, box1);
  int last_cell1_idx = cell_idx;
  int idx_in_last_cell1 = idx_in_cell;
  int current_nparticles1 = n1;

  // energy difference due to the deletion of the chosen particle
  double delta_E1 = -trial_energy1;

  // generate a trial particle in box 2

  double trial_x, trial_y, trial_z;
  r = random->random_mars();
  trial_x = box2->xlo + r * box2->Lx;
  r = random->random_mars();
  trial_y = box2->ylo + r * box2->Ly;
  r = random->random_mars();
  trial_z = box2->zlo + r * box2->Lz;

  if (n2 == nmax2) {
    nmax2 += DELTA;
    grow_arrays(list2, s2, n2, nmax2);
  }

  int trial_nparticles2 = n2 + 1;
  list2[n2].beadList[0].x = trial_x;
  list2[n2].beadList[0].y = trial_y;
  list2[n2].beadList[0].z = trial_z;
  int cell2_idx = cell2->add_particle(n2);
  n2++;

  double volume2 = box2->Lx * box2->Ly * box2->Lz;

  // compute the energy due to the trial insertion of the last particle

  double trial_energy2 = compute_particle_energy(n2-1, list2, cell2, n2, box2);
  int last_cell2_idx = cell_idx;
  int idx_in_last_cell2 = idx_in_cell;

  // update Wbox2 which is used for computing the chemical potential in box 2 (Frenkel Smit, Eq. 8.3.5 and page 212)
  // Wbox2 = volume2*exp(-beta*trial_energy2)/n2;
  // mu2 = -ln <Wbox2> /beta

  Wb2 = volume2*exp(-beta*trial_energy2)/n2;

  // energy difference due to the insertion of the chosen particle
  double delta_E2 = trial_energy2;

  double boltzfactor = -beta * (delta_E1 + delta_E2) +
    log((double)current_nparticles1/(double)trial_nparticles2) + log(volume2/volume1);

  if (boltzfactor >= 0) { // accept

    // remove the particle idx from box 1
    remove_particle(list1, cell1, particle_idx, last_cell1_idx, idx_in_last_cell1, n1);

    energy += delta_E1;
    energy2 += delta_E2;
    naccepted_exchange++;
    decision = ACCEPT;

  } else {

    double p = exp(boltzfactor);
    if (random->random_mars() < p) { // accept

      // remove the particle idx from box 1
      remove_particle(list1, cell1, particle_idx, last_cell1_idx, idx_in_last_cell1, n1);

      energy += delta_E1;
      energy2 += delta_E2;
      naccepted_exchange++;
      decision = ACCEPT;

    } else {  // reject

      cell2->remove_last_particle(last_cell2_idx);
      n2--;
      nrejected_exchange++;
      decision = REJECT;
    }
  }

  return decision;
}

/*---------------------------------------------------------------------------
  remove the particle from the particle list
  also update the cell
-----------------------------------------------------------------------------*/

void GEMC::remove_particle(Particle* list, Cell<Particle>* _cell, int particle_idx,
  int _cell_idx, int _pidx_in_cell, int& num_particles)
{
  // remove the particle_idx from the last cell that owns it

  _cell->remove_particle(_cell_idx, _pidx_in_cell);

  // replace the last particle (nparticles-1) in the cell list with particle_idx

  _cell->replace_particle(particle_idx, num_particles-1);

  // replace the chosen particle with the last particle in the particle list

  list[particle_idx] = list[num_particles-1];
  num_particles--;

  // update the cell list's number of particles (used for binning and total energy calculation)

  _cell->set_num_particles(num_particles);
}


/*---------------------------------------------------------------------------*/

double GEMC::compute_particle_energy(const int idx, Particle* list,
  const int n, Box* box)
{
  const double cutoff = evaluator->get_cutoff();
  const double cutsq = cutoff*cutoff;

  const double xtmp = list[idx].beadList[0].x;
  const double ytmp = list[idx].beadList[0].y;
  const double ztmp = list[idx].beadList[0].z;
  const double Lx = box->Lx;
  const double Ly = box->Ly;
  const double Lz = box->Lz;
  double eng = 0;
  double virial = 0;

  for (int i = 0; i < n; i++) {

    const Bead* const bead = &list[i].beadList[0];

    double delx = bead->x - xtmp;
    double dely = bead->y - ytmp;
    double delz = bead->z - ztmp;

    // minimum image convention

    if (delx < -0.5*Lx) delx += Lx;
    else if (delx > 0.5*Lx) delx -= Lx;
    if (dely < -0.5*Ly) dely += Ly;
    else if (dely > 0.5*Ly) dely -= Ly;
    if (delz < -0.5*Lz) delz += Lz;
    else if (delz > 0.5*Lz) delz -= Lz;

    double rsq = delx * delx + dely * dely + delz * delz;
    if (i != idx && rsq < cutsq && rsq > 0) {
      double e, v;
      evaluator->compute(rsq, e, v);
      eng += e;
      virial += v;
    }
  }

  return eng;
}

/*---------------------------------------------------------------------------
  compute the energy of a particle in a given particle list in a cell
-----------------------------------------------------------------------------*/

double GEMC::compute_particle_energy(const int idx, Particle* list,
  Cell<Particle>* _cell, const int n, Box* _box)
{
  const double cutoff = evaluator->get_cutoff();
  const double cutsq = cutoff*cutoff;
  const double xtmp = list[idx].beadList[0].x;
  const double ytmp = list[idx].beadList[0].y;
  const double ztmp = list[idx].beadList[0].z;
  const double xlo = _box->xlo;
  const double ylo = _box->ylo;
  const double zlo = _box->zlo;
  const double Lx = _box->Lx;
  const double Ly = _box->Ly;
  const double Lz = _box->Lz;

  int** const _cells = _cell->cells;
  int** const _neighbor_cells = _cell->neighbor_cells;
  int* const _num_neighbor_cells = _cell->num_neighbor_cells;
  int* const _num_particles_in_cell = _cell->num_particles_in_cell;
  
  // note that the particle may come from another cell with its coordinates
  // need to check if particle_idx is the same as those from the cell c and its neighboring cells

  cell_idx = _cell->coord2cell(xtmp, ytmp, ztmp, xlo, ylo, zlo);

  // loop through the particles within the cell c

  int found_idx = 0;
  double eng = 0;

  for (int i = 0; i < _num_particles_in_cell[cell_idx]; i++) {    
    int j = _cells[cell_idx][i];
    if (j == idx) {
      idx_in_cell = i;     // store the idx of the particle in the cell
      found_idx = 1;
      continue;
    }

    const Bead* const bead = &list[j].beadList[0];
    double delx = bead->x - xtmp;
    double dely = bead->y - ytmp;
    double delz = bead->z - ztmp;

    // minimum image convention

    if (delx < -0.5*Lx) delx += Lx;
    else if (delx > 0.5*Lx) delx -= Lx;
    if (dely < -0.5*Ly) dely += Ly;
    else if (dely > 0.5*Ly) dely -= Ly;
    if (delz < -0.5*Lz) delz += Lz;
    else if (delz > 0.5*Lz) delz -= Lz;

    double rsq = delx * delx + dely * dely + delz * delz;
    if (rsq < cutsq && rsq > 0) {
      double e;
      evaluator->compute(rsq, e);
      eng += e;
    }

  }  // end within the cell

  // now condering in the neighboring cells

  for (int n = 1; n < _num_neighbor_cells[cell_idx]; n++) {
    int cn = _neighbor_cells[cell_idx][n];
    if (cn == cell_idx) continue;

    // loop through each bead in the cell[nbr]

    for (int i = 0; i < _num_particles_in_cell[cn]; i++) {
      int j = _cell->cells[cn][i];
      if (j == idx) {
        idx_in_cell = i;     // store the idx of the particle in the cell
        if (found_idx == 1) {
          printf("ERROR: idx show up in two different cells\n");
        }
        found_idx = 1;  
        continue;
      }

      const Bead* const bead = &list[j].beadList[0];
      double delx = bead->x - xtmp;
      double dely = bead->y - ytmp;
      double delz = bead->z - ztmp;
 
      // minimum image convention

      if (delx < -0.5*Lx) delx += Lx;
      else if (delx > 0.5*Lx) delx -= Lx;
      if (dely < -0.5*Ly) dely += Ly;
      else if (dely > 0.5*Ly) dely -= Ly;
      if (delz < -0.5*Lz) delz += Lz;
      else if (delz > 0.5*Lz) delz -= Lz;

      double rsq = delx * delx + dely * dely + delz * delz;
      if (rsq < cutsq && rsq > 0) {
        double e;
        evaluator->compute(rsq, e);
        eng += e;
      }
    }
  }   // end loops over neighbor cells

  if (found_idx == 0) printf("ERROR: Cannot find idx in any cell!!!\n");
  return eng;
}

/*---------------------------------------------------------------------------*/

void GEMC::compute_total_energy(Particle* list, const int n,
  Box* box, double &eng, double &virial)
{
  const double cutoff = evaluator->get_cutoff();
  const double cutsq = cutoff*cutoff;

  const double Lx = box->Lx;
  const double Ly = box->Ly;
  const double Lz = box->Lz;

  // N^2 brute force

  eng = 0;
  virial = 0.0;

  for (int i = 0; i < n - 1; i++) {

    double xtmp = list[i].beadList[0].x;
    double ytmp = list[i].beadList[0].y;
    double ztmp = list[i].beadList[0].z;

    for (int j = i + 1; j < n; j++) {
      const Bead* const bead = &list[j].beadList[0];

      double delx = bead->x - xtmp;
      double dely = bead->y - ytmp;
      double delz = bead->z - ztmp;

      // minimum image convention

      if (delx < -0.5*Lx) delx += Lx;
      else if (delx > 0.5*Lx) delx -= Lx;
      if (dely < -0.5*Ly) dely += Ly;
      else if (dely > 0.5*Ly) dely -= Ly;
      if (delz < -0.5*Lz) delz += Lz;
      else if (delz > 0.5*Lz) delz -= Lz;

      double rsq = delx * delx + dely * dely + delz * delz;
      if (rsq < cutsq) {
        double e, v;
        evaluator->compute(rsq, e, v);
        eng += e;
        virial += v; 
      }
    }
  }

}

/*---------------------------------------------------------------------------
  compute total energy and virial for the given box and cell
-----------------------------------------------------------------------------*/

void GEMC::compute_total_energy(Particle* list, const int n,
  Box* _box, Cell<Particle>* _cell, double &eng, double &virial)
{
  eng = 0;
  virial = 0.0;
/*
  if (box_changed) {
    _cell->setup();
    _cell->bin_particles();
  }
*/
  for (int i = 0; i < n; i++) {
    double e, v = 0;
    e = compute_particle_energy(i, list, _cell, n, _box);
    eng += e;
    virial += v;
  }

  eng *= 0.5;
  virial *= 0.5;
}

/*---------------------------------------------------------------------------*/

void GEMC::restore(Particle& particle, const double* backup)
{
  particle.beadList[0].x = backup[0];
  particle.beadList[0].y = backup[1];
  particle.beadList[0].z = backup[2];
}

/*---------------------------------------------------------------------------*/

void GEMC::restore_box(Particle* list, const int n, Box* old_box,
    Box* _box, double** scaledcoords)
{
  // restore box bounds and dimensions

  _box->xhi = _box->xlo + old_box->Lx;
  _box->yhi = _box->ylo + old_box->Ly;
  _box->zhi = _box->zlo + old_box->Lz;

  _box->Lx = old_box->Lx;
  _box->Ly = old_box->Ly;
  _box->Lz = old_box->Lz;

  // restore particle coordinates from scaled coordinates and new box dims

  const double Lx = _box->Lx;
  const double Ly = _box->Ly;
  const double Lz = _box->Lz;
  const double xlo = _box->xlo;
  const double ylo = _box->ylo;
  const double zlo = _box->zlo;

  for (int i = 0; i < n; i++) {

    list[i].beadList[0].x = xlo + scaledcoords[i][0] * Lx;
    list[i].beadList[0].y = ylo + scaledcoords[i][1] * Ly;
    list[i].beadList[0].z = zlo + scaledcoords[i][2] * Lz;
  }
}

/*---------------------------------------------------------------------------*/

void GEMC::grow_arrays(Particle*& list, double**& scaledcoords, const int n,
  const int new_size)
{
  Particle* tmp = new Particle [new_size];
  for (int i = 0; i < n; i++) {
    tmp[i] = list[i];
  }
  delete [] list;
  list = tmp;

  grow(scaledcoords, new_size, 3);
}


