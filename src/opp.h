/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Oscillating pair potential (kappa, phi)
    V(r) = 1/r^15 + 1/r^3 * cos(kappa*(r - 1.25) - phi)
    dV/dr = -15/r^16 - 1/r^3 * sin(kappa*(r - 1.25) - phi) * kappa -
           3/r^4 * cos(kappa*(r - 1.25) - phi)

  Example parameter sets from Engel et al., Nat. Mater. 2015, 14, 109--16:
    Low-density         : kappa = 6.25; phi = 0.62; cutoff = 3.34
    Intermediate-density: kappa = 7.5;  phi = 0.53; cutoff = 2.978
    High-density        : kappa = 8.5;  phi = 0.68; cutoff = 2.80

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017
*/

#ifndef _OPP_H
#define _OPP_H

#include <cmath>
#include "evaluator.h"
#include "lattice.h"

namespace MC {

class Oscillating : public Evaluator
{
public:
  Oscillating() {}
  Oscillating(double _kappa, double _phi, double _cutoff) {
    kappa = _kappa; phi = _phi; cutoff = _cutoff;
  }
  virtual ~Oscillating() {}

  void set_params(double _kappa, double _phi) { kappa = _kappa; phi = _phi; }

  void compute(const double rsq, double& energy) {
    double rinv = 1.0/sqrt(rsq);
    double r3inv = rinv*rinv*rinv;
    double r15inv = r3inv*r3inv*r3inv*r3inv*r3inv;
    double cosine_term = cos(kappa*(1.0/rinv-1.25) - phi);
    energy = r15inv + r3inv*cosine_term;
  }

  void compute(const double rsq, double& energy, double& virial) {
    double rinv = 1.0/sqrt(rsq);
    double r3inv = rinv*rinv*rinv;
    double r15inv = r3inv*r3inv*r3inv*r3inv*r3inv;
    double cosine_term = cos(kappa*(1.0/rinv-1.25) - phi);
    energy = r15inv + r3inv*cosine_term;

    double sine_term = kappa * sin(kappa*(1.0/rinv-1.25) - phi);
    virial = -15.0*rinv*r15inv - r3inv*sine_term - 3.0*rinv*r3inv*cosine_term;
  }

protected:
  double kappa, phi;
};

} // namespace MC

#endif

