/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <string.h>
#include "buildingblocks.h"

using namespace std;

namespace MC {

// Particle

int Particle::id = 0;

Particle::Particle()
{
  Bead bead;
  bead.id = 0;
  bead.mol = Particle::id++;
  bead.ntype = 1;
  bead.x = 0;
  bead.y = 0;
  bead.z = 0;
  bead.q = 0;
  beadList.push_back(bead);

  rot_flag = 0;
  quat[0] = 1;
  quat[1] = 0;
  quat[2] = 0;
  quat[3] = 0;
}

Particle::Particle(const Particle& p)
{
  int mol = Particle::id++;

  for (int i = 0; i < p.beadList.size(); i++) {
    Bead bead;
    bead.id = p.beadList[i].id;
    strcpy(bead.type, p.beadList[i].type);
    bead.ntype = p.beadList[i].ntype;
    bead.mol = mol;
    bead.x = p.beadList[i].x;
    bead.y = p.beadList[i].y;
    bead.z = p.beadList[i].z;
    bead.q = p.beadList[i].q;

    beadList.push_back(bead);
  }

  rot_flag = p.rot_flag;
  quat[0] = p.quat[0];
  quat[1] = p.quat[1];
  quat[2] = p.quat[2];
  quat[3] = p.quat[3];
}

Particle::Particle(const int nbeadsperparticle)
{
  int mol = Particle::id++;

  int id = 0;
  for (int i = 0; i < nbeadsperparticle; i++) {
    Bead bead;
    bead.id = id;
    bead.mol = mol;
    bead.ntype = 1;
    bead.x = 0;
    bead.y = 0;
    bead.z = i;
    bead.q = 0;

    beadList.push_back(bead);
  }

  rot_flag = 0;
  quat[0] = 1;
  quat[1] = 0;
  quat[2] = 0;
  quat[3] = 0;
}

Particle& Particle::operator= (const Particle& p)
{
  if (&p == this)
    return *this;

  beadList.clear();
 
  for (int i = 0; i < p.beadList.size(); i++) {
    Bead bead;
    bead.id = p.beadList[i].id;
    bead.mol = p.beadList[i].mol;
    strcpy(bead.type, p.beadList[i].type);
    bead.ntype = p.beadList[i].ntype;
    bead.x = p.beadList[i].x;
    bead.y = p.beadList[i].y;
    bead.z = p.beadList[i].z;
    bead.q = p.beadList[i].q;

    beadList.push_back(bead);
  }

  rot_flag = p.rot_flag;
  quat[0] = p.quat[0];
  quat[1] = p.quat[1];
  quat[2] = p.quat[2];
  quat[3] = p.quat[3];

  return *this;
}

// Chain

int Chain::id = 0;

Chain::Chain(const Chain& p)
{
  int mol = Chain::id++;

  for (int i = 0; i < p.beadList.size(); i++) {
    Bead bead;
    bead.id = p.beadList[i].id;
    strcpy(bead.type, p.beadList[i].type);
    bead.ntype = p.beadList[i].ntype;
    bead.mol = mol;
    bead.x = p.beadList[i].x;
    bead.y = p.beadList[i].y;
    bead.z = p.beadList[i].z;
    bead.q = p.beadList[i].q;

    beadList.push_back(bead);
  }

  num_grown = p.num_grown;
}

Chain::Chain(const int nbeadsperchain)
{
  int mol = Chain::id++;
  int id = 0;
  for (int i = 0; i < nbeadsperchain; i++) {
    Bead bead;
    bead.id = id;
    bead.mol = mol;
    bead.ntype = 1;
    bead.x = 0;
    bead.y = 0;
    bead.z = i;
    bead.q = 0;

    beadList.push_back(bead);
  }

  num_grown = 0;
}

Chain& Chain::operator= (const Chain& p)
{
  if (&p == this)
    return *this;

  int mol = Chain::id++;

  beadList.clear();
 
  for (int i = 0; i < p.beadList.size(); i++) {
    Bead bead;
    bead.id = p.beadList[i].id;
    bead.mol = mol;
    strcpy(bead.type, p.beadList[i].type);
    bead.ntype = p.beadList[i].ntype;
    bead.x = p.beadList[i].x;
    bead.y = p.beadList[i].y;
    bead.z = p.beadList[i].z;
    bead.q = p.beadList[i].q;

    beadList.push_back(bead);
  }

  num_grown = p.num_grown;

  return *this;
}

} // namespace MC

