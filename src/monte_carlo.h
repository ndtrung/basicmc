/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Base class for Monte Carlo simulation methods

  Trung Nguyen (Northwestern)
  Start: Nov 8, 2017
*/

#ifndef _MONTE_CARLO_H
#define _MONTE_CARLO_H

#include "simulation.h"
#include "evaluator.h"
#include "cell.h"
#include <vector>

namespace MC {

class MonteCarlo : public Simulation
{
public:
  MonteCarlo(class Particle*& _particleList, class RandomMC* _random, int _nparticles);
  MonteCarlo(class Box* _box, class Particle*& _particleList,
    class RandomMC* _random, int _nparticles);
  ~MonteCarlo();

  virtual void init();
  virtual void setup();
  virtual void run(int nrelaxations, int ncycles) {}
  virtual void set_num_attempts(int) {}
  virtual void set_num_attempts(int, int) {}
  virtual void set_num_attempts(int, int, int) {}

  // compute energy of a particle
  double compute_particle_energy(const int idx);

  // compute total energy of the whole system
  void compute_total_energy(double &eng, double &virial);

  // attempt displacement of a particle
  void trial_displacement(Particle* particle, Box* box,
    const double dmax, double* backup);

  // restore particle coordinates from a trial displacement
  void restore_displacement(Particle* particle, const double* backup);

  // attempt rotation of a particle
  void trial_rotation(Particle* particle, const double rmax, double* backup);

  // restore particle orientation from a trial rotation
  void restore_rotation(Particle* particle, const double* backup);

  void set_target_acceptance(double value) { target_acceptance = value; }
  void set_dmax(double value) { dmax = value; }
  void set_evaluator(class Evaluator* _evaluator) { evaluator = _evaluator; }
  void set_sampling(int period) { sampling_period = period; }
  void set_dump(int period) { dump_period = period; }
  void set_log(const char* filename);
  void set_anisotropic(int _anisotropic_flag) { anisotropic_flag = _anisotropic_flag; }

  int read_restart(const char* filename);

protected:
  Box* box;
  Particle*& particleList;
  RandomMC* random;
  Evaluator* evaluator;
  Cell<Particle>* cell;

  int nparticles;             // number of particles
  int nmax;                   // maximum number of particles for allocation
  int nNumTotalBeads;         // total number of beads
  double dmax;                // maximum displacement
  double rmax;                // maximum rotation
  int ncurrent_cycle;         // read from restart
  int sampling_period;        // sampling period
  int dump_period;            // period to dumping configurations
  int idx_in_cell;            // idx of the chosen particle in a cell
  int cell_idx;               // idx of the cell that owns the chosen particle
  double target_acceptance;   // < 0 by default, if set > 0, then adjust dmax
  double* particle_energies;  // particle energies
  double* particle_virials;   // particle virials
  int allocated_size;
  int box_changed;
  int anisotropic_flag;

  void compute_particle_energy_nsq(const int, double& eng);
  void compute_particle_energy_cell(const int, double& eng);
  void compute_particle_energy_cell(const int, double& eng, double& virial);

  void compute_total_energy_nsq(double& eng, double& virial);
  void compute_total_energy_cell(double& eng, double& virial);

  void create_histogram(const std::vector<double> array,
    const char* filename, double& mean, double& stdev);

  int check_histogram_flatness(const double* histogram, const int nbins,
    const double threshold, double& fluctuation);

  class IOHandle* io_handle;
  int use_log;
  FILE* logfile;

  void print_citations();
};

} // namespace MC

#endif

