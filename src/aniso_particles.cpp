/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <vector>
#include <string.h>
#include "aniso_particles.h"

using namespace std;

namespace MC {

AnisotropicParticle::AnisotropicParticle() : Particle()
{
  rot_flag = 1;

  quat[0] = 1;
  quat[1] = 0;
  quat[2] = 0;
  quat[3] = 0;
}

AnisotropicParticle::AnisotropicParticle(const AnisotropicParticle& p)
{
  int mol = Particle::id++;

  for (int i = 0; i < p.beadList.size(); i++) {
    Bead bead;
    bead.id = p.beadList[i].id;
    strcpy(bead.type, p.beadList[i].type);
    bead.ntype = p.beadList[i].ntype;
    bead.mol = mol;
    bead.x = p.beadList[i].x;
    bead.y = p.beadList[i].y;
    bead.z = p.beadList[i].z;

    beadList.push_back(bead);
  }

  rot_flag = 1;

  quat[0] = p.quat[0];
  quat[1] = p.quat[1];
  quat[2] = p.quat[2];
  quat[3] = p.quat[3];
}

AnisotropicParticle::AnisotropicParticle(const int nbeadsperparticle)
  : Particle(nbeadsperparticle)
{
  rot_flag = 1;

  quat[0] = 1;
  quat[1] = 0;
  quat[2] = 0;
  quat[3] = 0;
}

AnisotropicParticle& AnisotropicParticle::operator= (const AnisotropicParticle& p)
{
  if (&p == this)
    return *this;

  int mol = Particle::id++;

  beadList.clear();
 
  for (int i = 0; i < p.beadList.size(); i++) {
    Bead bead;
    bead.id = p.beadList[i].id;
    bead.mol = mol;
    strcpy(bead.type, p.beadList[i].type);
    bead.ntype = p.beadList[i].ntype;
    bead.x = p.beadList[i].x;
    bead.y = p.beadList[i].y;
    bead.z = p.beadList[i].z;

    beadList.push_back(bead);
  }

  rot_flag = 1;

  quat[0] = p.quat[0];
  quat[1] = p.quat[1];
  quat[2] = p.quat[2];
  quat[3] = p.quat[3];

  return *this;
}

} // namespace MC

