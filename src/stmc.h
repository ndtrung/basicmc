/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  STMC - implements the statistical temperature Monte Carlo simulation

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017
*/

#ifndef _STMC_H
#define _STMC_H

#include "monte_carlo.h"

namespace MC {

class STMC : public MonteCarlo
{
public:
  STMC(Particle*& _particleList, class RandomMC* _random, int _nparticles);
  STMC(class Box* _box, Particle*& _particleList, class RandomMC* _random, int _nparticles);
  ~STMC();

  void init();
  void setup();
  void run(int nrelaxations, int ncycles);

  void set_energy_bin(const double _dE) { dE = _dE; }
  void set_energy_range(const double _Emin, const double _Emax);
  void set_temperature_range(const double _Tmin, const double _Tmax);
  void set_initial_fd(double f) { f_d = f; }
  void set_flatness_threshold(double t) { threshold = t; }

  virtual void sampling_stmc();

protected:
  double total_energy;        // total energy
  double virial;              // virial
  int energy_user;            // energy range set by user

  int particle_idx;           // particle index

  double energy_correction;   // energy correction term due to truncation

  double Emin, Emax, dE;      // energy range and energy bin size
  double *hE;                 // energy histogram
  int nbins;                  // number of bins
  double Tmin, Tmax, dT;      // temperature range and temperature bin size
  double *TE;                 // statistical temperatures
  double *LE;                 // integral of 1/T(E) up to E at grid points
  double f_d;                 // f_d = 1 - f where f is the modification factor
  double threshold;           // energy histogram flatness threshold
  int flattening;

  int naccepted;
  int nrejected;

  double interpolate(const double energy, const double Emin, const double dE,
    const double* log_weights, const int nbins);
  void approximate_integral();
  double low_end_flattening();
  void output_histograms(const char* filename);
};

} // namespace MC

#endif

