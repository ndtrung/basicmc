/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Evaluator class - a generic class for interactions

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017
*/

#ifndef _EVALUATOR_H
#define _EVALUATOR_H

namespace MC {

class Evaluator
{
public:
  Evaluator() {}
  virtual ~Evaluator() {}

  virtual void init() {}
  virtual void setup() {}
  virtual void compute(const double rsq, double& eng) { eng = 0; }
  virtual void compute(const double rsq, double& eng, double& virial) { eng = 0; virial = 0; }
  virtual void compute(const double rsq, double delx, double dely, double delz,
    const double* quat1, const double* quat2, double& eng) { eng = 0; }
  virtual void compute(const double rsq, double delx, double dely, double delz,
    const double* quat1, const double* quat2, double& eng, double& virial) { eng = 0; virial = 0; }

  virtual void get_energy_correction(const double cutoff, const double rho,
   double& ecor)
  {
    ecor = 0;
  }

  virtual void get_engv_corrections(const double cutoff, const double rho,
    double& ecor, double& pcor)
  {
    ecor = 0;
    pcor = 0;
  }

  double get_cutoff() { return cutoff; }
  void set_cutoff(double _cutoff) { cutoff = _cutoff; }

protected:
  double cutoff;
};

} // namespace MC

#endif

