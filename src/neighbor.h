/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Neighbor list

  Trung Nguyen (Northwestern)
  Start: Apr 20, 2018
*/

#ifndef _NEIGHBOR_H
#define _NEIGHBOR_H

#include "simulation.h"
#include <vector>

namespace MC {

class Neighbor
{
public:
  Neighbor(class Box* _box, class Particle*& _particleList, int _nparticles);
  ~Neighbor();

  void set_cutoff_neigh(double cutoff, double skin);
  void setup();
  void build();

  int** neighbors;
  int* num_neighbors;
protected:
  Box* box;
  Particle*& particleList;
  int nparticles;

  double binsizex;
  double binsizey;
  double binsizez;
  int nbinx;
  int nbiny;
  int nbinz;
  int* bins;
  int* binhead;
  int** stencil;
  int nstencil;

  double cutneighmax;
  double skin2;
  int allocated_size;

  void bin_atoms();
  double bin_distance(int i, int j, int k);
  int coord2bin(const double x, const double y, const double z,
    const double xlo, const double ylo, const double zlo);
};
}

#endif
