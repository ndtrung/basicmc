/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Multicanonical Monte Carlo simulations

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017
*/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <cassert>

#include "mc_muca.h"
#include "buildingblocks.h"
#include "lattice.h"
#include "io.h"
#include "memory.h"
#include "random_mc.h"

using namespace MC;
using namespace std;

#define MAX_ITER 100
#define TOLERANCE 1e-5
//#define MUCA_DEBUG

/*---------------------------------------------------------------------------*/

MCMUCA::MCMUCA(Particle*& _particleList, class RandomMC* _random, int _nparticles)
 : MonteCarlo(_particleList, _random, _nparticles)
{
  dmax = 0.1;
  energy_user = 0;
  nbins = 100;
  dE = 0;
  threshold = 0.1;

  log_weights = hE = NULL;
}

/*---------------------------------------------------------------------------*/

MCMUCA::MCMUCA(class Box* _box, Particle*& _particleList, class RandomMC* _random,
  int _nparticles) : MonteCarlo(_box, _particleList, _random, _nparticles)
{
  dmax = 0.1;
  energy_user = 0;
  nbins = 100;
  dE = 0;
  threshold = 0.1;

  log_weights = hE = NULL;
}

/*---------------------------------------------------------------------------*/

MCMUCA::~MCMUCA()
{
  if (hE) destroy(hE);
  if (log_weights) destroy(log_weights);
}

/*---------------------------------------------------------------------------*/

void MCMUCA::init()
{
  MonteCarlo::init();

  printf("Running Multicanonical MC simulation..\n");
  if (logfile) fprintf(logfile, "Running Multicanonical MC simulation..\n");
}

/*---------------------------------------------------------------------------*/

void MCMUCA::setup()
{
  MonteCarlo::setup();

  double volume = box->Lx * box->Ly * box->Lz;
  double rho = (double)nparticles/volume;
  double cutoff = evaluator->get_cutoff();
  evaluator->get_energy_correction(cutoff, rho, energy_correction);

  // cell list setup

  cell = new Cell<Particle>(box, particleList, nparticles, 2.0*cutoff);
  cell->setup();

  // compute total energy and virial

  compute_total_energy(total_energy, virial);
  total_energy += energy_correction;

  // estimates for energy range

  if (!energy_user) {
    Emin = -2.0*nparticles;
    Emax = 2.0*nparticles;
  }

  if (dE == 0) {
    dE = (Emax - Emin) / nbins;
  }

  assert(dE > 0);
  nbins = (int)((Emax - Emin) / dE) + 1;

  create(hE, nbins);
  create(log_weights, nbins);

  // initialize the log of the weights to be zero: log[W(E)] = 0

  for (int i = 0; i < nbins; i++) log_weights[i] = 0;

  // write out the initial configuration

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "config", 0);

}

/*---------------------------------------------------------------------------*/

void MCMUCA::set_energy_range(const double _Emin, const double _Emax)
{
  Emin = _Emin;
  Emax = _Emax;
  energy_user = 1;
}

/*---------------------------------------------------------------------------*/

void MCMUCA::run(int nrelaxations, int ncycles)
{
  // initialize the particles, if needed

  init();

  // setting up the simulation run

  setup();

  double volume = box->Lx * box->Ly * box->Lz;
  std::vector<double> energies;

  printf("%d particles; Number density = %f; Total energy = %f\n",
    nparticles, (double)nparticles/volume, total_energy);
  printf("E = %f %f; nbins = %d, dE = %f\n", Emin, Emax, nbins, dE);

  if (logfile) {
    fprintf(logfile, "%d particles; Number density = %f; Total energy = %f\n",
      nparticles, (double)nNumTotalBeads/volume, total_energy);
    fprintf(logfile, "E = %f %f; nbins = %d, dE = %f\n", Emin, Emax, nbins, dE);
    fflush(logfile);
  }

  printf("MUCA iteration for %d cycles; nrelaxations = %d\n",
    ncycles, nrelaxations);
  printf("Cycle AcceptanceRatio Flatness\n");

  if (logfile) {
    fprintf(logfile, "\nMUCA iteration for %d cycles; nrelaxations = %d\n",
      ncycles, nrelaxations);
    fprintf(logfile, "Cycle AcceptanceRatio Flatness\n");
    fflush(logfile);
  }

  // main iteration

  clock_t start, elapsed;
  start = clock();

  int converged = 0;
  int niterations = 0; 

  // For 2-D Ising model (Gross and Janke):
  // at the beginning of each iteration, perform a thermalization phase for N_therm attempts
  //    N_therm = f_therm*w, where w ~ L^2, f_therm = 30, L = lattice size
  // then perform updates for N_updates times
  //    N_updates = 6*w^z, where z = 2.25 to cover the energy range

  int width = 10;
  double z = 2.25;
  int Ntherms = nrelaxations;
  int Nupdates = 6*nbins*nbins; // scale with nbins^2: diffusion "time scale" over nbins energy bins

  for (int n = 0; n <= ncycles; n++) {

    // reset the energy histogram

    for (int m = 0; m < nbins; m++) hE[m] = 0;

    naccepted = 0;
    nrejected = 0;

    // bin the energy histogram for another nrelaxations: N_updates = 6*w^z
    // equilibrate with the current W(E): Ntherms
    // measure how far energy has been diffusing from the bin at i = 0

    assert(total_energy >= Emin && total_energy < Emax);
    
    for (int i = 0; i < Nupdates; i++) {
      sampling_muca();
    }

    // recompute the total energy

    compute_total_energy(total_energy, virial);
    total_energy += energy_correction;
    energies.push_back(total_energy);

    double acceptance_ratio = 0;
    acceptance_ratio = (double)naccepted/(double)(naccepted+nrejected);
    
    // check if h(E) is flat

    double fluctuation = 0;
    int histrogram_flat = check_histogram_flatness(hE, nbins, threshold, fluctuation);

    if (n % sampling_period == 0 || n == ncycles) {
      printf("%8d %0.3f %0.3f\n", n, acceptance_ratio, fluctuation);
   
      if (logfile) {
        fprintf(logfile, "%8d %0.3f %0.3f\n", n, acceptance_ratio, fluctuation);
        fflush(logfile);
      }

      char filename[128];
      sprintf(filename, "weights_%d.txt", n);
      output_histograms(filename);
    }

    if (histrogram_flat) {  // update weights log[W(E)] with current h(E)

      update_weights(log_weights, hE, nbins);

    } else {

      converged = 1;
      printf("%d8d %0.3f\nConverged!\n", n, fluctuation);
      if (logfile) {
        fprintf(logfile, "%8d %0.3f\nConverged!\n", n, fluctuation);
        fflush(logfile);
      }

      char filename[128];
      sprintf(filename, "weights_%d.txt", n);
      output_histograms(filename);
      break;
    }

    ncurrent_cycle++;
  }


  elapsed = clock()-start;
  printf("\nLoop time of %f seconds for %d cycles with %d particles\n",
      (double)elapsed/CLOCKS_PER_SEC, ncurrent_cycle-1, nparticles);
  if (logfile) {
    fprintf(logfile, "\nLoop time of %f seconds for %d cycles with %d particles\n", 
      (double)elapsed/CLOCKS_PER_SEC, ncurrent_cycle-1, nparticles);
  }

  // energies visited

  ofstream ofs;
  ofs.open("energies.txt");
  int num = (int)energies.size();
  for (int m = 0; m < num; m++)
    ofs << m << " " << energies[m] << "\n";
  ofs.close();

  // production run with the final weights

  printf("\nPerform production run with the final weights for %d cycles..\n", ncycles);
  if (logfile) {
    fprintf(logfile, "\nPerform production run with the final weights for %d cycles..\n", ncycles);
  }

  for (int m = 0; m < nbins; m++) hE[m] = 0;

  for (int n = 0; n <= ncycles; n++) {
    for (int i = 0; i < nparticles; i++) {
      sampling_muca();

      int current_e_bin = (int)((total_energy - Emin) / dE);
      hE[current_e_bin] += 1;
    }
  }
    
  // final histogram

  double mean, stdev;
  create_histogram(energies, "hist_energy_final.txt", mean, stdev);

  // entropy S(E) = log[g(E)] = log[1/W(E)] = -log[W(E)]

  ofs.open("S_E.txt");
  for (int m = 0; m < nbins; m++)
    ofs << Emin + m * dE << " " << -log_weights[m] << "\n";
  ofs.close();

  // reweighting g(E) for different temperatures

  reweighting();
}

/*---------------------------------------------------------------------------
  sample with the current W(E)
  update total_energy with trial moves accepted or rejected
-----------------------------------------------------------------------------*/

void MCMUCA::sampling_muca()
{
  // randomly pick a particle idx

  while (1) {
    double r = random->random_mars();
    particle_idx = r * nparticles;
    if (particle_idx >= 0 && particle_idx < nparticles) break;
  }

  // compute the current energy of the chosen particle

  double current_particle_energy = compute_particle_energy(particle_idx);
  int idx_in_last_cell = idx_in_cell;

  // attempt a trial move to the chosen particle

  int accepted = 0;
  double last_pos[4];
  int rot = 0;
  if (particleList[particle_idx].rot_flag) {
    if (random->random_mars() < 0.5) {
      trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
    } else {
      trial_rotation(&particleList[particle_idx], rmax, last_pos);
      rot = 1;
    }
  } else {
    trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
  }

  // compute the particle energy due to the trial move

  double trial_particle_energy = compute_particle_energy(particle_idx);
  double delta_E = trial_particle_energy - current_particle_energy;

  // change in total energy is still on the order of delta_E - a trial move by a particle

  double trial_energy = total_energy + delta_E;
  if (trial_energy < Emin || trial_energy > Emax) { // out of energy range, reject right away
    if (!rot) {
      restore_displacement(&particleList[particle_idx], last_pos);
    } else {
      restore_rotation(&particleList[particle_idx], last_pos);
    }
    particle_energies[particle_idx] = current_particle_energy;
    nrejected++;

  } else {

    // accept or reject the trial move based on the difference between the weights
    // delta_logW = log[W(new)/W(old)]

    int current_e_bin = (int)((total_energy - Emin) / dE);
    int trial_e_bin = (int)((trial_energy - Emin) / dE);
    double current_logW = log_weights[current_e_bin];
    double trial_logW   = log_weights[trial_e_bin];
    double delta_logW   = trial_logW - current_logW;

    double p = exp(delta_logW);
    if (p >= 1.0 || random->random_mars() < p) { // accept
      cell->update(particle_idx, idx_in_last_cell, last_pos);
      total_energy = trial_energy;
      naccepted++;
      accepted = 1;

    } else { // reject the move
      if (!rot) {
        restore_displacement(&particleList[particle_idx], last_pos);
      } else {
        restore_rotation(&particleList[particle_idx], last_pos);
      }
      particle_energies[particle_idx] = current_particle_energy;
      nrejected++;
    }

    current_e_bin = (int)((total_energy - Emin) / dE);
    if (current_e_bin >= 0 && current_e_bin < nbins) {
      hE[current_e_bin] += 1.0;
    }

    #ifdef MUCA_DEBUG
    printf("accepted = %d: current_e_bin %d; hE: %f; "
      "current log W: %f; trial log W: %f\n",
      accepted, current_e_bin, hE[current_e_bin], logW[current_e_bin], trial_logW);
    #endif
  }
}

/*---------------------------------------------------------------------------
  reweighting g(E) for different temperatures
-----------------------------------------------------------------------------*/

void MCMUCA::reweighting()
{
  // specific heat C_v

  double T = 0.1;
  double dT = 0.01;
  double Tmax = 3.0;

  ofstream ofs;
  ofs.open("C_v.txt");
  ofs << "T C_v\n";

  while (1) {

    double beta = 1.0/T;
    double F, Fmax=0;
    int first = 1;
    for (int i = 0; i < nbins; i++) {
      if (hE[i] > 0) {

        double E = Emin + i * dE;
        F = (-log_weights[i]) - beta*E;
        if (first) {
          first = 0;
          Fmax = F;
        }
        if (Fmax < F) Fmax = F;
      }
    }

    double Q = 0;
    for (int i = 0; i < nbins; i++) {
      if (hE[i] > 0) {
        double E = Emin + i * dE;
        F = (-log_weights[i]) - beta*E;
        Q += exp(F - Fmax);
      }
    }

    double ave_e = 0;
    double ave_e2 = 0;
    for (int i = 0; i < nbins; i++) {
      if (hE[i] > 0) {
        double E = Emin + i * dE;
        F = (-log_weights[i]) - beta*E;
        ave_e += E * exp(F - Fmax) / Q;
        ave_e2 += (E * E) * exp(F - Fmax) / Q;
      }
    }

    double Cv = (ave_e2 - ave_e*ave_e)*(beta*beta);
    ofs << T << " " << Cv << "\n";

    T += dT;
    if (T > Tmax) break;
  }

  ofs.close();
  
}

/*---------------------------------------------------------------------------
  determine the first and last index with non-empty histogram entry
-----------------------------------------------------------------------------*/

void MCMUCA::get_histogram_range(const double* histogram, const int nbins,
  int& start, int& end)
{
  start = nbins - 1;
  end = 0;
  for (int i = 0; i < nbins; i++) {
    if (histogram[i] > 1) {
      if (i < start) {
        start = i;
      }
      break;
    }
  }
  for (int i = nbins; i > 0; i--) {
    if (histogram[i] > 1) {
      if (i > end) {
        end = i;
      }
      break;
    }
  }
}

/*---------------------------------------------------------------------------
  update MUCA weights according to W^{n+1}(E) = W^{n}(E) / H^{n}(E)
-----------------------------------------------------------------------------*/

void MCMUCA::update_weights(double* log_weights, const double* histogram, const int nbins)
{
  // subtract the weights by their maxmimum value
  // so that the max log weight is always zero before scaled by histogram
  
  double max_weight = log_weights[0];
  for (int i = 0; i < nbins; i++)
    if (max_weight < log_weights[i])
      max_weight = log_weights[i];

  for (int i = 0; i < nbins; i++)
    log_weights[i] -= max_weight;

  // subtract the log weights by log(histograms)

  for (int i = 0; i < nbins; i++) {
    if (histogram[i] > 0) {
      log_weights[i] = log_weights[i] - log(histogram[i]);
    }
  }
}

/*---------------------------------------------------------------------------
  output current h(E) and log_weights(E)
-----------------------------------------------------------------------------*/

void MCMUCA::output_histograms(const char* filename)
{
  ofstream ofs;
  ofs.open(filename);
  for (int i = 0; i < nbins; i++)
    ofs << Emin + i * dE << " " << hE[i] << " " << log_weights[i] << "\n";
  ofs.close();
}

