/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  MCMUCA - implements the Monte Carlo mutlicanonical simulation

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017
*/

#ifndef _MC_MUCA_H
#define _MC_MUCA_H

#include "monte_carlo.h"

namespace MC {

class MCMUCA : public MonteCarlo
{
public:
  MCMUCA(Particle*& _particleList, class RandomMC* _random, int _nparticles);
  MCMUCA(class Box* _box, Particle*& _particleList, class RandomMC* _random, int _nparticles);
  ~MCMUCA();

  void init();
  void setup();
  void run(int nrelaxations, int ncycles);

  void set_energy_bin(const double _dE) { dE = _dE; }
  void set_energy_range(const double _Emin, const double _Emax);
  void set_flatness_threshold(double t) { threshold = t; }

  virtual void sampling_muca();

protected:
  double dmax;                // maximum displacement trials
  double total_energy;        // total energy
  double virial;              // virial
  int energy_user;            // energy range set by user

  int particle_idx;           // particle index

  double energy_correction;   // energy correction term due to truncation

  double Emin, Emax, dE;      // energy range and energy bin size
  double *hE;                 // energy histogram
  int nbins;                  // number of bins
  double *log_weights;        // log of the weights
  double threshold;           // energy histogram flatness threshold

  int naccepted;
  int nrejected;

  void reweighting();
  
  void update_weights(double* log_weights, const double* histogram, const int nbins);
  void get_histogram_range(const double* histogram, const int nbins,
     int& start, int& end);
  void output_histograms(const char* filename);
};

} // namespace MC

#endif

