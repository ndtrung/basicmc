/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Kern-Frenkel interaction: single circular patch with opening angle theta (in rad)

  Trung Nguyen (Northwestern)
  Start: Nov 15, 2017
*/

#ifndef _KERN_FRENKEL_H
#define _KERN_FRENKEL_H

#include <cmath>
#include "evaluator.h"
#include "lattice.h"

namespace MC {

#define BIG 1.0e+20

class KernFrenkel : public Evaluator
{
public:
  KernFrenkel() {}
  KernFrenkel(double _sigma, double _theta, double _A, double _cutoff)
  {
    sigma = _sigma;
    sigma2 = sigma * sigma;
    theta = _theta;
    costheta = cos(theta);
    A = _A;
    cutsq = cutoff*cutoff;
  }
  virtual ~KernFrenkel() {}

  void set_params(double _sigma, double _theta, double _A) {
    sigma = _sigma;
    sigma2 = sigma * sigma;
    theta = _theta;
    costheta = cos(theta);
    A = _A;
  }

  void compute(const double rsq, const double delx, const double dely, const double delz,
    const double* quat1, const double* quat2, double& energy)
  {
    if (rsq < sigma2) energy = BIG;
    else {

      // get the directors (ez) from the quaternions
      double ez_1[3], ez_2[3];
      quat_to_exyz(quat1, ez_1);
      quat_to_exyz(quat2, ez_2);
         
      // get the dot products between ez with the distance vector r_ij
      double r = sqrt(rsq);
      double costheta_i =  (ez_1[0]*delx+ez_1[1]*dely+ez_1[2]*delz)/r;
      double costheta_j = -(ez_2[0]*delx+ez_2[1]*dely+ez_2[2]*delz)/r;

      // compute energy
      if (costheta_i <= costheta && costheta_i <= costheta)
        energy = -A;
      else energy = 0;
    }
  }

protected:
  double sigma, sigma2;    // repulsive core
  double theta, costheta;  // patch opening angle
  double A;                // attraction strength
  double cutsq;

  inline void quat_to_exyz(const double* q, double* ez) {
    ez[0] = 2.0*(q[1]*q[3] + q[0]*q[2]);
    ez[1] = 2.0*(q[2]*q[3] - q[0]*q[1]);
    ez[2] = q[0]*q[0] - q[1]*q[1] - q[2]*q[2] + q[3]*q[3];    
  }

};

} // namespace MC

#endif

