/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Metropolis NPT Monte-Carlo simulation

  Trung Nguyen (Northwestern)
  Start: Aug 15, 2017
*/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>

#include "mc_npt.h"
#include "buildingblocks.h"
#include "io.h"
#include "lattice.h"
#include "memory.h"
#include "random_mc.h"

using namespace MC;
using namespace std;

/*---------------------------------------------------------------------------*/

MCNPT::MCNPT(Particle*& _particleList, class RandomMC* _random, int _nparticles,
   double _temperature, double _pressure)
 : MonteCarlo(_particleList, _random, _nparticles), temperature(_temperature), pressure(_pressure)
{
  dmax = 0.5;
  rmax = 0.5;
  logdVmax = 0.1;

  num_displacements = nparticles;
  num_vol_exchanges = 1;

  naccepted_displacement = 0;
  nrejected_displacement = 0;
  naccepted_vol_scaling = 0;
  nrejected_vol_scaling = 0;

  box_changed = 1;
  s = NULL;
}

/*---------------------------------------------------------------------------*/

MCNPT::MCNPT(class Box* _box, Particle*& _particleList, class RandomMC* _random,
  int _nparticles, double _temperature, double _pressure)
 : MonteCarlo(_box, _particleList, _random, _nparticles),
  temperature(_temperature), pressure(_pressure)
{
  dmax = 0.5;
  rmax = 0.5;
  logdVmax = 0.1;

  num_displacements = nparticles;
  num_vol_exchanges = 1;

  naccepted_displacement = 0;
  nrejected_displacement = 0;
  naccepted_vol_scaling = 0;
  nrejected_vol_scaling = 0;

  box_changed = 1;
  s = NULL;
}

/*---------------------------------------------------------------------------*/

MCNPT::~MCNPT()
{
  destroy(s);
}

/*---------------------------------------------------------------------------*/

void MCNPT::set_num_attempts(int _num_displacements, int _num_vol_exchanges)
{
  num_displacements = _num_displacements;
  num_vol_exchanges = _num_vol_exchanges;
}

/*---------------------------------------------------------------------------*/

void MCNPT::init()
{
  MonteCarlo::init();

  printf("Running MC NPT simulation..\n");
  if (logfile) fprintf(logfile, "Running MC NPT simulation..\n");
}

/*---------------------------------------------------------------------------*/

void MCNPT::setup()
{
  MonteCarlo::setup();

  double cutoff = evaluator->get_cutoff();
  volume = box->Lx * box->Ly * box->Lz;
  rho = (double)nparticles/volume;
  evaluator->get_engv_corrections(cutoff, rho, energy_correction, pressure_correction);

  // cell list setup

  cell = new Cell<Particle>(box, particleList, nparticles, 2.0*cutoff);
  cell->setup();

  // cell list setup

  cell = new Cell<Particle>(box, particleList, nparticles, 2.0*cutoff);
  cell->setup();

  // compute total energy

  compute_total_energy(total_energy, virial);
  total_energy += nparticles * energy_correction;

  printf("Number of particles = %d\nPressure = %g\nTemperature = %g\n",
    nparticles, pressure, temperature);
  if (logfile) {
    fprintf(logfile, "Number of particles = %d\nPressure = %g\n"
      "Temperature = %g\n", nparticles, pressure, temperature);
    fflush(logfile);
  }

  // allocate backup scaled coordinates

  create(s, nNumTotalBeads, 3);

  // write out the initial configuration

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "config", 0);
}

/*---------------------------------------------------------------------------*/

void MCNPT::run(int nrelaxations, int ncycles)
{
  // initialize the particles, if needed

  init();

  if (target_acceptance > 0) {
    printf("WARNING: Adjusting trial moves to achieve target acceptance ratio of %g, "
     "detailed balance is not satisfied.\n", target_acceptance);
  }

  // setting up the simulation run

  setup();

  // relaxation

  printf("Relaxation for %d cycles\n", nrelaxations);
  printf("Cycle Energy NumberDensity Pressure\n");

  if (logfile) {
    fprintf(logfile, "Relaxation for %d cycles\n", nrelaxations);
    fprintf(logfile, "Cycle Energy NumberDensity Pressure\n");
    fflush(logfile);
  }

  std::vector<double> energies, densities, pressures;
  double current_energy;
  int ntrials = num_displacements + num_vol_exchanges;
  int niterations = 0;

  clock_t start, elapsed;
  start = clock();

  for (int n = 0; n < nrelaxations; n++) {

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++) {

      int r = (int)(random->random_mars() * ntrials);
      if (r < num_displacements)
        current_energy = particle_displacement();
      else
        current_energy = volume_scaling();
    }

    if (n % dump_period == 0 || n % sampling_period == 0)
      thermo(ncurrent_cycle);

    ncurrent_cycle++;
  }

  thermo(ncurrent_cycle);

  elapsed = clock()-start;
  printf("\nLoop time of %g seconds for %d cycles with %d particles\n",
      (double)elapsed/CLOCKS_PER_SEC, nrelaxations, nparticles);
  if (logfile) {
    fprintf(logfile, "\nLoop time of %g seconds for %d cycles with %d particles\n", 
      (double)elapsed/CLOCKS_PER_SEC, nrelaxations, nparticles);
  }

  // production run
  niterations = 0;

  printf("\nProduction run for %d cycles\n", ncycles);
  printf("Cycle Energy NumberDensity Pressure AcceptanceRatio_disp AcceptanceRatio_vol\n");

  if (logfile) {
    fprintf(logfile, "\nProduction run for %d cycles\n", ncycles);
    fprintf(logfile, "Cycle Energy NumberDensity Pressure AcceptanceRatio_disp AcceptanceRatio_vol\n");
    fflush(logfile);
  }

  start = clock();

  for (int n = 0; n < ncycles; n++) {

    // make ntrials per cycle

    for (int i = 0; i < ntrials; i++) {

      sampling_npt(current_energy, num_displacements, ntrials);
      
      if (niterations % sampling_period == 0) {
        densities.push_back(rho);
        energies.push_back(total_energy);
        pressures.push_back(vpress);
      }

      niterations++;
    }

    if (n % dump_period == 0 || n % sampling_period == 0)
      thermo(ncurrent_cycle);

    if (n % dump_period == 0) {
      double mean, stdev;
      create_histogram(energies, "hist_energy.txt", mean, stdev);
      create_histogram(densities, "hist_density.txt", mean, stdev);
      create_histogram(pressures, "hist_pressure.txt", mean, stdev);

      io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
         box->ylo, box->yhi, box->zlo, box->zhi, "config", n);
    }

    ncurrent_cycle++;
  }

  thermo(ncurrent_cycle);

  elapsed = clock()-start;
  printf("\nLoop time of %f seconds for %d cycles with %d particles\n",
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  if (logfile) {
    fprintf(logfile, "\nLoop time of %f seconds for %d cycles with %d particles\n", 
      (double)elapsed/CLOCKS_PER_SEC, ncycles, nparticles);
  }

  // final configuration

  io_handle->write(particleList, nparticles, nNumTotalBeads, box->xlo, box->xhi,
     box->ylo, box->yhi, box->zlo, box->zhi, "final", ncurrent_cycle);

  // final histograms

  double mean, stdev;
  create_histogram(energies, "hist_energy_final.txt", mean, stdev);
  printf("Energy: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Energy: %f +/- %f\n", mean, stdev);

  create_histogram(densities, "hist_density_final.txt", mean, stdev);
  printf("Density: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Density: %f +/- %f\n", mean, stdev);

  create_histogram(pressures, "hist_pressure_final.txt", mean, stdev);
  printf("Pressure: %f +/- %f\n", mean, stdev);
  if (logfile) fprintf(logfile, "Pressure: %f +/- %f\n", mean, stdev);
}

/*---------------------------------------------------------------------------*/

void MCNPT::thermo(int n)
{
  double cutoff = evaluator->get_cutoff();
  volume = box->Lx * box->Ly * box->Lz;
  rho = (double)nparticles/volume;
  evaluator->get_engv_corrections(cutoff, rho, energy_correction,
    pressure_correction);

  compute_total_energy(total_energy, virial);
  total_energy += nparticles * energy_correction;
  vpress = nparticles*temperature/volume + virial/(3*volume) +
    pressure_correction;

  double acceptance_disp = (double)naccepted_displacement /
        (double)(naccepted_displacement+nrejected_displacement);
  double acceptance_vol = 0;
  if (naccepted_vol_scaling+nrejected_vol_scaling > 0)
    acceptance_vol = (double)naccepted_vol_scaling /
      (double)(naccepted_vol_scaling+nrejected_vol_scaling);

  printf("%8d %8.3f %8.3f %8.3f %0.3f %0.3f\n",
    n, total_energy, rho, vpress, acceptance_disp, acceptance_vol);

  if (logfile) {
    fprintf(logfile, "%8d %8.3f %8.3f %8.3f %0.3f %0.3f\n",
      n, total_energy, rho, vpress, acceptance_disp, acceptance_vol);
    fflush(logfile);
  }

}

/*---------------------------------------------------------------------------*/

void MCNPT::sampling_npt(double& current_energy, int num_displacements, int ntrials)
{
  int r = (int)(random->random_mars() * ntrials);
  if (r < num_displacements)
    current_energy = particle_displacement();
  else
    current_energy = volume_scaling();
}

/*---------------------------------------------------------------------------*/

double MCNPT::particle_displacement()
{
  double beta = 1.0/temperature;

  // randomly pick a particle idx

  while (1) {
    double r = random->random_mars();
    particle_idx = r * nparticles;
    if (particle_idx >= 0 && particle_idx < nparticles) break;
  }

  // compute the current energy of the chosen particle

  double current_particle_energy = compute_particle_energy(particle_idx);
  int idx_in_last_cell = idx_in_cell;

  // attempt a trial move to the chosen particle, backup last position

  double last_pos[4];
  int rot = 0;
  if (particleList[particle_idx].rot_flag) {
    if (random->random_mars() < 0.5) {
      trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
    } else {
      trial_rotation(&particleList[particle_idx], rmax, last_pos);
      rot = 1;
    }
  } else {
    trial_displacement(&particleList[particle_idx], box, dmax, last_pos);
  }

  // compute the particle energy due to the trial move

  double trial_particle_energy = compute_particle_energy(particle_idx);
  double delta_E = trial_particle_energy - current_particle_energy;

  // accept or reject the trial move

  if (delta_E <= 0) { // accept the move

    cell->update(particle_idx, idx_in_last_cell, last_pos);
    total_energy += delta_E;
    naccepted_displacement++;

  } else {

    double p = exp(-beta*delta_E);
    if (random->random_mars() < p) { // accept

      cell->update(particle_idx, idx_in_last_cell, last_pos);
      total_energy += delta_E;
      naccepted_displacement++;

    } else { // reject
      if (!rot) {
        restore_displacement(&particleList[particle_idx], last_pos);
      } else {
        restore_rotation(&particleList[particle_idx], last_pos);
      }
      restore_displacement(&particleList[particle_idx], last_pos);
      particle_energies[particle_idx] = current_particle_energy;
      nrejected_displacement++;
    }
  }

  return total_energy;
}

/*---------------------------------------------------------------------------*/

double MCNPT::volume_scaling()
{
  double current_energy, trial_energy, current_virial, trial_virial;
  double beta = 1.0/temperature;

  // pick a random number of [-logdVmax;logdVmax]

  double r = random->random_mars();
  double delta = -logdVmax + r * (2*logdVmax);
  double current_volume = box->Lx * box->Ly * box->Lz;
  //double logV = log(current_volume);
  double logVtrial = log(current_volume) + delta;
  double Vtrial = exp(logVtrial);
  double deltaV = Vtrial - current_volume;
  double scale_vol = Vtrial/current_volume;
  double scale_L = pow(scale_vol, 1.0/3.0);
  
  // compute the current energy of the whole system

  compute_total_energy(current_energy, current_virial);

  // save the box dimension

  double old_Lx = box->Lx;
  double old_Ly = box->Ly;
  double old_Lz = box->Lz;

  double xlo = box->xlo;
  double ylo = box->ylo;
  double zlo = box->zlo;

  // make trial box dimensions

  box->Lx *= scale_L;
  box->Ly *= scale_L;
  box->Lz *= scale_L;

  box->xhi = box->xlo + box->Lx;
  box->yhi = box->ylo + box->Ly;
  box->zhi = box->zlo + box->Lz;

  // rescale the particle coordinates, fixed point at (xlo, ylo, zlo)

  for (int i = 0; i < nparticles; i++) {

    double xtmp = particleList[i].beadList[0].x;
    double ytmp = particleList[i].beadList[0].y;
    double ztmp = particleList[i].beadList[0].z;

    s[i][0] = (xtmp - xlo) / old_Lx;
    s[i][1] = (ytmp - ylo) / old_Ly;
    s[i][2] = (ztmp - zlo) / old_Lz;

    particleList[i].beadList[0].x = xlo + s[i][0] * box->Lx;
    particleList[i].beadList[0].y = ylo + s[i][1] * box->Ly;
    particleList[i].beadList[0].z = zlo + s[i][2] * box->Lz;
  }

  // compute the total energy due to the trial volume scaling

  cell->setup();
  cell->bin_particles();

  compute_total_energy(trial_energy, trial_virial);

  double delta_E = trial_energy - current_energy;
  double boltzfactor = (nparticles+1)*log(scale_vol) - beta*delta_E - beta*pressure*deltaV;

  if (boltzfactor >= 0) { // accept the move

    current_energy = trial_energy;
    naccepted_vol_scaling++;

  } else {

    double p = exp(boltzfactor);
    if (random->random_mars() < p) { // accept

      current_energy = trial_energy;
      naccepted_vol_scaling++;

    } else { // reject

      // restore box bounds and dimensions

      box->xhi = box->xlo + old_Lx;
      box->yhi = box->ylo + old_Ly;
      box->zhi = box->zlo + old_Lz;
      box->Lx = old_Lx;
      box->Ly = old_Ly;
      box->Lz = old_Lz;

      restore_all_particles();

      cell->setup();
      cell->bin_particles();

      nrejected_vol_scaling++;
    }
  }

  double cutoff = evaluator->get_cutoff();
  volume = box->Lx * box->Ly * box->Lz;
  rho = (double)nparticles/volume;
  evaluator->get_energy_correction(cutoff, rho, energy_correction);
  total_energy = current_energy + energy_correction;
  vpress = nparticles*temperature/volume + virial/(3*volume) +
    pressure_correction;

  return current_energy;
}

/*---------------------------------------------------------------------------*/

void MCNPT::restore_all_particles()
{
  double Lx = box->Lx;
  double Ly = box->Ly;
  double Lz = box->Lz;

  double xlo = box->xlo;
  double ylo = box->ylo;
  double zlo = box->zlo;

  for (int i = 0; i < nparticles; i++) {

    particleList[i].beadList[0].x = xlo + s[i][0] * Lx;
    particleList[i].beadList[0].y = ylo + s[i][1] * Ly;
    particleList[i].beadList[0].z = zlo + s[i][2] * Lz;
  }

}

