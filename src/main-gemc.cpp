/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  Example of Gibbs ensemble Monte Carlo simulation with ESMC

  Trung Nguyen (Northwestern)
  Nov 2017
*/

#include <stdlib.h>
#include <vector>
#include <iostream>
#include <cassert>

#include "buildingblocks.h"
#include "initializer.h"
#include "memory.h"
#include "simulation.h"
#include "random_mc.h"

#include "monte_carlo.h"
#include "gemc.h"

#include "lj126.h"
#include "hardsphere.h"
#include "square_well.h"
#include "colloid.h"
#include "opp.h"

using namespace MC;
using namespace std;

int main(int argc, char** argv)
{
  int me=0, nprocs=1;
  int ncycles = 100;
  int nrelaxations = 1000;

  // ensemble
  double temperature = 1.0;

  // particle initialization and box dimensions
  int nparticles = 125;
  double ax = 1.2;
  double ay = 1.2;
  double az = 1.2;
  double lx = 14;
  double ly = 14;
  double lz = 14;

  // force field
  char forcefield[64];
  strcpy(forcefield, "lj/cut");
  double epsilon = 1.0;
  double sigma = 1.0;
  double cutoff = 2.5;
  double kappa = 6.25;
  double phi = 0.62;
  double lambda = 1.2;

  // output setting
  unsigned int seed = 3478914;
  int sampling_period = 100;
  int dump_period = 1000;
  char logfile[256];
  int use_log = 0; 
  char restartfile[256];
  int use_restart = 0; 

  int num_displacements = nparticles;
  int num_vol_exchanges = (int)(0.1*nparticles) + 1;
  int num_particle_exchanges = (int)(0.5*nparticles) + 1;

  int iarg = 1;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"box") == 0) {
      lx = atof(argv[iarg+1]);
      ly = atof(argv[iarg+2]);
      lz = atof(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"nparticles") == 0) {
      nparticles = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"spacing") == 0) {
      ax = atof(argv[iarg+1]);
      ay = atof(argv[iarg+2]);
      az = atof(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"ncycles") == 0) {
      ncycles = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"nrelaxations") == 0) {
      nrelaxations = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"temperature") == 0) {
      temperature = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"seed") == 0) {
      seed = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"sampling_period") == 0) {
      sampling_period = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"dump_period") == 0) {
      dump_period = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"log") == 0) {
      strcpy(logfile, argv[iarg+1]);
      use_log = 1;
      iarg += 2;
    } else if (strcmp(argv[iarg],"restart") == 0) {
      strcpy(restartfile, argv[iarg+1]);
      use_restart = 1;
      iarg += 2;
    } else if (strcmp(argv[iarg],"attempts") == 0) {
      num_displacements = atoi(argv[iarg+1]);
      num_vol_exchanges = atoi(argv[iarg+2]);
      num_particle_exchanges = atoi(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"pair_style") == 0) {
      strcpy(forcefield, argv[iarg+1]);
      if (strcmp(forcefield, "lj/cut") == 0) {
        epsilon = atof(argv[iarg+2]);
        sigma = atof(argv[iarg+3]);
        cutoff = atof(argv[iarg+4]);
        iarg += 5;
      } else if (strcmp(forcefield, "squarewell") == 0) {
        sigma = atof(argv[iarg+2]);
        lambda = atof(argv[iarg+3]);
        epsilon = atof(argv[iarg+4]);
        iarg += 5;
      } else if (strcmp(forcefield, "colloid") == 0) {
        sigma = atof(argv[iarg+2]);
        epsilon = atof(argv[iarg+3]);
        kappa = atof(argv[iarg+4]);
        cutoff = atof(argv[iarg+5]);
        iarg += 6;
      } else if (strcmp(forcefield, "hardsphere") == 0) {
        cutoff = atof(argv[iarg+2]);
        iarg += 3;
      } else if (strcmp(forcefield, "opp") == 0) {
        kappa = atof(argv[iarg+2]);
        phi = atof(argv[iarg+3]);
        cutoff = atof(argv[iarg+4]);
        iarg += 5;
      } else {
        std::cout << "Invalid force field " << forcefield << "\n";
        return 1;
      }
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  // particles
  Particle* particleList = NULL;
  // simulation box
  Box* box = NULL;
  // the simulator
  MonteCarlo* sim = NULL;
  // create the random number generator
  RandomMC* random = new RandomMC(seed+me);

  if (use_restart == 0) {

    Initializer<Particle> init;
    init.create_box(box, particleList, nparticles, lx, ly, lz, ax, ay, az);

    sim = new GEMC(box, particleList, random, nparticles, temperature);

  } else {

    particleList = new Particle [nparticles];

    sim = new GEMC(particleList, random, nparticles, temperature);
    
    if (sim) sim->read_restart(restartfile);

  }

  if (!sim) {
    std::cout << "Error: Cannot create the simulator\n";
    delete random;
    return 1;
  }

  // specify the pair style in use

  if (strcmp(forcefield, "lj/cut") == 0)
    sim->set_evaluator(new LJ126(epsilon, sigma, cutoff));
  else if (strcmp(forcefield, "hardsphere") == 0)
    sim->set_evaluator(new HardSphere(cutoff));
  else if (strcmp(forcefield, "squarewell") == 0)
    sim->set_evaluator(new SquareWell(sigma, lambda, epsilon));
  else if (strcmp(forcefield, "colloid") == 0)
    sim->set_evaluator(new Colloid(sigma, epsilon, kappa, cutoff));
  else if (strcmp(forcefield, "opp") == 0)
    sim->set_evaluator(new Oscillating(kappa, phi, cutoff));

  // specify output periods

  if (use_log) sim->set_log(logfile);
  sim->set_sampling(sampling_period);
  sim->set_dump(dump_period);

  // specify GEMC parameters

  sim->set_num_attempts(num_displacements,
    num_vol_exchanges, num_particle_exchanges);

  // run the simulation
  
  sim->run(nrelaxations, ncycles);
    
  // clean up memory allocation

  delete [] particleList;
  delete random;
  delete sim;

  return 0;
}
