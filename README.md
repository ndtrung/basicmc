# BasicMC

BasicMC is an open-source software library for performing particle-based Monte Carlo simulations.
BasicMC is designed for the following purposes:

* For students, to learn how basic Monte Carlo algorithms are implemented.
* For researchers, to have access to popular and advanced MC techniques
that are widely used in the literature yet not publicly available anywhere.
* For developers, to have an object-oriented framework to extend and modify the existing API to suit their needs.

BasicMC provides an API through a static libary (libmc.a)
that supports the following Monte Carlo sampling techniques in version 0.0.1:

 * Standard Boltzmann techniques:
      * Canonical ensemble (NVT)
      * Isobaric-isothermal (NPT)
      * Grand canonical ensemble (muVT)
      * Gibbs ensemble Monte Carlo (GEMC)

 * Non-Boltzmann sampling methods:
      * Multicanonical ensemble (MUCA)
      * Wang-Landau sampling
      * Statistical temperature Monte Carlo (STMC)

The following pair-wise interactions are supported in version 0.0.1:

 * Lennard-Jones 12-6 potential
 * Hard-sphere potential
 * Square-well potential
 * Oscillating pair potential
 * Kern-Frenkel potential (single-patch)

# Building BasicMC

Old-fashioned build:

```bash
cd src/
make
```

The following executables are also built as drivers that use the API provided by libmc.a

* mc_run		standard MC techniques
* muca_run		multicanonical-related sampling
* gemc_run		Gibbs-ensemble MC

These executables allow off-lattice simulations of single-component systems.
See examples/README for some example uses of the executables.

CMake build:

```bash
mkdir build
mkdir install
cd build
cmake ../ -CMAKE_INSTALL_PREFIX=../install
make install -j4
```

The generated library libmc.a is located in install/lib, the executables in install/bin.

# Extending the API

* To add new pair potentials: derive from Evaluator in a similar way to LJ126
* To add new Monte Carlo methods or new trial moves: derive from MonteCarlo
* To add new particle types, copy and modify main.cpp to another driving code.

See the classes in src/ to derive new classes from the existing ones.

# Under-development functionalities

* Sampling with anisotropic particles
* Umbrella sampling driver code
* Hybrid MC/MD by interfacing with LAMMPS
* Replica exchange simulations
* Python wrapper

